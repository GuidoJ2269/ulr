# the system base image target
set(shell_SRCS
    shell.c
)

set(shell_LAYOUT ${CONFIG_DIR}/${ARCH}/shell.ld)

add_library(shell SHARED ${shell_SRCS})

set_target_properties(shell PROPERTIES LINK_FLAGS "-T ${shell_LAYOUT} -z common-page-size=16 -z max-page-size=4096")

target_link_libraries(shell gcc)

# install
install(TARGETS shell DESTINATION ${PROJECT_SOURCE_DIR}/iso/vms\$common/sys\$ldr)
