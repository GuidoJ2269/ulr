/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <opdriver.h>
#include <ssdef.h>
#include <util.h>
#include <vgaconsole.h>

#include <vms_drivers.h>

/* Prototypes for driver routines defined in this module */
/* Unit initialization routine */
int op$unit_init(IDB *idb, UCB *ucb);
/* FDT routine for write functions */
int op$write(IRP *irp, PCB *pcb, UCB *ucb, CCB *ccb);
/* Device I/O database structure initialization routine */
void op$struc_init(CRB *crb, DDB *ddb, IDB *idb, ORB *orb, UCB *ucb);
/* Device I/O database structure re-initialization routine */
void op$struc_reinit(CRB *crb, DDB *ddb, IDB *idb, ORB *orb, UCB *ucb);

int driver$init_tables(void)
{
    /* Finish initialization of the Driver Prologue Table (DPT) */
    ini_dpt_name        (&driver$dpt, "OPDRIVER");
/*
    ini_dpt_adapt       (&driver$dpt, AT$_KA0602);
    ini_dpt_defunits    (&driver$dpt, 1);
    ini_dpt_ucbsize     (&driver$dpt, sizeof(UCB));
*/
    ini_dpt_struc_init  (&driver$dpt, op$struc_init );
    ini_dpt_struc_reinit(&driver$dpt, op$struc_reinit );
    ini_dpt_end         (&driver$dpt);

    /* Finish initialization of the Driver Dispatch Table (DDT) */
    ini_ddt_unitinit    (&driver$ddt, op$unit_init);
/*
    ini_ddt_start       (&driver$ddt, op$startio);
    ini_ddt_cancel      (&driver$ddt, ioc_std$cancelio);
*/
    ini_ddt_end         (&driver$ddt);

    /* Finish initialization of the Function Decision Table (FDT)   */
    ini_fdt_act(&driver$fdt, IO$_WRITELBLK, op$write, BUFFERED);
    ini_fdt_act(&driver$fdt, IO$_WRITEPBLK, op$write, BUFFERED);
    ini_fdt_act(&driver$fdt, IO$_WRITEVBLK, op$write, BUFFERED);
    ini_fdt_end(&driver$fdt);

    /* If we got this far then everything worked, so return success. */
    return SS$_NORMAL;
}

void op$struc_init(CRB *crb, DDB *ddb, IDB *idb, ORB *orb, UCB *ucb)
{
    SUPPRESS_UNUSED_WARNING(crb);
    SUPPRESS_UNUSED_WARNING(ddb);
    SUPPRESS_UNUSED_WARNING(idb);
    SUPPRESS_UNUSED_WARNING(orb);

    ucb->ucb$q_devchar.dev$q_devchar = DEV$M_CCL | DEV$M_TRM | DEV$M_OPR | DEV$M_SHR | DEV$M_AVL | DEV$M_ODV;
    ucb->ucb$b_devclass = DC$_TERM;
}

void op$struc_reinit(CRB *crb, DDB *ddb, IDB *idb, ORB *orb, UCB *ucb)
{
    SUPPRESS_UNUSED_WARNING(crb);
    SUPPRESS_UNUSED_WARNING(idb);
    SUPPRESS_UNUSED_WARNING(orb);
    SUPPRESS_UNUSED_WARNING(ucb);

    ddb->ddb$ps_ddt = &driver$ddt;
}

int op$unit_init(IDB *idb, UCB *ucb)
{
    SUPPRESS_UNUSED_WARNING(idb);
    SUPPRESS_UNUSED_WARNING(ucb);

    vga_console_init();

    return SS$_NORMAL;
}

int op$write(IRP *irp, PCB *pcb, UCB *ucb, CCB *ccb)
{
    /* If we got this far then everything worked, so return success. */
    return SS$_NORMAL;
}
