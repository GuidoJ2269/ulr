/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <doinit.h>
#include <opdriver.h>
#include <system_routines.h>
#include <vgaconsole.h>

void init_opdriver(void);

struct _inivec ini$a_vector_table[] = {{init_opdriver, {{0}}}, {0, {{0}}}};

void init_opdriver(void)
{
    driver$init_tables();

    base_image.con_init_cty = vga_console_init;
    base_image.con_putchar = vga_console_putchar;
    base_image.exe_outblank = vga_console_outblank;
    base_image.exe_outbyte = vga_console_outbyte;
    base_image.exe_outchar = vga_console_outchar;
    base_image.exe_outcrlf = vga_console_outcrlf;
    base_image.exe_outcstring = vga_console_outcstring;
    base_image.exe_outhex = vga_console_outhex;
    base_image.exe_outztring = vga_console_outzstring;
}
