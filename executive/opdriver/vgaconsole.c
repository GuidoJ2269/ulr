/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 * NON INFRINGEMENT.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <io.h>
#include <ssdef.h>

#include <vgaconsole.h>

/* move the cursor to the start of the line */
void vga_console_carriage_return(void);

/* clear the VGA console */
int vga_console_clear(void);

/* move the cursor to the next position */
void vga_console_next(void);

/* move the cursor one line down */
void vga_console_new_line(void);

/* write a character at the given position */
int vga_console_putchar_at(const char c, const unsigned int x, const unsigned int y);

/* scroll up the entire screen for the given number of lines */
int vga_console_scrollup(const unsigned int lines);

/* scroll down the entire screen for the given number of lines */
int vga_console_scrolldown(const unsigned int lines);

/* set the hardware cursor to the given position */
int vga_console_setcursor(const unsigned int x, const unsigned int y);

#define VGA_CONSOLE_COLOR_MASK 0x0f

#define VGA_DISPLAY_MEMORY_ADDR 0xb8000
#define VGA_CRTC_REGISTER_PORT  0x03d4
#define VGA_CRTC_DATA_PORT      0x03d5

#define VGA_REG_HORIZONTAL_TOTAL          0x00
#define VGA_REG_END_HORIZONTAL_DISPLAY    0x01
#define VGA_REG_START_HORIZONTAL_BLANKING 0x02
#define VGA_REG_END_HORIZONTAL_BLANKING   0x03
#define VGA_REG_START_HORIZONTAL_RETRACE  0x04
#define VGA_REG_END_HORIZONTAL_RETRACE    0x05
#define VGA_REG_VERTICAL_TOTAL            0x06
#define VGA_REG_OVERFLOW                  0x07
#define VGA_REG_PRESET_ROW_SCAN           0x08
#define VGA_REG_MAXIMUM_SCAN_LINE         0x09
#define VGA_REG_CURSOR_START              0x0a
#define VGA_REG_CURSOR_END                0x0b
#define VGA_REG_START_ADDRESS_HIGH        0x0c
#define VGA_REG_START_ADDRESS_LOW         0x0d
#define VGA_REG_CURSOR_LOCATION_HIGH      0x0e
#define VGA_REG_CURSOR_LOCATION_LOW       0x0f
#define VGA_REG_VERTICAL_RETRACE_START    0x10
#define VGA_REG_VERTICAL_RETRACE_END      0x11
#define VGA_REG_VERTICAL_DISPLAY_END      0x12
#define VGA_REG_OFFSET                    0x13
#define VGA_REG_UNDERLINE_LOCATION        0x14
#define VGA_REG_START_VERTICAL_BLANKING   0x15
#define VGA_REG_END_VERTICAL_BLANKING     0x16
#define VGA_REG_CRTC_MODE_CONTROL         0x17
#define VGA_REG_LINE_COMPARE              0x18

#define TABSIZE 8

uint16_t vga_console_color = 0;
unsigned int vga_console_xpos = 0;
unsigned int vga_console_ypos = 0;

int vga_console_clear(void)
{
    uint16_t *vga_console_ptr = (uint16_t*) VGA_DISPLAY_MEMORY_ADDR;
    unsigned int n = VGA_CONSOLE_WIDTH * VGA_CONSOLE_HEIGHT;
    while (n > 0)
    {
        *vga_console_ptr++ = vga_console_color;
        n--;
    }
    return SS$_NORMAL;
}

int vga_console_putchar_at(const char c, const unsigned int x, const unsigned int y)
{
    if ((x >= VGA_CONSOLE_WIDTH) || (y >= VGA_CONSOLE_HEIGHT))
    {
        return SS$_BADPARAM;
    }
    uint16_t *vga_console_ptr = (uint16_t*) VGA_DISPLAY_MEMORY_ADDR + y * VGA_CONSOLE_WIDTH + x;
    *vga_console_ptr = ((unsigned int)c | vga_console_color) & 0xffff;
    return SS$_NORMAL;
}

int vga_console_setcolor(const unsigned int fg, const unsigned int bg)
{
    vga_console_color = ((((bg & VGA_CONSOLE_COLOR_MASK) << 4) | (fg & VGA_CONSOLE_COLOR_MASK)) << 8) & 0xffff;
    return SS$_NORMAL;
}

int vga_console_scrollup(const unsigned int lines)
{
    if ((lines == 0) || (lines >= VGA_CONSOLE_HEIGHT))
    {
        return SS$_BADPARAM;
    }
    uint16_t *vga_console_destptr = (uint16_t*) VGA_DISPLAY_MEMORY_ADDR;
    uint16_t *vga_console_srcptr = vga_console_destptr + (VGA_CONSOLE_WIDTH * lines);
    unsigned int n = VGA_CONSOLE_WIDTH * (VGA_CONSOLE_HEIGHT - lines);
    while (n > 0)
    {
        *vga_console_destptr++ = *vga_console_srcptr++;
        n--;
    }
    n = VGA_CONSOLE_WIDTH * lines;
    while (n > 0)
    {
        *vga_console_destptr++ = vga_console_color;
        n--;
    }
    return SS$_NORMAL;
}

int vga_console_scrolldown(const unsigned int lines)
{
    if ((lines == 0) || (lines >= VGA_CONSOLE_HEIGHT))
    {
        return SS$_BADPARAM;
    }
    uint16_t *vga_console_destptr = (uint16_t*) VGA_DISPLAY_MEMORY_ADDR + (VGA_CONSOLE_WIDTH * VGA_CONSOLE_HEIGHT);
    uint16_t *vga_console_srcptr = vga_console_destptr - (VGA_CONSOLE_WIDTH * lines);
    unsigned int n = VGA_CONSOLE_WIDTH * (VGA_CONSOLE_HEIGHT - lines);
    while (n > 0)
    {
        *--vga_console_destptr = *--vga_console_srcptr;
        n--;
    }
    n = VGA_CONSOLE_WIDTH * lines;
    while (n > 0)
    {
        *--vga_console_destptr = vga_console_color;
        n--;
    }
    return SS$_NORMAL;
}

int vga_console_setcursor(const unsigned int x, const unsigned int y)
{
    unsigned int offset = y * VGA_CONSOLE_WIDTH + x;

    outb_p(VGA_REG_CURSOR_LOCATION_HIGH, VGA_CRTC_REGISTER_PORT);
    outb_p(offset >> 8, VGA_CRTC_DATA_PORT);
    outb_p(VGA_REG_CURSOR_LOCATION_LOW, VGA_CRTC_REGISTER_PORT);
    outb_p(offset, VGA_CRTC_DATA_PORT);
    return SS$_NORMAL;
}

void vga_console_next(void)
{
    vga_console_xpos++;
    if (vga_console_xpos >= VGA_CONSOLE_WIDTH)
    {
        vga_console_xpos = 0;
        if (vga_console_ypos < (VGA_CONSOLE_HEIGHT - 1))
        {
            vga_console_ypos++;
        }
        else
        {
            (void) vga_console_scrollup(1);
        }
    }
    vga_console_setcursor(vga_console_xpos, vga_console_ypos);
}

void vga_console_carriage_return(void)
{
    vga_console_xpos = 0;
    vga_console_setcursor(vga_console_xpos, vga_console_ypos);
}

void vga_console_new_line(void)
{
    vga_console_xpos = 0;
    if (vga_console_ypos < (VGA_CONSOLE_HEIGHT - 1))
    {
        vga_console_ypos++;
    }
    else
    {
        (void) vga_console_scrollup(1);
    }
    vga_console_setcursor(vga_console_xpos, vga_console_ypos);
}

void vga_console_init(void)
{
    vga_console_xpos = 0;
    vga_console_ypos = 0;
    vga_console_setcolor(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
    vga_console_clear();
    vga_console_setcursor(vga_console_xpos, vga_console_ypos);
}

void vga_console_putchar(char c)
{
    switch (c)
    {
        case '\n':
            vga_console_new_line();
            break;
        case '\r':
            vga_console_carriage_return();
            break;
        case '\t':
        {
            vga_console_xpos += TABSIZE - (vga_console_xpos % TABSIZE);
            vga_console_setcursor(vga_console_xpos, vga_console_ypos);
            break;
        }
        default:
        {
            int result = vga_console_putchar_at(c, vga_console_xpos, vga_console_ypos);
            if (result == SS$_NORMAL)
            {
                vga_console_next();
            }
            break;
        }
    }
}

void vga_console_outblank(void)
{
    vga_console_putchar(' ');
}

void vga_console_outbyte(const unsigned char c)
{
    static const char *digits = "0123456789ABCDEF";

    vga_console_putchar(digits[(c >> 4) & 0x0f]);
    vga_console_putchar(digits[c & 0x0f]);
}

void vga_console_outchar(const char c)
{
    vga_console_putchar(c);
}

void vga_console_outcrlf(void)
{
    vga_console_carriage_return();
    vga_console_new_line();
}

void vga_console_outcstring(const char *s)
{
    unsigned int n = (unsigned int)*s;
    while (n > 0)
    {
        s++;
        vga_console_putchar(*s);
        n--;
    }
}

void vga_console_outhex(const unsigned int x)
{
    vga_console_outbyte((unsigned char)((x >> 24) & 0xff));
    vga_console_outbyte((unsigned char)((x >> 16) & 0xff));
    vga_console_outbyte((unsigned char)((x >> 8) & 0xff));
    vga_console_outbyte((unsigned char)(x & 0xff));
}

void vga_console_outzstring(const char *s)
{
    while (*s != '\0')
    {
        vga_console_putchar(*s);
        s++;
    }
}
