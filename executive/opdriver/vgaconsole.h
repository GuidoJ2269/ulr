/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 * NON INFRINGEMENT.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _VGA_CONSOLE_H
#define _VGA_CONSOLE_H

#include <stdint.h>

/* dimensions of the console */
#define VGA_CONSOLE_WIDTH 80
#define VGA_CONSOLE_HEIGHT 25

/* text mode console colors */
#define VGA_COLOR_BLACK        0
#define VGA_COLOR_BLUE         1
#define VGA_COLOR_GREEN        2
#define VGA_COLOR_CYAN         3
#define VGA_COLOR_RED          4
#define VGA_COLOR_MAGENTA      5
#define VGA_COLOR_BROWN        6
#define VGA_COLOR_LIGHT_GREY   7
#define VGA_COLOR_DARK_GREY    8
#define VGA_COLOR_LIGHT_BLUE   9
#define VGA_COLOR_LIGHT_GREEN 10
#define VGA_COLOR_LIGHT_CYAN  11
#define VGA_COLOR_LIGHT_RED   12
#define VGA_COLOR_PINK        13
#define VGA_COLOR_YELLOW      14
#define VGA_COLOR_WHITE       15

extern uint16_t vga_console_color;
extern unsigned int vga_console_xpos;
extern unsigned int vga_console_ypos;

/* initialize the VGA console driver */
extern void vga_console_init(void);

/* write a character to the current cursor position and move the cursor forward */
extern void vga_console_putchar(const char c);

/* set the current foreground and background colors */
extern int vga_console_setcolor(const unsigned int fg, const unsigned int bg);

/* several output routines for convenience */
extern void vga_console_outblank(void);
extern void vga_console_outbyte(const unsigned char c);
extern void vga_console_outchar(const char c);
extern void vga_console_outcrlf(void);
extern void vga_console_outcstring(const char *s);
extern void vga_console_outhex(const unsigned int x);
extern void vga_console_outzstring(const char *s);

#endif /* _VGA_CONSOLE_H */
