/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ldat.h>
#include <smp.h>
#include <spinlocks.h>
#include <vms_macros.h>

void synch_acquire(enum _spl_index index)
{
    struct _spl *spinlock = &smp$ar_spnlkvec[index];
    synch_acquirel(spinlock);
}

void synch_acquirel(struct _spl *spinlock)
{
    set_ipl(spinlock->spl$b_ipl);
    struct _cpu * current_cpu = find_cpu_data();
    for(;;)
    {
        if (atomic_bit_test_and_set(&spinlock->spl$b_spinlock))
        {
            if (spinlock->spl$l_own_cpu == current_cpu)
            {
                break;
            }
            else
            {
                while (spinlock->spl$b_spinlock)
                {
                    cpu_relax();
                }
            }
        }
        else
        {
            spinlock->spl$l_own_cpu = current_cpu;
            break;
        }
    }
    spinlock->spl$w_own_cnt++;
}

void synch_restore(enum _spl_index index)
{
    struct _spl *spinlock = &smp$ar_spnlkvec[index];
    synch_restorel(spinlock);
}

void synch_restorel(struct _spl *spinlock)
{
    spinlock->spl$w_own_cnt--;
    if (spinlock->spl$w_own_cnt == 0)
    {
        spinlock->spl$l_own_cpu = 0;
        spinlock->spl$b_spinlock = 0;
    }
}

void synch_release(enum _spl_index index)
{
    struct _spl *spinlock = &static_spinlock_table[index];
    synch_releasel(spinlock);
}

void synch_releasel(struct _spl *spinlock)
{
    spinlock->spl$w_own_cnt = 0;
    spinlock->spl$l_own_cpu = 0;
    spinlock->spl$b_spinlock = 0;
}
