/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ldat.h>

struct _spl static_spinlock_table[SPNLKCNT] =
{
    {0, IPL$_QUEUEAST, SPL$C_RANK_QUEUEAST, SPL$C_FORKLOCK, 0, 0, 0},
    {0, IPL$_FILSYS, SPL$C_RANK_FILSYS, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_TIMER, SPL$C_RANK_TIMER, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_JIB, SPL$C_RANK_JIB, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_MMG, SPL$C_RANK_MMG, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_SCHED, SPL$C_RANK_SCHED, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_MAILBOX, SPL$C_RANK_MAILBOX, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_POOL, SPL$C_RANK_POOL, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_HWCLK, SPL$C_RANK_HWCLK, SPL$C_STATICLOCK, 0, 0, 0},
    {0, IPL$_MEGA, SPL$C_RANK_MEGA, SPL$C_STATICLOCK, 0, 0, 0}
};
