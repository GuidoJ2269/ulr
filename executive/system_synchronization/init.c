/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <doinit.h>
#include <mutex.h>
#include <ldat.h>
#include <spinlocks.h>
#include <system_data_cells.h>
#include <system_routines.h>

void init_system_synchronization(void);

struct _inivec ini$a_vector_table[] = {{init_system_synchronization, {{0}}}, {0, {{0}}}};

void init_system_synchronization(void)
{
    smp$ar_spnlkvec = static_spinlock_table;
    smp$gw_spnlkcnt = SPNLKCNT;

    base_image.smp_acquire = synch_acquire;
    base_image.smp_acquirel = synch_acquirel;
    base_image.smp_restore = synch_restore;
    base_image.smp_restorel = synch_restorel;
    base_image.smp_release = synch_release;
    base_image.smp_releasel = synch_releasel;
}
