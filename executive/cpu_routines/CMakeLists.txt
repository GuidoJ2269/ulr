# the cpu routines target
set(cpu_routines_SRCS
    ${COMMON_DIR}/doinit.c
    init.c
)

set(cpu_routines_LAYOUT ${CONFIG_DIR}/${ARCH}/cpu_routines.ld)

add_library(cpu_routines SHARED ${cpu_routines_SRCS})

set_target_properties(cpu_routines PROPERTIES LINK_FLAGS "-T ${cpu_routines_LAYOUT} -z common-page-size=16 -z max-page-size=4096")

target_link_libraries(cpu_routines arch_${ARCH} -Wl,-R base_image)

# install
install(TARGETS cpu_routines DESTINATION ${PROJECT_SOURCE_DIR}/iso/vms\$common/sys\$ldr)
