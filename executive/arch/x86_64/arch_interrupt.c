/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifdef BOOTLOADER
#include <boo_routines.h>
#endif

#include <lapic.h>
#include <interrupt.h>
#include <smp.h>
#include <system_routines.h>
#include <util.h>
#include <vms_macros.h>

interrupt_handler interrupt_handler_vector[MAX_NUM_INTERRUPT_GATES];

/* local function prototypes */
void dump_interrupt_stack_frame(struct interrupt_stack_frame *frame);
void default_exception_handler(char *msg, uint64_t err, struct interrupt_stack_frame *frame);

void de_divide_by_zero_error(uint64_t err, struct interrupt_stack_frame *frame);
void db_debug(uint64_t err, struct interrupt_stack_frame *frame);
void nmi_non_maskable_interrupt(uint64_t err, struct interrupt_stack_frame *frame);
void bp_breakpoint(uint64_t err, struct interrupt_stack_frame *frame);
void of_overflow(uint64_t err, struct interrupt_stack_frame *frame);
void br_bound_range_exceeded(uint64_t err, struct interrupt_stack_frame *frame);
void ud_invalid_opcode(uint64_t err, struct interrupt_stack_frame *frame);
void nm_device_not_available(uint64_t err, struct interrupt_stack_frame *frame);
void df_double_fault(uint64_t err, struct interrupt_stack_frame *frame);
void ts_invalid_tss(uint64_t err, struct interrupt_stack_frame *frame);
void np_segment_not_present(uint64_t err, struct interrupt_stack_frame *frame);
void ss_stack_segment_fault(uint64_t err, struct interrupt_stack_frame *frame);
void gp_general_protection_fault(uint64_t err, struct interrupt_stack_frame *frame);
void pf_page_fault(uint64_t err, struct interrupt_stack_frame *frame);
void mf_x87_floating_point_exception(uint64_t err, struct interrupt_stack_frame *frame);
void ac_alignment_check(uint64_t err, struct interrupt_stack_frame *frame);
void mc_machine_check(uint64_t err, struct interrupt_stack_frame *frame);
void xm_simd_floating_point_exception(uint64_t err, struct interrupt_stack_frame *frame);
void ve_virtualization_exception(uint64_t err, struct interrupt_stack_frame *frame);
void sx_security_exception(uint64_t err, struct interrupt_stack_frame *frame);

void inter_cpu_interrupt(uint64_t err, struct interrupt_stack_frame *frame);
void timer_interrupt(uint64_t err, struct interrupt_stack_frame *frame);
void smp_error_interrupt(uint64_t err, struct interrupt_stack_frame *frame);
void spurious_interrupt(uint64_t err, struct interrupt_stack_frame *frame);

void isr_common_handler(struct interrupt_stack_frame *frame)
{
    interrupt_handler handler = interrupt_handler_vector[frame->isr_nr];
    if (handler != 0)
    {
        handler(frame->error_code, frame);
    }
    else
    {
        default_exception_handler("Unhandled exception:", frame->isr_nr, frame);
    }
    lapic_ack_interrupt();
}

void isr_register_handler(unsigned int n, interrupt_handler handler)
{
    interrupt_handler_vector[n] = handler;
}

void dump_interrupt_stack_frame(struct interrupt_stack_frame *frame)
{
#ifdef BOOTLOADER
    boo$kprintf("RAX=%x RBX=%x RCX=%x RDX=%x\n", frame->rax, frame->rbx, frame->rcx, frame->rdx);
    boo$kprintf("RSI=%x RDI=%x RBP=%x RFL=%x\n", frame->rsi, frame->rdi, frame->rbp, frame->rflags);
    boo$kprintf("CS:RIP=%x:%x SS:RSP=%x:%x\n", frame->cs, frame->rip, frame->ss, frame->rsp);
    boo$kprintf("R8=%x R9=%x R10=%x R11=%x\n", frame->r8, frame->r9, frame->r10, frame->r11);
    boo$kprintf("R12=%x R13=%x R14=%x R15=%x\n", frame->r12, frame->r13, frame->r14, frame->r15);
#else
    exe$kprintf("RAX=%x RBX=%x RCX=%x RDX=%x\n", frame->rax, frame->rbx, frame->rcx, frame->rdx);
    exe$kprintf("RSI=%x RDI=%x RBP=%x RFL=%x\n", frame->rsi, frame->rdi, frame->rbp, frame->rflags);
    exe$kprintf("CS:RIP=%x:%x SS:RSP=%x:%x\n", frame->cs, frame->rip, frame->ss, frame->rsp);
    exe$kprintf("R8=%x R9=%x R10=%x R11=%x\n", frame->r8, frame->r9, frame->r10, frame->r11);
    exe$kprintf("R12=%x R13=%x R14=%x R15=%x\n", frame->r12, frame->r13, frame->r14, frame->r15);
#endif
}

void default_exception_handler(char *msg, uint64_t err, struct interrupt_stack_frame *frame)
{
#ifdef BOOTLOADER
    boo$kprintf("\n!!! %s %x !!!\n", msg, err);
#else
    exe$kprintf("\n!!! %s %x !!!\n", msg, err);
#endif
    dump_interrupt_stack_frame(frame);
    cpu_halt();
}

void de_divide_by_zero_error(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("de_divide_by_zero_error:", err, frame);
}

void db_debug(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("db_debug:", err, frame);
}

void nmi_non_maskable_interrupt(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("nmi_non_maskable_interrupt:", err, frame);
}

void bp_breakpoint(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("bp_breakpoint:", err, frame);
}

void of_overflow(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("of_overflow:", err, frame);
}

void br_bound_range_exceeded(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("br_bound_range_exceeded:", err, frame);
}

void ud_invalid_opcode(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("ud_invalid_opcode:", err, frame);
}

void nm_device_not_available(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("nm_device_not_available:", err, frame);
}

void df_double_fault(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("df_double_fault:", err, frame);
}

void ts_invalid_tss(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("ts_invalid_tss:", err, frame);
}

void np_segment_not_present(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("np_segment_not_present:", err, frame);
}

void ss_stack_segment_fault(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("ss_stack_segment_fault:", err, frame);
}

void gp_general_protection_fault(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("gp_general_protection_fault:", err, frame);
}

void pf_page_fault(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("pf_page_fault:", err, frame);
}

void mf_x87_floating_point_exception(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("mf_x87_floating_point_exception:", err, frame);
}

void ac_alignment_check(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("ac_alignment_check:", err, frame);
}

void mc_machine_check(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("mc_machine_check:", err, frame);
}

void xm_simd_floating_point_exception(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("xm_simd_floating_point_exception:", err, frame);
}

void ve_virtualization_exception(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("ve_virtualization_exception:", err, frame);
}

void sx_security_exception(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("sx_security_exception:", err, frame);
}

void timer_interrupt(uint64_t err, struct interrupt_stack_frame *frame)
{
    SUPPRESS_UNUSED_WARNING(err);
    SUPPRESS_UNUSED_WARNING(frame);
    static char ticker[] = "/-\\|";
    static int n = 0;
    exe$kprintf("\r%c", ticker[n++ & 0x03]);
}

void inter_cpu_interrupt(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("inter_cpu_interrupt:", err, frame);
}

void smp_error_interrupt(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("smp_error_interrupt:", err, frame);
}

void spurious_interrupt(uint64_t err, struct interrupt_stack_frame *frame)
{
    default_exception_handler("spurious_interrupt:", err, frame);
}

void isr_setup(void)
{
    cpu_set_gate(0, isr_handler_0, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(1, isr_handler_1, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(2, isr_handler_2, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(3, isr_handler_3, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(4, isr_handler_4, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(5, isr_handler_5, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(6, isr_handler_6, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(7, isr_handler_7, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(8, isr_handler_8, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(9, isr_handler_9, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(10, isr_handler_10, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(11, isr_handler_11, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(12, isr_handler_12, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(13, isr_handler_13, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(14, isr_handler_14, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(15, isr_handler_15, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(16, isr_handler_16, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(17, isr_handler_17, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(18, isr_handler_18, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(19, isr_handler_19, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(20, isr_handler_20, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(21, isr_handler_21, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(22, isr_handler_22, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(23, isr_handler_23, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(24, isr_handler_24, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(25, isr_handler_25, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(26, isr_handler_26, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(27, isr_handler_27, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(28, isr_handler_28, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(29, isr_handler_29, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(30, isr_handler_30, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(31, isr_handler_31, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(32, isr_handler_32, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(33, isr_handler_33, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(34, isr_handler_34, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(35, isr_handler_35, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(36, isr_handler_36, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(37, isr_handler_37, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(38, isr_handler_38, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(39, isr_handler_39, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(40, isr_handler_40, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(41, isr_handler_41, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(42, isr_handler_42, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(43, isr_handler_43, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(44, isr_handler_44, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(45, isr_handler_45, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(46, isr_handler_46, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(47, isr_handler_47, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(252, isr_handler_252, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(253, isr_handler_253, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(254, isr_handler_254, PL_KERNEL, GT_INTERRUPT);
    cpu_set_gate(255, isr_handler_255, PL_KERNEL, GT_INTERRUPT);
}

void interrupt_setup(void)
{
    pic_remap_irqs();
#ifndef BOOTLOADER
    lapic_setup();
#endif
    isr_setup();

    isr_register_handler(INT_DE_DIVIDE_BY_ZERO, de_divide_by_zero_error);
    isr_register_handler(INT_DB_DEBUG, db_debug);
    isr_register_handler(INT_NMI_NON_MASKABLE_INT, nmi_non_maskable_interrupt);
    isr_register_handler(INT_BP_BREAKPOINT, bp_breakpoint);
    isr_register_handler(INT_OF_OVERFLOW, of_overflow);
    isr_register_handler(INT_BR_BOUND_RANGE_EXCEEDED, br_bound_range_exceeded);
    isr_register_handler(INT_UD_INVALID_OPCODE, ud_invalid_opcode);
    isr_register_handler(INT_NM_DEVICE_NOT_AVAILABLE, nm_device_not_available);
    isr_register_handler(INT_DF_DOUBLE_FAULT, df_double_fault);
    isr_register_handler(INT_TS_INVALID_TSS, ts_invalid_tss);
    isr_register_handler(INT_NP_SEGMENT_NOT_PRESENT, np_segment_not_present);
    isr_register_handler(INT_SS_STACK_SEGMENT_FAULT, ss_stack_segment_fault);
    isr_register_handler(INT_GP_GENERAL_PROTECTION_FAULT, gp_general_protection_fault);
    isr_register_handler(INT_PF_PAGE_FAULT, pf_page_fault);
    isr_register_handler(INT_MF_X86_FLOATING_POINT, mf_x87_floating_point_exception);
    isr_register_handler(INT_AC_ALIGNMENT_CHECK, ac_alignment_check);
    isr_register_handler(INT_MC_MACHINE_CHECK, mc_machine_check);
    isr_register_handler(INT_XM_SIMD_FLOATING_POINT, xm_simd_floating_point_exception);
    isr_register_handler(INT_VE_VIRTUALIZATION, ve_virtualization_exception);
    isr_register_handler(INT_SX_SECURITY, sx_security_exception);
    isr_register_handler(INT_TIMER_INTERRUPT, timer_interrupt);
    isr_register_handler(INT_INTER_CPU_INTERRUPT, inter_cpu_interrupt);
    isr_register_handler(INT_SMP_ERROR, smp_error_interrupt);
    isr_register_handler(INT_SPURIOUS_INTERRUPT, spurious_interrupt);
}
