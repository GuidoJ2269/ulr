/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <io.h>
#include <lapic.h>
#include <system_routines.h>
#include <vms_macros.h>

/* Programmable Interrupt Controller */
#define PIC_MASTER_COMMAND  0x20    /* Master PIC Command Port */
#define PIC_SLAVE_COMMAND   0xa0    /* Slave PIC Command Port */
#define PIC_MASTER_DATA     0x21    /* Master PIC Data Port */
#define PIC_SLAVE_DATA      0xa1    /* Slave PIC Data Port */

#define PIC_CMD_READ_IRR    0x0a    /* read pending interrupt requests */
#define PIC_CMD_READ_ISR    0x0b    /* read in-service requests */
#define PIC_CMD_INIT        0x11    /* initialize the PIC */
#define PIC_CMD_EOI         0x20    /* end of interrupt */

/* Programmable Interval Timer */
#define PIT_CHANNEL_0       0x40    /* PIT channel 0 data port (read/write) */
#define PIT_CHANNEL_1       0x41    /* PIT channel 1 data port (read/write) */
#define PIT_CHANNEL_2       0x42    /* PIT channel 2 data port (read/write) */
#define PIT_COMMAND         0x43    /* PIT command/mode register (write only) */
#define PIT_GATE_CONTROL_2  0x61    /* PIT channel 2 gate control port (PC speaker) */

#define APIC_BASE_MSR 0x1b

/* APIC register offsets */
#define APIC_ID      0x0020     /* identification register */
#define APIC_VERSION 0x0030     /* version register */
#define APIC_TPRI    0x0080     /* task priority register */
#define APIC_EOI     0x00b0     /* end-of-interrupt */
#define APIC_SIVR    0x00f0     /* spurious interrupt vector register */
#define APIC_ESR     0x0280     /* error status register */
#define APIC_ICR0    0x0300     /* interrupt command register 0 */
#define APIC_ICR1    0x0310     /* interrupt command register 1 */
#define APIC_LVTT    0x0320     /* local vector table timer */
#define APIC_LINT0   0x0350     /* local vector table 0 */
#define APIC_LINT1   0x0360     /* local vector table 1 */
#define APIC_LVTE    0x0370     /* local vector table error */
#define APIC_ICRT    0x0380     /* initial count register timer */
#define APIC_CCRT    0x0390     /* initial count register timer */
#define APIC_TDCR    0x03e0     /* timer divide configuration register */

#define APIC_SIVR_ENABLE    0x0100      /* APIC enable */
#define APIC_SIVR_FOCUS     0x0200      /* focus checking enable */

#define APIC_LVT_MASK       0x010000    /* interrupt is masked */
#define APIC_LVT_PERIODIC   0x020000    /* fire timer interrupt periodically */

/* timer division configuration values */
#define APIC_TDCR_2         0x00
#define APIC_TDCR_4         0x01
#define APIC_TDCR_8         0x02
#define APIC_TDCR_16        0x03
#define APIC_TDCR_32        0x08
#define APIC_TDCR_64        0x09
#define APIC_TDCR_128       0x0a
#define APIC_TDCR_1         0x0b

#define APIC_VECTOR_MASK    0xffffff00

/* private function prototypes */
addr_t lapic_get_base(void);
void lapic_set_base(addr_t base);
void lapic_calibrate_timer(addr_t base);
uint32_t lapic_read(addr_t base, unsigned int offset);
void lapic_write(addr_t base, unsigned int offset, uint32_t value);

void pic_remap_irqs(void)
{
    /* remap IRQ 0-15 (0x00-0x0f) to 32-47 (0x20-0x2f) */
    /* setup master and slave PICs */
    outb(PIC_CMD_INIT, PIC_MASTER_COMMAND);
    outb(PIC_CMD_INIT, PIC_SLAVE_COMMAND);
    outb(0x20, PIC_MASTER_DATA);    /* first master IRQ at 32 */
    outb(0x28, PIC_SLAVE_DATA);     /* first slave IRQ at 40 */
    outb(0x04, PIC_MASTER_DATA);    /* connect slave on IRQ2 */
    outb(0x02, PIC_SLAVE_DATA);     /* connect to master's IRQ2 */
    outb(0x01, PIC_MASTER_DATA);    /* expect normal EOI (not auto) */
    outb(0x01, PIC_SLAVE_DATA);     /* expect normal EOI (not auto) */
    /* mask all IRQ's except slave PIC line */
    outb(0xfb, PIC_MASTER_DATA);
    outb(0xff, PIC_SLAVE_DATA);
}

addr_t lapic_get_base(void)
{
    uint32_t lo;
    uint32_t hi;
    cpu_read_msr(APIC_BASE_MSR, lo, hi);
    return ((addr_t)lo & 0xfffff000) | ((addr_t)hi << 32);
}

void lapic_set_base(addr_t base)
{
    /* note that "base" must be the physical address! */
    base |= 0x0800;
    cpu_write_msr(APIC_BASE_MSR, base & 0xfffff800, (base >> 32) & 0xffffffff);
}

#ifdef BOOTLOADER
void lapic_map_base(addr_t *base)
{
    *base = (addr_t)mmu_map_system_data((void *)lapic_get_base(), 1);
}
#endif

void lapic_calibrate_timer(addr_t base)
{
    /* set timer division to 1 */
    lapic_write(base, APIC_TDCR, APIC_TDCR_1);
    /* enable channel 2 (speaker) */
    outb((inb(PIT_GATE_CONTROL_2) & 0xfc) | 0x01, PIT_GATE_CONTROL_2);
    /* set channel 2 in one-shot mode */
    outb(0xb2, PIT_COMMAND);
    /* initialize to 10ms: 1193180Hz * 0.010s ticks */
    outb(11932 & 0xff, PIT_CHANNEL_2);
    outb((11932 >> 8) & 0xff, PIT_CHANNEL_2);
    /* start one-shot timer */
    uint8_t x = inb(PIT_GATE_CONTROL_2) & 0xfc;
    outb(x, PIT_GATE_CONTROL_2);
    outb(x | 0x01, PIT_GATE_CONTROL_2);
    /* set the  initial count of the APIC timer to max */
    lapic_write(base, APIC_ICRT, 0xffffffff);
    /* wait for the one-shot timer to finish */
    while (!(inb(PIT_GATE_CONTROL_2) & 0x20));
    /* mask the timer interrupt */
    lapic_write(base, APIC_LVTT, APIC_LVT_MASK);
    /* now calculate the number of ticks per second */
    find_cpu_data()->cpu$l_lapic_ticks = (0xffffffff - lapic_read(base, APIC_CCRT)) * 100;
    /* disable channel 2 (speaker) */
    outb(inb(PIT_GATE_CONTROL_2) & 0xfc, PIT_GATE_CONTROL_2);
    /* set timer division to 1 */
    lapic_write(base, APIC_TDCR, APIC_TDCR_1);

    //exe$kprintf("Ticks/s=%d\n", find_cpu_data()->cpu$l_lapic_ticks);
}

/* TODO: note that this should be done per CPU ... */
void lapic_setup(void)
{
    addr_t base = find_cpu_data()->cpu$l_lapic_base;
    lapic_calibrate_timer(base);

    /* set timer interrupt vector to 0xfc */
    lapic_write(base, APIC_LVTT, APIC_LVT_MASK | 0xfc);
    /* set spurious interrupt vector to 0xff */
    lapic_write(base, APIC_SIVR, (lapic_read(base, APIC_SIVR) & APIC_VECTOR_MASK) | APIC_SIVR_ENABLE | 0xff);
    /* set error interrupt vector to 0xfe */
    lapic_write(base, APIC_LVTE, (lapic_read(base, APIC_LVTE) & APIC_VECTOR_MASK) | 0xfe);
    /* accept all interrupts */
    lapic_write(base, APIC_TPRI, lapic_read(base, APIC_TPRI) & APIC_VECTOR_MASK);

    lapic_ack_interrupt();
}

uint32_t lapic_read(addr_t base, unsigned int offset)
{
    return *((uint32_t *)(base + offset));
}

void lapic_write(addr_t base, unsigned int offset, uint32_t value)
{
    *((uint32_t *)(base + offset)) = value;
}

void lapic_ack_interrupt(void)
{
    lapic_write(find_cpu_data()->cpu$l_lapic_base, APIC_EOI, 0);
}

void lapic_set_timer(uint64_t timeout)
{
    addr_t base = find_cpu_data()->cpu$l_lapic_base;
    uint32_t ticks = (uint32_t)((timeout * find_cpu_data()->cpu$l_lapic_ticks / 1000000) & 0xffffffff);
    disable_interrupts(IPL$_TIMER);
    /* mask timer interrupt */
    lapic_write(base, APIC_LVTT, lapic_read(base, APIC_LVTT) | APIC_LVT_MASK);
    /* set initial count */
    lapic_write(base, APIC_ICRT, ticks);
    /* unmask timer interrupt */
    lapic_write(base, APIC_LVTT, (lapic_read(base, APIC_LVTT) & (uint32_t)~APIC_LVT_MASK) | APIC_LVT_PERIODIC);
    /* set timer division to 1 */
    lapic_write(base, APIC_TDCR, APIC_TDCR_1);
    enable_interrupts();
}
