/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <allocpfn.h>
#include <cpu.h>
#include <system_routines.h>
#include <util.h>

#ifdef BOOTLOADER
#include <boo_routines.h>
#endif

#define NUM_PGD_ENTRIES(x) (PGD_OFFSET((x) - 1) + 1)
#define NUM_PDP_ENTRIES(x) ((NUM_PGD_ENTRIES(x) > 1) ? (ADDRESSES_PER_PAGE) : (PDP_OFFSET((x) - 1) + 1))
#define NUM_PD_ENTRIES(x)  ((NUM_PDP_ENTRIES(x) > 1) ? (ADDRESSES_PER_PAGE) : (PD_OFFSET((x) - 1) + 1))

#ifdef BOOTLOADER
addr_t mmu_mapped_system_data_start = 0;    /* virtual address of the current first page of system data mapped into the non-paged data area */

union pgd_entry *mmu_pgd = 0;               /* page global directory for lvl 4 page mapping */
union pdp_entry *mmu_pdp = 0;               /* page directory pointer */
union pd2m_entry *mmu_pd2m = 0;             /* page directory with 2MiB pages */
union pt_entry *mmu_boot_4k_pages = 0;      /* space for 4k page tables */
unsigned int mmu_boot_used_4k_pages = 0;    /* # of 4k page tables in use */
#endif
union pt_entry *mmu_4k_pages = 0;       /* space for 4k page tables */
union pt_entry *mmu_phys_4k_pages = 0;  /* physical address of 4k page tables */
unsigned int mmu_used_4k_pages = 0;     /* # of 4k page tables in use */

/* private function prototypes */
void mmu_init_pgd_entry(union pgd_entry *pgd, union pdp_entry *pdp);
void mmu_init_pdp_entry(union pdp_entry *pdp, union pd2m_entry *pd);
void mmu_init_pd_entry(union pd2m_entry *pd, addr_t addr);
void mmu_init_pt_entry(union pt_entry *pt, addr_t addr);
union pd4k_entry *mmu_split_large_page(union pd4k_entry *pd, addr_t vaddr);
union pt_entry *mmu_get_page_table_entry(union pgd_entry *pgd, addr_t vaddr);

void mmu_init_pgd_entry(union pgd_entry *pgd, union pdp_entry *pdp)
{
    pgd->entry = 0;
    pgd->present = 1;
    pgd->read_write = 1;
    pgd->user_super = 1;
    pgd->base_31_12 = ((unsigned long)pdp >> 12) & 0x000fffff;
    pgd->base_51_32 = ((unsigned long)pdp >> 32) & 0x000fffff;
}

void mmu_init_pdp_entry(union pdp_entry *pdp, union pd2m_entry *pd)
{
    pdp->entry = 0;
    pdp->present = 1;
    pdp->read_write = 1;
    pdp->user_super = 1;
    pdp->base_31_12 = ((unsigned long)pd >> 12) & 0x000fffff;
    pdp->base_51_32 = ((unsigned long)pd >> 32) & 0x000fffff;
}

void mmu_init_pd_entry(union pd2m_entry *pd, addr_t addr)
{
    pd->entry = 0;
    pd->present = 1;
    pd->read_write = 1;
    pd->user_super = 1;
    pd->page_size = 1; /* initially 2Mb pages */
    pd->base_31_21 = (addr >> 21) & 0x000007ff;
    pd->base_51_32 = (addr >> 32) & 0x000fffff;
}

void mmu_init_pt_entry(union pt_entry *pt, addr_t addr)
{
    pt->entry = 0;
    pt->present = 1;
    pt->read_write = 1;
    pt->user_super = 1;
    pt->base_31_12 = (addr >> 12) & 0x000fffff;
    pt->base_51_32 = (addr >> 32) & 0x000fffff;
}

union pd4k_entry *mmu_split_large_page(union pd4k_entry *pd, addr_t vaddr)
{
    void *page_ptr = PAGE_2M_PTR((union pd2m_entry *)pd, PD_OFFSET(vaddr));
    union pd4k_entry pd4k = pd[PD_OFFSET(vaddr)];
    union pt_entry *pt = 0;
    union pt_entry *phys_pt = 0;

#ifdef BOOTLOADER
    if (mmu_boot_used_4k_pages < MAX_4K_PAGE_TABLES)
    {
        pt = &mmu_boot_4k_pages[ADDRESSES_PER_PAGE * mmu_boot_used_4k_pages];
        phys_pt = &mmu_phys_4k_pages[ADDRESSES_PER_PAGE * mmu_boot_used_4k_pages];
        mmu_boot_used_4k_pages++;
    }
    else
    {
        /* TODO: panic! */
        boo$kprintf("Max # of 4k pages (%d) reached!\n", MAX_4K_PAGE_TABLES);
        cpu_halt();
    }
#else
    if (mmu_used_4k_pages < MAX_4K_PAGE_TABLES)
    {
        pt = &mmu_4k_pages[ADDRESSES_PER_PAGE * mmu_used_4k_pages];
        phys_pt = &mmu_phys_4k_pages[ADDRESSES_PER_PAGE * mmu_used_4k_pages];
        mmu_used_4k_pages++;
    }
    else
    {
        /* TODO: panic! */
        exe$kprintf("Max # of 4k pages (%d) reached!\n", MAX_4K_PAGE_TABLES);
        cpu_halt();
    }
#endif

    for (unsigned int i = 0; i < ADDRESSES_PER_PAGE; i++)
    {
        mmu_init_pt_entry(&pt[i], (unsigned long)page_ptr);
        if ((page_ptr != 0) || (vaddr == 0))
        {
            /* unless the original page refers to null */
            page_ptr += PAGE_SIZE;
        }
    }

    pd4k.page_size = 0;
    pd4k.base_31_12 = ((unsigned long)phys_pt >> 12) & 0x000fffff;
    pd4k.base_51_32 = ((unsigned long)phys_pt >> 32) & 0x000fffff;
    pd[PD_OFFSET(vaddr)] = pd4k;

    return pd;
}

union pt_entry *mmu_get_page_table_entry(union pgd_entry *pgd, addr_t vaddr)
{
    union pdp_entry  *pdp = PDP_ENTRY_PTR(pgd, PGD_OFFSET(vaddr));
    union pd4k_entry *pd = PD4K_ENTRY_PTR(pdp, PDP_OFFSET(vaddr));
    union pt_entry   *pt = 0;

    /* check for a 2M page and if so split it up */
    if (pd[PD_OFFSET(vaddr)].page_size == 1)
    {
        pd = mmu_split_large_page(pd, vaddr);
    }

    pt = PT_ENTRY_PTR(pd, PD_OFFSET(vaddr));
    return &pt[PT_OFFSET(vaddr)];
}

void mmu_set_access_page(union pgd_entry *pgd, addr_t vaddr, unsigned int rw, unsigned int us)
{
    union pt_entry *pt = mmu_get_page_table_entry(pgd, vaddr);

    pt->read_write = rw & 0x01;
    pt->user_super = us & 0x01;
}

void mmu_set_access_pages(union pgd_entry *pgd, addr_t vaddr, unsigned int rw, unsigned int us, size_t n)
{
    addr_t page_offset = 0;
    for (size_t i = 0; i < n; i++)
    {
        mmu_set_access_page(pgd, vaddr + page_offset, rw, us);
        page_offset += PAGE_SIZE;
    }
}

void mmu_map_page(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us)
{
    union pt_entry *pt = mmu_get_page_table_entry(pgd, vaddr);

    pt->present = 1;
    pt->read_write = rw & 0x01;
    pt->user_super = us & 0x01;
    pt->base_31_12 = (paddr >> 12) & 0x000fffff;
    pt->base_51_32 = (paddr >> 32) & 0x000fffff;
}

void mmu_map_pages(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us, size_t n)
{
    addr_t page_offset = 0;
    for (size_t i = 0; i < n; i++)
    {
        mmu_map_page(pgd, paddr + page_offset, vaddr + page_offset, rw, us);
        page_offset += PAGE_SIZE;
    }
}

void mmu_map_large_page(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us)
{
    union pdp_entry  *pdp = PDP_ENTRY_PTR(pgd, PGD_OFFSET(vaddr));
    union pd2m_entry *pd = PD2M_ENTRY_PTR(pdp, PDP_OFFSET(vaddr));

    pd->present = 1;
    pd->read_write = rw & 0x01;
    pd->user_super = us & 0x01;
    pd->page_size = 1;
    pd->base_31_21 = (paddr >> 21) & 0x000007ff;
    pd->base_51_32 = (paddr >> 32) & 0x000fffff;
}

void mmu_map_large_pages(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us, size_t n)
{
    addr_t page_offset = 0;
    for (size_t i = 0; i < n; i++)
    {
        mmu_map_large_page(pgd, paddr + page_offset, vaddr + page_offset, rw, us);
        page_offset += LARGE_PAGE_SIZE;
    }
}

void mmu_unmap_page(union pgd_entry *pgd, addr_t vaddr)
{
    mmu_map_page(pgd, 0, vaddr, 0, 0);
}

void mmu_unmap_pages(union pgd_entry *pgd, addr_t vaddr, size_t n)
{
    addr_t page_offset = 0;
    for (size_t i = 0; i < n; i++)
    {
        mmu_unmap_page(pgd, vaddr + page_offset);
        page_offset += PAGE_SIZE;
    }
}

void mmu_unmap_large_page(union pgd_entry *pgd, addr_t vaddr)
{
    mmu_map_large_page(pgd, 0, vaddr, 0, 0);
}

void mmu_unmap_large_pages(union pgd_entry *pgd, addr_t vaddr, size_t n)
{
    addr_t page_offset = 0;
    for (size_t i = 0; i < n; i++)
    {
        mmu_unmap_large_page(pgd, vaddr + page_offset);
        page_offset += LARGE_PAGE_SIZE;
    }
}

#ifdef BOOTLOADER
void mmu_setup_paging(size_t memory_size)
{
    mmu_phys_4k_pages = mmu_boot_4k_pages;

    /* set up the entire P0, P1 and S0 address space (canonical!) */
    for (unsigned long i = 0; i < MAX_PGD_ENTRIES / 2; i++)
    {
        mmu_init_pgd_entry(&mmu_pgd[i], &mmu_pdp[i * ADDRESSES_PER_PAGE]);
        mmu_init_pgd_entry(&mmu_pgd[ADDRESSES_PER_PAGE - i - 1], &mmu_pdp[(MAX_PGD_ENTRIES - i - 1) * ADDRESSES_PER_PAGE]);
        for (unsigned long j = 0; j < MAX_PDP_ENTRIES / 2; j++)
        {
            mmu_init_pdp_entry(&mmu_pdp[i * ADDRESSES_PER_PAGE + j], &mmu_pd2m[(i * ADDRESSES_PER_PAGE + j) * ADDRESSES_PER_PAGE]);
            mmu_init_pdp_entry(&mmu_pdp[(MAX_PGD_ENTRIES - i - 1) * ADDRESSES_PER_PAGE + ADDRESSES_PER_PAGE - j - 1],
                               &mmu_pd2m[(MAX_PDP_ENTRIES - j - 1) * ADDRESSES_PER_PAGE]);
            for (unsigned long k = 0; k < MAX_PD_ENTRIES; k++)
            {
                /* initialize to 2Mb pages; no pt_entries required */
                if ((j * ADDRESSES_PER_PAGE + k) < NUM_PD_ENTRIES(memory_size))
                {
                    /* identity map at most the entire P0 space */
                    mmu_init_pd_entry(&mmu_pd2m[j * ADDRESSES_PER_PAGE + k], (i << 39) + (j << 30) + (k << 21));
                }
                else
                {
                    /* map the rest of the P0 and P1 space to 0 */
                    mmu_init_pd_entry(&mmu_pd2m[j * ADDRESSES_PER_PAGE + k], 0);
                }
                /* map S0 space to 0 */
                mmu_init_pd_entry(&mmu_pd2m[(MAX_PDP_ENTRIES - j - 1) * ADDRESSES_PER_PAGE + k], 0);
            }
        }
    }

    /* activate the new page tables */
    cpu_write_cr3(mmu_pgd);
}

void mmu_setup_4k_pages(void)
{
    mmu_4k_pages = mmu_boot_4k_pages;
    mmu_used_4k_pages = mmu_boot_used_4k_pages;
}

void * mmu_map_system_data(void *paddr, size_t n)
{
    mmu_mapped_system_data_start -= n * PAGE_SIZE;
    mmu_map_pages(mmu_pgd, (addr_t)paddr, mmu_mapped_system_data_start, 1, 0, n);
    return (void *)mmu_mapped_system_data_start;
}

void * mmu_reserve_system_space(size_t n)
{
    mmu_mapped_system_data_start -= n * PAGE_SIZE;
    mmu_map_pages(mmu_pgd, 0, mmu_mapped_system_data_start, 1, 0, n);
    return (void *)mmu_mapped_system_data_start;
}
#endif
