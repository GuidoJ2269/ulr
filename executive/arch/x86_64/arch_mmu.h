/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_X86_64_MMU_H
#define _ARCH_X86_64_MMU_H

#include <page.h>
#include <stddef.h>

#define ADDRESSES_PER_PAGE (PAGE_SIZE / sizeof(addr_t))

#define MAX_PGD_ENTRIES (2 * (PGD_OFFSET(P1_SPACE_END) + 1))
#define MAX_PDP_ENTRIES ((MAX_PGD_ENTRIES > 2) ? (ADDRESSES_PER_PAGE) : (2 * (PDP_OFFSET(P1_SPACE_END) + 1)))
#define MAX_PD_ENTRIES  ((MAX_PDP_ENTRIES > 2) ? (ADDRESSES_PER_PAGE) : (2 * (PD_OFFSET(P1_SPACE_END) + 1)))

#define MAX_4K_PAGE_TABLES 32   /* allow for some of the 2Mb pages to be split up into 4kb pages */

typedef unsigned long addr_t;

enum memory_status
{
    MS_FREE,
    MS_USED,
    MS_RESERVED,
    MS_UNUSABLE
};

struct memory_area
{
    addr_t address;
    size_t size;
    enum memory_status status;
};

#ifdef BOOTLOADER
extern union pgd_entry *mmu_pgd;        /* page global directory for lvl 4 page mapping */
extern union pdp_entry *mmu_pdp;        /* page directory pointer */
extern union pd2m_entry *mmu_pd2m;      /* page directory with 2MiB pages */
extern union pt_entry *mmu_boot_4k_pages;    /* space for 4k page tables */

extern void mmu_setup_paging(size_t memory_size);
extern void mmu_setup_4k_pages(void);
extern void * mmu_map_system_data(void *paddr, size_t n);
extern void * mmu_reserve_system_space(size_t n);
#endif

extern void mmu_set_access_page(union pgd_entry *pgd, addr_t vaddr, unsigned int rw, unsigned int us);
extern void mmu_set_access_pages(union pgd_entry *pgd, addr_t vaddr, unsigned int rw, unsigned int us, size_t n);

extern void mmu_map_page(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us);
extern void mmu_map_pages(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us, size_t n);

extern void mmu_map_large_page(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us);
extern void mmu_map_large_pages(union pgd_entry *pgd, addr_t paddr, addr_t vaddr, unsigned int rw, unsigned int us, size_t n);

extern void mmu_unmap_page(union pgd_entry *pgd, addr_t vaddr);
extern void mmu_unmap_pages(union pgd_entry *pgd, addr_t vaddr, size_t n);

extern void mmu_unmap_large_page(union pgd_entry *pgd, addr_t vaddr);
extern void mmu_unmap_large_pages(union pgd_entry *pgd, addr_t vaddr, size_t n);

#endif /* _ARCH_X86_64_MMU_H */
