/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <cpu.h>
#include <mmu.h>
#include <smp.h>
#include <system_data_cells.h>
#include <trace.h>

#ifdef BOOTLOADER
#include <boo_routines.h>
#endif

#define MP_SIGNATURE 0x5f504d5f

#define NUM_mp_fps_areaS 2

struct mp_fps
{
    uint32_t    signature;
    uint32_t    cth_ptr;
    uint8_t     length;
    uint8_t     version;
    uint8_t     checksum;
    uint8_t     mp_features1;
    uint8_t     mp_features2;
    uint8_t     mp_features3;
    uint8_t     mp_features4;
    uint8_t     mp_features5;
} __attribute__((packed));

struct mp_cth
{
    uint32_t    signature;
    uint16_t    base_length;
    uint8_t     spec_revision;
    uint8_t     checksum;
    char        oem_id[8];
    char        product_id[12];
    uint32_t    oem_table_ptr;
    uint16_t    oem_table_size;
    uint16_t    entry_count;
    uint32_t    lapic_addr;
    uint16_t    extended_table_length;
    uint8_t     extended_table_checksum;
    uint8_t     reserved;
} __attribute__((packed));

struct mp_cpu
{
    uint8_t     type;           /* must be 0 */
    uint8_t     apic_id;
    uint8_t     apic_version;
    uint8_t     flags;
    uint32_t    signature;
    uint32_t    features;
    uint8_t     reserved[8];
};

struct mp_bus
{
    uint8_t     type;           /* must be 1 */
    uint8_t     id;
    char        bus_type[6];
};

struct mp_ioapic
{
    uint8_t     type;           /* must be 2 */
    uint8_t     id;
    uint8_t     version;
    uint8_t     flags;
    uint32_t    addr;
};

#ifdef BOOTLOADER
unsigned int num_cpu = 0;
unsigned int num_bus = 0;
unsigned int num_ioapic = 0;
#endif

/* private functions */
#ifdef BOOTLOADER
struct mp_fps * find_mp_fp_struct(void);
void smp_detect(void);
#endif

#ifdef BOOTLOADER
struct mp_fps * find_mp_fp_struct(void)
{
    static addr_t mp_fps_area[] = { 0x9fc00, 0xa0000, 0xf0000, 0x100000 };
    struct mp_fps *result = 0;
    int found = 0;
    for (unsigned int i = 0; (i < 2 * NUM_mp_fps_areaS) && !found; i += 2)
    {
        for (uint32_t *p = (uint32_t *)mp_fps_area[i]; (p < (uint32_t *)mp_fps_area[i+1]) && !found; p++)
        {
            found = (*p == MP_SIGNATURE);
            if (found)
            {
                result = (struct mp_fps *)p;
            }
        }
    }
    return result;
}

void smp_detect(void)
{
    struct mp_fps *mp_fp = find_mp_fp_struct();
    if (mp_fp != 0)
    {
        /* TODO: detect CPUs/cores */
        struct mp_cth *mp_cfg = (struct mp_cth *)((unsigned long)mp_fp->cth_ptr);
        uint8_t *entry_ptr = (uint8_t *)(unsigned long)(mp_fp->cth_ptr + sizeof(struct mp_cth));
        for (unsigned int i = 0; i < mp_cfg->entry_count; i++)
        {
            switch (*entry_ptr) {
            case 0:
                num_cpu++;
                entry_ptr += sizeof(struct mp_cpu);
                break;
            case 1:
                num_bus++;
                entry_ptr += sizeof(struct mp_bus);
                break;
            case 2:
                num_ioapic++;
                entry_ptr += sizeof(struct mp_ioapic);
                break;
            case 3:
                entry_ptr += 8;
                break;
            case 4:
                entry_ptr += 8;
                break;
            default:
                boo$kprintf("Unknown MP Configuration entry type %d: %x", i, *entry_ptr);
                cpu_halt();
                break;
            }
        }
        boo$kprintf("MP: %x %x %d %d %d %d\n", mp_fp, mp_cfg, mp_cfg->entry_count, num_cpu, num_bus, num_ioapic);
    }
}
#endif

void smp_setup(void)
{
#ifdef BOOTLOADER
    smp_detect();
#endif
}
