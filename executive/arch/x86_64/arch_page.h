/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_X86_64_PAGE_H
#define _ARCH_X86_64_PAGE_H

#include <stdint.h>

/* page map level 4 */
union pgd_entry
{
    uint64_t entry;
    struct
    {
        uint64_t present: 1;
        uint64_t read_write: 1;
        uint64_t user_super: 1;
        uint64_t write_through: 1;
        uint64_t cache_disable: 1;
        uint64_t accessed: 1;
        uint64_t zero:3;
        uint64_t os_defined:3;
        uint64_t base_31_12: 20;
        uint64_t base_51_32: 20;
        uint64_t available: 11;
        uint64_t no_execute: 1;
    };
} __attribute__((packed));

/* page directory pointer */
union pdp_entry
{
    uint64_t entry;
    struct
    {
        uint64_t present: 1;
        uint64_t read_write: 1;
        uint64_t user_super: 1;
        uint64_t write_through: 1;
        uint64_t cache_disable: 1;
        uint64_t accessed: 1;
        uint64_t ignored: 1;
        uint64_t zero: 2;
        uint64_t os_defined: 3;
        uint64_t base_31_12: 20;
        uint64_t base_51_32: 20;
        uint64_t available: 11;
        uint64_t no_execute: 1;
    };
} __attribute__((packed));

/* page directory for 2Mb pages */
union pd2m_entry
{
    uint64_t entry;
    struct
    {
        uint64_t present: 1;
        uint64_t read_write: 1;
        uint64_t user_super: 1;
        uint64_t write_through: 1;
        uint64_t cache_disable: 1;
        uint64_t accessed: 1;
        uint64_t dirty: 1;
        uint64_t page_size: 1; /* must be 1 for 2Mb pages */
        uint64_t global: 1;
        uint64_t os_defined: 3;
        uint64_t pageattrtable: 1;
        uint64_t reserved: 8;
        uint64_t base_31_21: 11;
        uint64_t base_51_32: 20;
        uint64_t available: 11;
        uint64_t no_execute: 1;
    };
} __attribute__((packed));

/* page directory for 4kb pages */
union pd4k_entry
{
    uint64_t entry;
    struct
    {
        uint64_t present: 1;
        uint64_t read_write: 1; /* 0 = readonly, 1, = readwrite */
        uint64_t user_super: 1; /* 0 = super, 1 = user */
        uint64_t write_through: 1;
        uint64_t cache_disable: 1;
        uint64_t accessed: 1;
        uint64_t ignored0: 1;
        uint64_t page_size: 1; /* must be 0 for 4kb pages */
        uint64_t ignored1: 1;
        uint64_t os_defined: 3;
        uint64_t base_31_12: 20;
        uint64_t base_51_32: 20;
        uint64_t available: 11;
        uint64_t no_execute: 1;
    };
} __attribute__((packed));

/* page table */
union pt_entry
{
    uint64_t entry;
    struct
    {
        uint64_t present: 1;
        uint64_t read_write: 1; /* 0 = readonly, 1, = readwrite */
        uint64_t user_super: 1; /* 0 = super, 1 = user */
        uint64_t write_through: 1;
        uint64_t cache_disable: 1;
        uint64_t accessed: 1;
        uint64_t dirty: 1;
        uint64_t pageattrtable: 1;
        uint64_t global: 1;
        uint64_t os_defined:3;
        uint64_t base_31_12: 20;
        uint64_t base_51_32: 20;
        uint64_t available: 11;
        uint64_t no_execute: 1;
    };
} __attribute__((packed));

#define PGD_OFFSET(x) (((x) >> 39) & 0x01ff)
#define PDP_OFFSET(x) (((x) >> 30) & 0x01ff)
#define PD_OFFSET(x) (((x) >> 21) & 0x01ff)
#define PT_OFFSET(x) (((x) >> 12) & 0x01ff)

#define PDP_ENTRY_PTR(p,x) ((union pdp_entry *)(((unsigned long)((p)[(x)].base_31_12) << 12) | ((unsigned long)((p)[(x)].base_51_32) << 32)))
#define PD2M_ENTRY_PTR(p,x) ((union pd2m_entry *)(((unsigned long)((p)[(x)].base_31_12) << 12) | ((unsigned long)((p)[(x)].base_51_32) << 32)))
#define PD4K_ENTRY_PTR(p,x) ((union pd4k_entry *)(((unsigned long)((p)[(x)].base_31_12) << 12) | ((unsigned long)((p)[(x)].base_51_32) << 32)))
#define PT_ENTRY_PTR(p,x) ((union pt_entry *)(((unsigned long)((p)[(x)].base_31_12) << 12) | ((unsigned long)((p)[(x)].base_51_32) << 32)))

#define PAGE_2M_PTR(p,x) ((void *)(((unsigned long)((p)[(x)].base_31_21) << 21) | ((unsigned long)((p)[(x)].base_51_32) << 32)))
#define PAGE_4K_PTR(p,x) ((void *)(((unsigned long)((p)[(x)].base_31_12) << 12) | ((unsigned long)((p)[(x)].base_51_32) << 32)))

typedef union pgd_entry l1pte;
typedef union pdp_entry l2pte;
typedef union pd2m_entry l3pte;
typedef union pt_entry l4pte;

#endif /* _ARCH_X86_64_PAGE_H */
