/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_X86_64_IO_H
#define _ARCH_X86_64_IO_H

#include <stdint.h>

/* basic I/O funtions */
#define inb(port) ({ \
        uint8_t _v; \
        asm volatile("inb %%dx,%%al":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inw(port) ({ \
        uint16_t _v; \
        asm volatile("inw %%dx,%%ax":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inl(port) ({ \
        uint32_t _v; \
        asm volatile("inl %%dx,%%eax":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inq(port) ({ \
        uint64_t _v; \
        asm volatile("inq %%dx,%%rax":"=a" (_v):"d" (port)); \
        _v; \
        })

#define outb(value,port) \
        asm("outb %%al,%%dx"::"a" (value),"d" (port))
#define outw(value,port) \
        asm("outw %%ax,%%dx"::"a" (value),"d" (port))
#define outl(value,port) \
        asm("outl %%eax,%%dx"::"a" (value),"d" (port))
#define outq(value,port) \
        asm("outq %%rax,%%dx"::"a" (value),"d" (port))

/* I/O funtions with pause (for slower hardware) */
#define inb_p(port) ({ \
        uint8_t _v; \
        __asm__ volatile ("inb %%dx,%%al\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inw_p(port) ({ \
        uint16_t_t _v; \
        __asm__ volatile ("inw %%dx,%%ax\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inl_p(port) ({ \
        uint32_t_t _v; \
        __asm__ volatile ("inl %%dx,%%eax\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:":"=a" (_v):"d" (port)); \
        _v; \
        })
#define inq_p(port) ({ \
        uint64_t_t _v; \
        __asm__ volatile ("inq %%dx,%%rax\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:":"=a" (_v):"d" (port)); \
        _v; \
        })

#define outb_p(value,port) \
        __asm__ ("outb %%al,%%dx\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:"::"a" (value),"d" (port))
#define outw_p(value,port) \
        __asm__ ("outw %%ax,%%dx\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:"::"a" (value),"d" (port))
#define outl_p(value,port) \
        __asm__ ("outl %%eax,%%dx\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:"::"a" (value),"d" (port))
#define outq_p(value,port) \
        __asm__ ("outq %%rax,%%dx\n" \
        "\tjmp 1f\n" \
        "1:\tjmp 1f\n" \
        "1:"::"a" (value),"d" (port))

#endif /* _ARCH_X86_64_IO_H */
