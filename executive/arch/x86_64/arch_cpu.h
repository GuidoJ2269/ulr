/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_X86_64_CPU_H
#define _ARCH_X86_64_CPU_H

#include <stdint.h>

/* Only x86_64 CPU support */

/* CR0 bits */
#define CR0_PE 0x00000001  /* RW: Protected mode enable */
#define CR0_MP 0x00000002  /* RW: Monitor FPU? If 1 then #NM exception may be generated */
#define CR0_EM 0x00000004  /* RW: Emulate FPU */
#define CR0_TS 0x00000008  /* RW: Task switched */
#define CR0_ET 0x00000010  /* RO: Exception type */
#define CR0_NE 0x00000020  /* RW: Numeric error */
#define CR0_WP 0x00010000  /* RW: Write protect for RO pages in supervisor mode */
#define CR0_AM 0x00040000  /* RW: Require data alignment */
#define CR0_NW 0x20000000  /* RW: IGNORED: Not writethrough */
#define CR0_CD 0x40000000  /* RW: Cache disable */
#define CR0_PG 0x80000000  /* RW: Paging enable */

/* CR3 bits */
#define CR3_PWT 0x00000008 /* RW: Page writethrough */
#define CR3_PCD 0x00000010 /* RW: Cache disable */

/* CR4 bits */
#define CR4_VME        0x0001   /* RW: Virtual-8086 enable */
#define CR4_PVI        0x0002   /* RW: Protected mode virtual interrupts */
#define CR4_TSD        0x0004   /* RW: Time stamp disable for usermode */
#define CR4_DE         0x0008   /* RW: Debug extensions */
#define CR4_PSE        0x0010   /* RW: Page size extensions */
#define CR4_PAE        0x0020   /* RW: Physical-address extensions */
#define CR4_MCE        0x0040   /* RW: Machine check enable */
#define CR4_PGE        0x0080   /* RW: Page-Global enable */
#define CR4_PCE        0x0100   /* RW: Performance monitoring counter enable */
#define CR4_OSFXSR     0x0200   /* RW: Operating system fxsave/fsrstor support */
#define CR4_OSXMMEXCPT 0x0400   /* RW: Operating system unmasked exception support */

/* EFER */
#define EFER       0xc0000080  /* EFER number for rsmsr/wrmsr */

#define EFER_SCE   0x00000001   /* RW: System call extensions */
#define EFER_LME   0x00000100   /* RW: Long mode enable */
#define EFER_LMA   0x00000400   /* RW: Long mode activated */
#define EFER_NXE   0x00000800   /* RW: No-execute bit enable */
#define EFER_FFXSR 0x00002000   /* RW: Fast fxsave/fxrstor */

/* gate types */
#define GT_TASK      0x05
#define GT_INTERRUPT 0x0e
#define GT_TRAP      0x0f

/* privilege levels */
#define PL_KERNEL     0
#define PL_EXECUTIVE  1
#define PL_SUPERVISOR 2
#define PL_USER       3

#define MAX_NUM_INTERRUPT_GATES 256

typedef uint64_t reg_t;

union gate_descriptor
{
    struct
    {
        uint64_t descriptor_127_64;
        uint64_t descriptor_63_00;
    } __attribute__((packed));
    struct
    {
        uint16_t offset_15_00;
        uint16_t selector;
        uint16_t ist: 3;
        uint16_t reserved0: 5;
        uint16_t type: 4;
        uint16_t zero0: 1;
        uint16_t dpl: 2;
        uint16_t present: 1;
        uint16_t offset_31_16;
        uint32_t offset_63_32;
        uint32_t reserved1: 8;
        uint32_t zero1: 5;
        uint32_t reserved2: 19;
    } __attribute__((packed));
};

/*
 * Segment descriptor used in the GDT, in 64 bit mode:
 *  - long_mode = 1
 *  - def_size = 0
 *  - present, descriptor priviledge level and conforming bit in the
 *    type field are used as normal
 *  - all other fields are ignored
 */
union segment_descriptor
{
    uint64_t descriptor;
    struct
    {
        uint16_t limit_15_00;
        uint16_t base_15_00;
        uint32_t base_23_16: 8;
        uint32_t accessed: 1;
        uint32_t read_write: 1;
        uint32_t direct_conform: 1;
        uint32_t executable: 1;
        uint32_t system: 1;
        uint32_t dpl: 2;
        uint32_t present: 1;
        uint32_t limit_19_16: 4;
        uint32_t avail: 1;
        uint32_t long_mode: 1;
        uint32_t def_size: 1;
        uint32_t granularity: 1;
        uint32_t base_31_24: 8;
    } __attribute__((packed));
};

struct system_segment_descriptor
{
    union segment_descriptor descriptor_63_00;
    union
    {
        uint64_t descriptor_127_64;
        struct
        {
            uint32_t base_63_32;
            uint32_t reserved;
        } __attribute__((packed));
    };
} __attribute__((packed));

struct tss
{
    uint32_t reserved0;
    uint64_t rsp0;
    uint64_t rsp1;
    uint64_t rsp2;
    uint64_t reserved1;
    uint64_t ist1;
    uint64_t ist2;
    uint64_t ist3;
    uint64_t ist4;
    uint64_t ist5;
    uint64_t ist6;
    uint64_t ist7;
    uint64_t reserved2;
    uint16_t reserved3;
    uint16_t io_map_base;
} __attribute__((packed));

struct descriptor_table_register {
        uint16_t size;
        uint64_t address;
} __attribute__((packed));

typedef void (*interrupt_service_routine)(void);

#define cpu_load_gdt(x) asm("lgdt %0;" : : "m"(x))
#define cpu_load_idt(x) asm("lidt %0;" : : "m"(x))
#define cpu_load_tr(x) asm("ltr %w0;" : : "r"(x))

#define cpu_write_cr3(x) \
    asm volatile("mov %0,%%cr3" : : "r"(x))

#define cpuid(num, eax, ebx, ecx, edx) \
    asm volatile("cpuid": "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx): "a"(num))

#define cpu_read_msr(msr, lo, hi) \
    asm volatile("rdmsr" : "=a"(lo), "=d"(hi): "c"(msr));

#define cpu_write_msr(msr, lo, hi) \
    asm volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));

#define cpu_halt() for(;;) asm volatile("hlt")
#define cpu_relax() asm volatile("pause")

#define cpu_disable_interrupts() asm volatile("cli")
#define cpu_enable_interrupts() asm volatile("sti")

#define atomic_bit_test_and_set(lock) \
    ({  uint8_t result; \
        asm volatile ("lock; bts $0, %0; setc %1" \
                      : "=m" (*lock), "=q" (result) \
                      : "m" (*lock)); \
        result; })

extern void cpu_set_gate(unsigned int n, interrupt_service_routine isr, unsigned int dpl, unsigned int type);

#endif /* _ARCH_X86_64_CPU_H */
