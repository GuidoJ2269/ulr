/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_X86_64_INTERRUPT_H
#define _ARCH_X86_64_INTERRUPT_H

#include <stdint.h>

#define INT_DE_DIVIDE_BY_ZERO             0
#define INT_DB_DEBUG                      1
#define INT_NMI_NON_MASKABLE_INT          2
#define INT_BP_BREAKPOINT                 3
#define INT_OF_OVERFLOW                   4
#define INT_BR_BOUND_RANGE_EXCEEDED       5
#define INT_UD_INVALID_OPCODE             6
#define INT_NM_DEVICE_NOT_AVAILABLE       7
#define INT_DF_DOUBLE_FAULT               8
#define INT_TS_INVALID_TSS               10
#define INT_NP_SEGMENT_NOT_PRESENT       11
#define INT_SS_STACK_SEGMENT_FAULT       12
#define INT_GP_GENERAL_PROTECTION_FAULT  13
#define INT_PF_PAGE_FAULT                14
#define INT_MF_X86_FLOATING_POINT        16
#define INT_AC_ALIGNMENT_CHECK           17
#define INT_MC_MACHINE_CHECK             18
#define INT_XM_SIMD_FLOATING_POINT       19
#define INT_VE_VIRTUALIZATION            20
#define INT_SX_SECURITY                  30
#define INT_TIMER_INTERRUPT             252
#define INT_INTER_CPU_INTERRUPT         253
#define INT_SMP_ERROR                   254
#define INT_SPURIOUS_INTERRUPT          255

struct interrupt_stack_frame
{
    uint64_t rbp;
    uint64_t rsi;
    uint64_t rdi;
    uint64_t r15;
    uint64_t r14;
    uint64_t r13;
    uint64_t r12;
    uint64_t r11;
    uint64_t r10;
    uint64_t r9;
    uint64_t r8;
    uint64_t rdx;
    uint64_t rcx;
    uint64_t rbx;
    uint64_t rax;
    uint64_t isr_nr;
    uint64_t error_code;
    uint64_t rip;
    uint64_t cs;
    uint64_t rflags;
    uint64_t rsp;
    uint64_t ss;
};

typedef void (*interrupt_handler)(uint64_t err, struct interrupt_stack_frame *frame);

extern void isr_handler_0(void);
extern void isr_handler_1(void);
extern void isr_handler_2(void);
extern void isr_handler_3(void);
extern void isr_handler_4(void);
extern void isr_handler_5(void);
extern void isr_handler_6(void);
extern void isr_handler_7(void);
extern void isr_handler_8(void);
extern void isr_handler_9(void);
extern void isr_handler_10(void);
extern void isr_handler_11(void);
extern void isr_handler_12(void);
extern void isr_handler_13(void);
extern void isr_handler_14(void);
extern void isr_handler_15(void);
extern void isr_handler_16(void);
extern void isr_handler_17(void);
extern void isr_handler_18(void);
extern void isr_handler_19(void);
extern void isr_handler_20(void);
extern void isr_handler_21(void);
extern void isr_handler_22(void);
extern void isr_handler_23(void);
extern void isr_handler_24(void);
extern void isr_handler_25(void);
extern void isr_handler_26(void);
extern void isr_handler_27(void);
extern void isr_handler_28(void);
extern void isr_handler_29(void);
extern void isr_handler_30(void);
extern void isr_handler_31(void);
extern void isr_handler_32(void);
extern void isr_handler_33(void);
extern void isr_handler_34(void);
extern void isr_handler_35(void);
extern void isr_handler_36(void);
extern void isr_handler_37(void);
extern void isr_handler_38(void);
extern void isr_handler_39(void);
extern void isr_handler_40(void);
extern void isr_handler_41(void);
extern void isr_handler_42(void);
extern void isr_handler_43(void);
extern void isr_handler_44(void);
extern void isr_handler_45(void);
extern void isr_handler_46(void);
extern void isr_handler_47(void);
extern void isr_handler_252(void);
extern void isr_handler_253(void);
extern void isr_handler_254(void);
extern void isr_handler_255(void);

void isr_common_handler(struct interrupt_stack_frame *frame);

void isr_register_handler(unsigned int n, interrupt_handler handler);

void isr_setup(void);

#endif /* _ARCH_X86_64_INTERRUPT_H */
