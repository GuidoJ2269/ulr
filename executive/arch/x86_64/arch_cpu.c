/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

#include <cpu.h>
#include <page.h>
#include <smp.h>
#include <system_routines.h>

#ifdef BOOTLOADER
#include <boo_routines.h>
#endif

#define IDT_GATE_MASK 0xff
#define IDT_SIZE (MAX_NUM_INTERRUPT_GATES * sizeof(union gate_descriptor))

#define IDT_PHYSICAL_PAGE 1
#define IDT_PHYSICAL_ADDRESS (IDT_PHYSICAL_PAGE * PAGE_SIZE)
#define GDT_PHYSICAL_PAGE (IDT_PHYSICAL_PAGE + (PAGE_ROUNDUP(IDT_SIZE) / PAGE_SIZE))
#define GDT_PHYSICAL_ADDRESS (GDT_PHYSICAL_PAGE * PAGE_SIZE)
#define TSS_PHYSICAL_PAGE (GDT_PHYSICAL_PAGE + (PAGE_ROUNDUP(sizeof(struct gdt)) / PAGE_SIZE))

#define NULL_SELECTOR            0x00
#define KERNEL_CODE_SELECTOR     0x08
#define KERNEL_DATA_SELECTOR     0x10
#define EXECUTIVE_CODE_SELECTOR  0x19
#define EXECUTIVE_DATA_SELECTOR  0x21
#define SUPERVISOR_CODE_SELECTOR 0x2a
#define SUPERVISOR_DATA_SELECTOR 0x32
#define USER_CODE_SELECTOR       0x3b
#define USER_DATA_SELECTOR       0x43
#define RESERVED_SELECTOR        0x43
#define TASK_STATE_SELECTOR      0x50

struct gdt
{
    union segment_descriptor         null_descriptor;                   /* seg 0x00 */
    union segment_descriptor         kernel_code_segment;               /* seg 0x08 */
    union segment_descriptor         kernel_data_segment;               /* seg 0x10 */
    union segment_descriptor         executive_code_segment;            /* seg 0x19 */
    union segment_descriptor         executive_data_segment;            /* seg 0x21 */
    union segment_descriptor         supervisor_code_segment;           /* seg 0x2a */
    union segment_descriptor         supervisor_data_segment;           /* seg 0x32 */
    union segment_descriptor         user_code_segment;                 /* seg 0x3b */
    union segment_descriptor         user_data_segment;                 /* seg 0x43 */
    union segment_descriptor         reserved_segment;                  /* seg 0x48 */
    struct system_segment_descriptor task_state_segment[MAX_NUM_CPUS];  /* seg 0x50 */
};

#define CPUID_GET_VENDOR   0
#define CPUID_GET_FEATURES 1

#define CPUID_EDX_FPU   0x00000001
#define CPUID_EDX_VME   0x00000002
#define CPUID_EDX_DE    0x00000004
#define CPUID_EDX_PSE   0x00000008
#define CPUID_EDX_TSC   0x00000010
#define CPUID_EDX_MSR   0x00000020
#define CPUID_EDX_PAE   0x00000040
#define CPUID_EDX_MCE   0x00000080
#define CPUID_EDX_CX8   0x00000100
#define CPUID_EDX_APIC  0x00000200
#define CPUID_EDX_SEP   0x00000800
#define CPUID_EDX_MTRR  0x00001000
#define CPUID_EDX_PGE   0x00002000
#define CPUID_EDX_MCA   0x00004000
#define CPUID_EDX_CMOV  0x00008000
#define CPUID_EDX_PAT   0x00010000
#define CPUID_EDX_PSE36 0x00020000
#define CPUID_EDX_PSN   0x00040000
#define CPUID_EDX_CLF   0x00080000
#define CPUID_EDX_DTES  0x00200000
#define CPUID_EDX_ACPI  0x00400000
#define CPUID_EDX_MMX   0x00800000
#define CPUID_EDX_FXSR  0x01000000
#define CPUID_EDX_SSE   0x02000000
#define CPUID_EDX_SSE2  0x04000000
#define CPUID_EDX_SS    0x08000000
#define CPUID_EDX_HTT   0x10000000
#define CPUID_EDX_TM1   0x20000000
#define CPUID_EDX_IA64  0x40000000
#define CPUID_EDX_PBE   0x80000000

struct _cpu_info cpu_info = {"", 0, 0, 0, 0};

/* local function prototypes */
void cpu_get_info(void);
void cpu_set_gdt_descriptor(union segment_descriptor *d, unsigned int long_mode, unsigned int dpl, unsigned int executable, unsigned int read_write);
void cpu_set_idt_descriptor(union gate_descriptor *d, interrupt_service_routine isr, unsigned int dpl, unsigned int type);
void cpu_set_tss_descriptor(struct system_segment_descriptor *d);
void cpu_setup_gdt(void);
void cpu_setup_idt(void);

void cpu_get_info(void)
{
    uint32_t eax, ebx, ecx, edx;
    uint32_t* cpu_vendor_ptr = (uint32_t *)cpu_info.vendor;

    cpuid(CPUID_GET_VENDOR, eax, ebx, ecx, edx);
    *cpu_vendor_ptr++ = ebx;
    *cpu_vendor_ptr++ = edx;
    *cpu_vendor_ptr++ = ecx;
    cpu_info.vendor[12] = 0;

    cpuid(CPUID_GET_FEATURES, eax, ebx, ecx, edx);
    cpu_info.family = (((eax >> 8) & 0x0f) | (((eax >> 20) & 0xff) << 4)) & 0xffff;
    cpu_info.model = (((eax >> 4) & 0x0f) | (((eax >> 16) & 0x0f) << 4)) & 0xff;
    cpu_info.stepping = eax & 0x0f;
    cpu_info.count = (ebx >> 16) & 0xff;
    if ((edx & (CPUID_EDX_MSR | CPUID_EDX_PAE | CPUID_EDX_APIC)) == 0)
    {
#ifdef BOOTLOADER
        boo$kprintf("CPU %s family %d model %d stepping %d not supported", cpu_info.vendor, cpu_info.family, cpu_info.model, cpu_info.stepping);
#else
        exe$kprintf("CPU %s family %d model %d stepping %d not supported", cpu_info.vendor, cpu_info.family, cpu_info.model, cpu_info.stepping);
#endif
        cpu_halt();
    }
}

void cpu_set_gdt_descriptor(union segment_descriptor *d, unsigned int long_mode, unsigned int dpl, unsigned int executable, unsigned int read_write)
{
    d->descriptor = 0;
    d->long_mode = long_mode & 0x01;
    d->present = 1;
    d->dpl = dpl & 0x03;
    d->system = 1;
    d->executable = executable & 0x01;
    d->read_write = read_write & 0x01;
}

void cpu_set_idt_descriptor(union gate_descriptor *d, interrupt_service_routine isr, unsigned int dpl, unsigned int type)
{
    d->offset_15_00 = (unsigned long)isr & 0xffff;
    d->offset_31_16 = ((unsigned long)isr >> 16) & 0xffff;
    d->offset_63_32 = (uint32_t)(((unsigned long)isr >> 32) & 0xffffffff);
    switch (dpl & 0x03)
    {
        case PL_KERNEL:
            d->selector = KERNEL_CODE_SELECTOR;
            break;
        case PL_EXECUTIVE:
            d->selector = EXECUTIVE_CODE_SELECTOR;
            break;
        case PL_SUPERVISOR:
            d->selector = SUPERVISOR_CODE_SELECTOR;
            break;
        case PL_USER:
            d->selector = USER_CODE_SELECTOR;
            break;
    }
    d->ist = 0;
    d->type = type & 0x0f;
    d->dpl = dpl & 0x03;
    d->present = 1;
}

void cpu_set_tss_descriptor(struct system_segment_descriptor *d)
{
    struct tss *tss = (struct tss *)TSS_PHYSICAL_PAGE;
#ifdef BOOTLOADER
    memset(tss, 0, sizeof(struct tss) * MAX_NUM_CPUS);
#else
    decc$memset(tss, 0, sizeof(struct tss) * MAX_NUM_CPUS);
#endif

    for (unsigned int i = 0; i < MAX_NUM_CPUS; i++, d++)
    {
        d->descriptor_63_00.descriptor = 0;
        d->descriptor_63_00.limit_15_00 = sizeof(struct tss) & 0xffff;
        d->descriptor_63_00.base_15_00 = (unsigned long)&tss[i] & 0xffff;
        d->descriptor_63_00.base_23_16 = ((unsigned long)&tss[i] >> 16) & 0xff;
        d->descriptor_63_00.executable = 1;
        d->descriptor_63_00.accessed = 1;
        d->descriptor_63_00.dpl = 0;
        d->descriptor_63_00.present = 1;
        d->descriptor_63_00.limit_19_16 = (sizeof(struct tss) >> 16) & 0x0f;
        d->descriptor_63_00.base_31_24 = ((unsigned long)&tss[i] >> 24) & 0xff;

        d->descriptor_127_64 = 0;
        d->base_63_32 = (uint32_t)(((unsigned long)&tss[i] >> 32) & 0xffffffff);
    }
}

void cpu_setup_gdt(void)
{
    struct descriptor_table_register gdt_reg;
    struct gdt *gdt = (struct gdt *)GDT_PHYSICAL_ADDRESS;

    gdt->null_descriptor.descriptor = 0;
    cpu_set_gdt_descriptor(&gdt->kernel_code_segment, 1, 0, 1, 0);
    cpu_set_gdt_descriptor(&gdt->kernel_data_segment, 0, 0, 0, 1);
    cpu_set_gdt_descriptor(&gdt->executive_code_segment, 1, 1, 1, 0);
    cpu_set_gdt_descriptor(&gdt->executive_data_segment, 0, 1, 0, 1);
    cpu_set_gdt_descriptor(&gdt->supervisor_code_segment, 1, 2, 1, 0);
    cpu_set_gdt_descriptor(&gdt->supervisor_data_segment, 0, 2, 0, 1);
    cpu_set_gdt_descriptor(&gdt->user_code_segment, 1, 3, 1, 0);
    cpu_set_gdt_descriptor(&gdt->user_data_segment, 0, 3, 0, 1);

    cpu_set_tss_descriptor(gdt->task_state_segment);

    /* activate the new GDT and TSS */
    gdt_reg.size = sizeof(struct gdt) - 1;
    gdt_reg.address = (unsigned long)gdt;
    cpu_load_gdt(gdt_reg);
    cpu_load_tr(TASK_STATE_SELECTOR); /* TODO: SMP correct cpu  */
}

void cpu_setup_idt(void)
{
    struct descriptor_table_register idt_reg;
    union gate_descriptor *interrupt_desc_list = (union gate_descriptor *)IDT_PHYSICAL_ADDRESS;
#ifdef BOOTLOADER
    memset(interrupt_desc_list, 0, IDT_SIZE);
#else
    decc$memset(interrupt_desc_list, 0, IDT_SIZE);
#endif

    /* activate the new IDT */
    idt_reg.size = IDT_SIZE - 1;
    idt_reg.address = (unsigned long)interrupt_desc_list;
    cpu_load_idt(idt_reg);
}

void cpu_setup(void)
{
    cpu_get_info();
    cpu_setup_gdt();
    cpu_setup_idt();
}

void cpu_set_gate(unsigned int n, interrupt_service_routine isr, unsigned int dpl, unsigned int type)
{
    union gate_descriptor *interrupt_desc_list = (union gate_descriptor *)IDT_PHYSICAL_ADDRESS;
    cpu_set_idt_descriptor(&(interrupt_desc_list[n & IDT_GATE_MASK]), isr, dpl, type);
}

struct _cpu_info * get_cpu_info(void)
{
    return &cpu_info;
}
