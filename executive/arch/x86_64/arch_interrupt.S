#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
#

.code64

.macro PUSH_ALL_REGISTERS
    pushq   %rax                        # save all registers
    pushq   %rbx
    pushq   %rcx
    pushq   %rdx
    pushq   %r8
    pushq   %r9
    pushq   %r10
    pushq   %r11
    pushq   %r12
    pushq   %r13
    pushq   %r14
    pushq   %r15
    pushq   %rdi
    pushq   %rsi
    pushq   %rbp
.endm

.macro POP_ALL_REGISTERS
    popq    %rbp                        # restore all registers
    popq    %rsi
    popq    %rdi
    popq    %r15
    popq    %r14
    popq    %r13
    popq    %r12
    popq    %r11
    popq    %r10
    popq    %r9
    popq    %r8
    popq    %rdx
    popq    %rcx
    popq    %rbx
    popq    %rax
.endm

.macro ERRORCODE_ISR_HANDLER isr_nr
.align 8
.global isr_handler_\isr_nr
isr_handler_\isr_nr:
    pushq   $\isr_nr
    jmp     isr_bottom_handler
.endm

.macro NOMINAL_ISR_HANDLER isr_nr
.align 8
.global isr_handler_\isr_nr
isr_handler_\isr_nr:
    pushq   $0
    pushq   $\isr_nr
    jmp     isr_bottom_handler
.endm

NOMINAL_ISR_HANDLER 0
NOMINAL_ISR_HANDLER 1
NOMINAL_ISR_HANDLER 2
NOMINAL_ISR_HANDLER 3
NOMINAL_ISR_HANDLER 4
NOMINAL_ISR_HANDLER 5
NOMINAL_ISR_HANDLER 6
NOMINAL_ISR_HANDLER 7
NOMINAL_ISR_HANDLER 8
#ERRORCODE_ISR_HANDLER 8
NOMINAL_ISR_HANDLER 9
ERRORCODE_ISR_HANDLER 10
ERRORCODE_ISR_HANDLER 11
NOMINAL_ISR_HANDLER 12
#ERRORCODE_ISR_HANDLER 12
ERRORCODE_ISR_HANDLER 13
ERRORCODE_ISR_HANDLER 14
NOMINAL_ISR_HANDLER 15
NOMINAL_ISR_HANDLER 16
ERRORCODE_ISR_HANDLER 17
NOMINAL_ISR_HANDLER 18
NOMINAL_ISR_HANDLER 19
NOMINAL_ISR_HANDLER 20
NOMINAL_ISR_HANDLER 21
NOMINAL_ISR_HANDLER 22
NOMINAL_ISR_HANDLER 23
NOMINAL_ISR_HANDLER 24
NOMINAL_ISR_HANDLER 25
NOMINAL_ISR_HANDLER 26
NOMINAL_ISR_HANDLER 27
NOMINAL_ISR_HANDLER 28
NOMINAL_ISR_HANDLER 29
ERRORCODE_ISR_HANDLER 30
NOMINAL_ISR_HANDLER 31
NOMINAL_ISR_HANDLER 32
NOMINAL_ISR_HANDLER 33
NOMINAL_ISR_HANDLER 34
NOMINAL_ISR_HANDLER 35
NOMINAL_ISR_HANDLER 36
NOMINAL_ISR_HANDLER 37
NOMINAL_ISR_HANDLER 38
NOMINAL_ISR_HANDLER 39
NOMINAL_ISR_HANDLER 40
NOMINAL_ISR_HANDLER 41
NOMINAL_ISR_HANDLER 42
NOMINAL_ISR_HANDLER 43
NOMINAL_ISR_HANDLER 44
NOMINAL_ISR_HANDLER 45
NOMINAL_ISR_HANDLER 46
NOMINAL_ISR_HANDLER 47
NOMINAL_ISR_HANDLER 252
NOMINAL_ISR_HANDLER 253
NOMINAL_ISR_HANDLER 254
NOMINAL_ISR_HANDLER 255

.align 16
.global isr_bottom_handler
isr_bottom_handler:
    PUSH_ALL_REGISTERS

    cld
    movq    %rsp, %rdi                  # pointer to register struct as parameter
    call    isr_common_handler

    POP_ALL_REGISTERS

    addq    $16, %rsp                   # pop parameters
    iretq
