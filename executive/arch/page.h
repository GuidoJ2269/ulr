/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_PAGE_H
#define _ARCH_PAGE_H

#include <arch_page.h>

#define PAGE_SIZE 0x1000 /* 4kb */
#define PAGE_MASK 0x0fff
#define PAGE_SHIFT 12
#define LARGE_PAGE_SIZE 0x200000 /* 2Mb */
#define LARGE_PAGE_MASK 0x1fffff
#define LARGE_PAGE_SHIFT 21

#define PAGE_ROUNDUP(x) ((((unsigned long)x) + PAGE_MASK) & (unsigned long)~PAGE_MASK)
#define PAGE_ROUNDDOWN(x) (((unsigned long)x) & (unsigned long)~PAGE_MASK)
#define LARGE_PAGE_ROUNDUP(x) ((((unsigned long)x) + LARGE_PAGE_MASK) & (unsigned long)~LARGE_PAGE_MASK)
#define LARGE_PAGE_ROUNDDOWN(x) (((unsigned long)x) & (unsigned long)~LARGE_PAGE_MASK)

#define NUM_PAGES(x) ((((unsigned long)x) + PAGE_MASK) >> PAGE_SHIFT)
#define NUM_LARGE_PAGES(x) ((((unsigned long)x) + LARGE_PAGE_MASK) >> LARGE_PAGE_SHIFT)

#endif /* _ARCH_PAGE_H */
