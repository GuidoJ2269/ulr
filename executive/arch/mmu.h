/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ARCH_MMU_H
#define _ARCH_MMU_H

#include <arch_mmu.h>

#define MAX_VIRTUAL_MEMORY (1UL << 32)  /* TODO: address space limited to 4Gb */

#define P0_SPACE_START 0x0000000000100000
#define P0_SPACE_END   ((MAX_VIRTUAL_MEMORY >> 2) - 1)
#define P1_SPACE_START (MAX_VIRTUAL_MEMORY >> 2)
#define P1_SPACE_END   ((MAX_VIRTUAL_MEMORY >> 1) - 1)
#define S0_SPACE_START (~((MAX_VIRTUAL_MEMORY >> 1) - 1))
#define S0_SPACE_END   0xffffffffffffffff

union pt_flags
{
    struct
    {
        uint8_t read_write: 1;
        uint8_t user_super: 1;
        uint8_t fill: 6;
    };
    uint8_t flags;
};

#endif /* _ARCH_MMU_H */
