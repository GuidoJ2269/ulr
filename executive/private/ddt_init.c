/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <driver_routines.h>
#include <ssdef.h>

DDT driver$ddt = {};

/*
int driver$ini_ddt_altstart( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_aux_routine( DDT *ddt, int ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_aux_storage( DDT *ddt, void *addr )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_cancel( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_cancel_selective( DDT *ddt, int ( *func )( void ) )
{
}

int driver$ini_ddt_channel_assign( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_cloneducb( DDT *ddt, int ( *func )( void ) )
{
}

int driver$ini_ddt_ctrlinit( DDT *ddt, int ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_diagbf( DDT *ddt, unsigned short value )
{
}

int driver$ini_ddt_erlgbf( DDT *ddt, unsigned short value )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_kp_reg_mask( DDT *ddt, unsigned long value )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_kp_stack_size( DDT *ddt, unsigned long value )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_kp_startio( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_mntv_for( DDT *ddt, int ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_mntver( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_regdmp( DDT *ddt, void ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_ddt_start( DDT *ddt, void  ( *func )( void ) )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}
*/

int driver$ini_ddt_unitinit( DDT *ddt, ddt$unitinit func )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    ddt->ddt$ps_unitinit = func;
    return SS$_NORMAL;
}

int driver$ini_ddt_end( DDT *ddt )
{
    if (ddt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}
