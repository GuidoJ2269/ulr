/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <driver_routines.h>
#include <ssdef.h>

#include <util.h>

int driver$illegal_io_function(IRP *, PCB *, UCB *, CCB *);

FDT driver$fdt = {0, {driver$illegal_io_function}};

int driver$illegal_io_function(IRP *irp, PCB *pcb, UCB *ucb, CCB *ccb)
{
    SUPPRESS_UNUSED_WARNING(irp);
    SUPPRESS_UNUSED_WARNING(pcb);
    SUPPRESS_UNUSED_WARNING(ucb);
    SUPPRESS_UNUSED_WARNING(ccb);
    return SS$_ILLIOFUNC;
}

int driver$ini_fdt_act( FDT *fdt, IODEF iofunc, fdt$action_routine_func action, unsigned int bufflag )
{
    if (fdt == 0)
    {
        return SS$_ACCVIO;
    }
    uint16_t func_index = iofunc.io$r_fcode_structure.io$v_fcode & IO$M_FCODE;
    uint64_t mask = (1UL << func_index);
    if (bufflag == BUFFERED)
    {
        fdt->fdt$q_buffered |= mask;
    }
    fdt->fdt$ps_func_rtn[func_index] = action;
    return SS$_NORMAL;
}

int driver$ini_fdt_end( FDT *fdt )
{
    if (fdt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}
