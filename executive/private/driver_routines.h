/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _DRIVER_ROUTINES_H
#define _DRIVER_ROUTINES_H

#include <dptdef.h>

/* Driver Dispatch Table initialization routines */
int driver$ini_ddt_altstart( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_aux_routine( DDT *ddt, int ( *func )( void ) );
int driver$ini_ddt_aux_storage( DDT *ddt, void *addr );
int driver$ini_ddt_cancel( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_cancel_selective( DDT *ddt, int ( *func )( void ) );
int driver$ini_ddt_channel_assign( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_cloneducb( DDT *ddt, int ( *func )( void ) );
int driver$ini_ddt_ctrlinit( DDT *ddt, int ( *func )( void ) );
int driver$ini_ddt_diagbf( DDT *ddt, unsigned short value );
int driver$ini_ddt_erlgbf( DDT *ddt, unsigned short value );
int driver$ini_ddt_kp_reg_mask( DDT *ddt, unsigned long value );
int driver$ini_ddt_kp_stack_size( DDT *ddt, unsigned long value );
int driver$ini_ddt_kp_startio( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_mntv_for( DDT *ddt, int ( *func )( void ) );
int driver$ini_ddt_mntver( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_regdmp( DDT *ddt, void ( *func )( void ) );
int driver$ini_ddt_start( DDT *ddt, void  ( *func )( void ) );
int driver$ini_ddt_unitinit( DDT *ddt, ddt$unitinit func );
int driver$ini_ddt_end( DDT *ddt );

/* Driver Prologue Table initialization routines */
int driver$ini_dpt_adapt( DPT *dpt, unsigned long value );
int driver$ini_dpt_bt_order( DPT *dpt, long value );
int driver$ini_dpt_decode( DPT *dpt, long value );
int driver$ini_dpt_defunits( DPT *dpt, unsigned short value );
int driver$ini_dpt_deliver( DPT *dpt, int ( *func )( void ) );
int driver$ini_dpt_flags( DPT *dpt, unsigned long value );
int driver$ini_dpt_idb_crams( DPT *dpt, unsigned short value );
int driver$ini_dpt_maxunits( DPT *dpt, unsigned short value );
int driver$ini_dpt_name( DPT *dpt, char *string_ptr );
int driver$ini_dpt_struc_init( DPT *dpt, dpt$struc_init_func func );
int driver$ini_dpt_struc_reinit( DPT *dpt, dpt$struc_init_func func );
int driver$ini_dpt_ucb_crams( DPT *dpt, unsigned short value );
int driver$ini_dpt_ucbsize( DPT *dpt, unsigned short value );
int driver$ini_dpt_unload( DPT *dpt, int ( *func )( void ) );
int driver$ini_dpt_vector( DPT *dpt, void( **func )( void ) );
int driver$ini_dpt_end( DPT *dpt );

/* Function Decision Table initialization routines */
int driver$ini_fdt_act( FDT *fdt, IODEF iofunc, fdt$action_routine_func action, unsigned int bufflag );
int driver$ini_fdt_end( FDT *fdt );

/* Possible values for the bufflag parameter in driver$ini_fdt_act */
#define BUFFERED     0
#define NOT_BUFFERED 1
#define DIRECT       1

#endif /* _DRIVER_ROUTINES_H */
