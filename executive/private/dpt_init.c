/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

#include <driver_routines.h>
#include <ssdef.h>

DPT driver$dpt = {0, 0, {""}};

/*
int driver$ini_dpt_adapt( DPT *dpt, unsigned long value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_bt_order( DPT *dpt, long value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_decode( DPT *dpt, long value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_defunits( DPT *dpt, unsigned short value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_deliver( DPT *dpt, int ( *func )( void ) )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_flags( DPT *dpt, unsigned long value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_idb_crams( DPT *dpt, unsigned short value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_maxunits( DPT *dpt, unsigned short value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_ucb_crams( DPT *dpt, unsigned short value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_ucbsize( DPT *dpt, unsigned short value )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_unload( DPT *dpt, int ( *func )( void ) )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}

int driver$ini_dpt_vector( DPT *dpt, void( **func )( void ) )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}
*/

int driver$ini_dpt_name( DPT *dpt, char *string_ptr )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    COUNTED_STRING_ASSIGN(dpt, string_ptr, DEVICE_NAME_LEN);
    return SS$_NORMAL;
}

int driver$ini_dpt_struc_init( DPT *dpt, dpt$struc_init_func func )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    dpt->dpt$ps_init_pd = func;
    return SS$_NORMAL;
}

int driver$ini_dpt_struc_reinit( DPT *dpt, dpt$struc_init_func func )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    dpt->dpt$ps_reinit_pd = func;
    return SS$_NORMAL;
}

int driver$ini_dpt_end( DPT *dpt )
{
    if (dpt == 0)
    {
        return SS$_ACCVIO;
    }
    return SS$_NORMAL;
}
