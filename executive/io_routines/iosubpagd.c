/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ccbdef.h>
#include <iosubpagd.h>
#include <shell.h>
#include <ssdef.h>
#include <sysparam.h>
#include <system_routines.h>

struct _ccb ioc$channel_table[CHANNELCNT];

/* Search the I/O channel table for a free channel */
int ioroutines_ffchan(channel_t *chan, struct _ccb **ccb)
{
    *chan = sgn$gw_pchancnt;
    *ccb = &ioc$channel_table[sgn$gw_pchancnt - 1];
    while ((*chan > 0) && ((*ccb)->ccb$b_amod != 0))
    {
        (*chan)--;
        (*ccb)--;
    }
    if (*chan > 0)
    {
        return SS$_NORMAL;
    }
    *ccb = 0;
    return SS$_NOIOCHAN;
}

/* search the I/O database for a specified device */
int ioc_std$search(struct dsc$descriptor_s *devnam)
{
    /* TODO */
    return SS$_ABORT;
}

/* search the I/O database for a specific physical device */
int ioc_std$searchdev(struct dsc$descriptor_s *devnam)
{
    /* TODO */
    return SS$_ABORT;
}

/* verify that the channel is legal */
int ioroutines_verifychan(channel_t *chan, struct _ccb **ccb)
{
    if ((*chan > 0) && (*chan < sgn$gw_pchancnt))
    {
        *ccb = &ioc$channel_table[*chan - 1];
        /* TODO: properly check the priviledge level */
        if ((*ccb)->ccb$b_amod > 0)
        {
            return SS$_NORMAL;
        }
        else
        {
            *chan = 0;
            *ccb = 0;
            return SS$_NOPRIV;
        }
    }
    return SS$_IVCHAN;
}

void ioc_std$init_channel_table(void)
{
    /* TODO: dynamically allocate the channel table */
    /* TODO: get channel table size from on SYSGEN params */
    sgn$gw_pchancnt = CHANNELCNT;
    decc$memset(ioc$channel_table, 0, sizeof(struct _ccb) * sgn$gw_pchancnt);
    for (channel_t chan = 0; chan < sgn$gw_pchancnt; chan++)
    {
        ioc$channel_table[chan].ccb$w_chan = chan;
    }

    ctl$gw_chindx = sgn$gw_pchancnt;
    ctl$gl_ccbbase = ioc$channel_table;
}
