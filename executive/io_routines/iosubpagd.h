/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, GOOD TITLE or
 * NON INFRINGEMENT.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _IOSUBPAGD_H
#define _IOSUBPAGD_H

#include <ccbdef.h>
#include <descrip.h>

#define CHANNELCNT 256

extern struct _ccb ioc$channel_table[CHANNELCNT];

extern int ioroutines_ffchan(channel_t *chan, struct _ccb **ccb);
extern int ioc_std$search(struct dsc$descriptor_s *devnam);
extern int ioc_std$searchdev(struct dsc$descriptor_s *devnam);
extern int ioroutines_verifychan(channel_t *chan, struct _ccb **ccb);

extern void ioc_std$init_channel_table(void);

#endif /* _IOSUBPAGD_H */
