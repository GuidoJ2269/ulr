/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <iosubpagd.h>
#include <psldef.h>
#include <shell.h>
#include <ssdef.h>
#include <sysassign.h>
#include <system_data_cells.h>
#include <system_routines.h>

/* assign a channel */
int exe$assign(struct dsc$descriptor_s *devnam, channel_t *chan, unsigned int acmode, struct dsc$descriptor_s *mbxnam, unsigned int flags)
{
    if ((chan == 0) || ((mbxnam != 0) && (mbxnam->dsc$a_pointer == 0)) || (devnam == 0) || (devnam->dsc$a_pointer == 0))
    {
        return SS$_ACCVIO;
    }
    struct _ccb *ccb = 0;
    int result = ioroutines_ffchan(chan, &ccb);
    if (result != SS$_NORMAL)
    {
        return result;
    }
    sch$lockw(&ioc$gl_mutex);
    if (mbxnam != 0)
    {
        /* TODO: ioc$searchdev(mbxnam) */
    }
    /* TODO: ioc$search(devnam) */

    if (flags != 0)
    {
        /* TODO: handle device dependent flags */
    }
    ctl$gl_ccbbase[*chan].ccb$b_amod = (uint8_t)((acmode & PSL$C_MASK) + 1);
    sch$unlock(&ioc$gl_mutex);
    return SS$_NORMAL;
}
