/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <iosubpagd.h>
#include <ssdef.h>
#include <sysdassgn.h>
#include <system_data_cells.h>
#include <system_routines.h>

/* deassign a channel */
int exe$dassgn(channel_t chan)
{
    struct _ccb *ccb = 0;
    int result = ioroutines_verifychan(&chan, &ccb);
    if (result != SS$_NORMAL)
    {
        return result;
    }
    /* TODO: cancel I/O */
    /* TODO: close files */
    /* TODO: wait for AST completion */
    sch$lockw(&ioc$gl_mutex);
    ccb->ccb$b_amod = 0;
    /* TODO: disassociate mailbox */
    /* TODO: decrement UCB reference count */
    sch$unlock(&ioc$gl_mutex);
    return SS$_NORMAL;
}
