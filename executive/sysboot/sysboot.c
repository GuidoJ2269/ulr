/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

#include <allocpfn.h>
#include <lapic.h>
#include <booparam.h>
#include <interrupt.h>
#include <mmg_routines.h>
#include <smp.h>
#include <ssdef.h>
#include <sysloavec.h>
#include <sysparam.h>
#include <system_data_cells.h>
#include <system_routines.h>
#include <system_service_loader.h>
#include <vgaconsole.h>

#include "boo_routines.h"
#include "multiboot2.h"
#include "sysldr.h"

#define MAXNUM_BOOT_MEM_BLOCKS 16

#define SYSTEM_PAGE_TABLE_PAGES (pfn_t)(1 + MAX_PGD_ENTRIES + MAX_PDP_ENTRIES + MAX_4K_PAGE_TABLES)
#define PFN_DATABASE_PAGES (pfn_t)NUM_PAGES(NUM_PAGES(physical_memory_size) * sizeof(struct _pfn))
#define NON_PAGED_POOL_PAGES (pfn_t)(PAGE_ROUNDDOWN(NPAGEDYN) / PAGE_SIZE)
#define NON_PAGED_RESERVED_PAGES (pfn_t)(PAGE_ROUNDDOWN(NPAGEVIR - NPAGEDYN) / PAGE_SIZE)
#define PAGED_RESERVED_PAGES (pfn_t)(PAGE_ROUNDDOWN(PAGEDYN) / PAGE_SIZE)
#define PFN_NO_DB_PAGES (pfn_t)(LARGE_PAGE_ROUNDUP((SYSTEM_PAGE_TABLE_PAGES + PFN_DATABASE_PAGES + NON_PAGED_POOL_PAGES) * PAGE_SIZE) / PAGE_SIZE)

extern unsigned long _start;

size_t physical_memory_size = 0;
size_t available_memory_size = 0;

unsigned int num_boot_mem_blocks = 0;
struct memory_area boot_memory_blocks[MAXNUM_BOOT_MEM_BLOCKS];

int num_boot_modules = 0;
struct _ldrimg boot_modules[MAXNUM_BOOT_MODULES];
addr_t modules_end_address = 0;         /* last address used by the boot loader */

struct _vlb *nonpaged_area = 0;
struct _vlb *paged_area = 0;
struct _pfn *pfn_db = 0;

addr_t lapic_base_addr = 0;

/* private function prototypes */
void parse_multiboot_module(struct multiboot_tag_module *mb_module_tag);
void parse_multiboot_mmap(struct multiboot_tag_mmap *mb_mmap_tag, unsigned long size);
void parse_multiboot_info(void *mb_info);
void pfn_db_setup(void);
void set_system_params(void);
void system_setup(void);
void sys$boot(void *mb_info);

void parse_multiboot_module(struct multiboot_tag_module *mb_module_tag)
{
    struct _ldrimg* bm_ptr = &boot_modules[num_boot_modules];
    bm_ptr->ldrimg$l_base = mb_module_tag->mod_start;
    bm_ptr->ldrimg$l_length = mb_module_tag->mod_end - mb_module_tag->mod_start;
    strncpy(bm_ptr->ldrimg$s_imgnam, mb_module_tag->cmdline, MAX_IMAGE_NAME_LEN);
    bm_ptr->ldrimg$v_delay_init = 0;
    num_boot_modules++;
    if (modules_end_address < mb_module_tag->mod_end)
    {
        modules_end_address = mb_module_tag->mod_end;
    }
    ldr$insert_image(bm_ptr);
}

void parse_multiboot_mmap(struct multiboot_tag_mmap *mb_mmap_tag, unsigned long size)
{
    struct multiboot_mmap_entry *mb_mmap_entry = mb_mmap_tag->entries;
    struct memory_area* bmb_ptr = boot_memory_blocks;
    while (mb_mmap_entry < (struct multiboot_mmap_entry *)((unsigned long)mb_mmap_tag + size))
    {
        if ((mb_mmap_entry->type != MULTIBOOT_MEMORY_RESERVED) ||
            (mb_mmap_entry->addr <= physical_memory_size))
        {
            /* save the boot memory block */
            bmb_ptr->address = mb_mmap_entry->addr;
            bmb_ptr->size = mb_mmap_entry->len;
            if (mb_mmap_entry->type == MULTIBOOT_MEMORY_AVAILABLE)
            {
                bmb_ptr->status = MS_FREE;
                if (available_memory_size < PAGE_ROUNDUP(mb_mmap_entry->addr + mb_mmap_entry->len))
                {
                    available_memory_size = PAGE_ROUNDUP(mb_mmap_entry->addr + mb_mmap_entry->len);
                }
            }
            else if (mb_mmap_entry->type == MULTIBOOT_MEMORY_RESERVED)
            {
                bmb_ptr->status = MS_RESERVED;
            }
            else
            {
                bmb_ptr->status = MS_UNUSABLE;
            }

            num_boot_mem_blocks++;
            bmb_ptr++;

            /* calculate RAM size */
            if (physical_memory_size < LARGE_PAGE_ROUNDUP(mb_mmap_entry->addr + mb_mmap_entry->len))
            {
                physical_memory_size = LARGE_PAGE_ROUNDUP(mb_mmap_entry->addr + mb_mmap_entry->len);
            }
        }
        mb_mmap_entry = (struct multiboot_mmap_entry *)((unsigned long)mb_mmap_entry + mb_mmap_tag->entry_size);
    }
}

void parse_multiboot_info(void *mb_info)
{
    uint32_t mb_size = *(uint32_t *)mb_info;
    mb_info += 8;
    struct multiboot_tag *mb_tag = (struct multiboot_tag *)mb_info;
    while ((mb_size > 0) && (mb_tag->type != MULTIBOOT_TAG_TYPE_END))
    {
        switch (mb_tag->type)
        {
            case MULTIBOOT_TAG_TYPE_MODULE:
                {
                    parse_multiboot_module((struct multiboot_tag_module *)mb_tag);
                }
                break;
            case MULTIBOOT_TAG_TYPE_MMAP:
                    parse_multiboot_mmap((struct multiboot_tag_mmap *)mb_tag, mb_tag->size);
                break;
            default:
                break;
        }
        mb_tag->size = (mb_tag->size + 7) & ~7U;
        mb_info += mb_tag->size;
        mb_size -= mb_tag->size;
        mb_tag = (struct multiboot_tag *)mb_info;
    }
}

void set_system_params(void)
{
    struct _cpu_info *cpu_info = get_cpu_info();
    exe$gl_state.v_sysboot = 1;
    strncpy(exe$gb_cpuvendor, cpu_info->vendor, CPUVENDOR_LEN);
    exe$gw_cpufamily = cpu_info->family;
    exe$gb_cpumodel = cpu_info->model;
    exe$gb_cpustepping = cpu_info->stepping;

    smp$gw_cpu_cnt = 1;
    struct _cpu * cpu_data = smp$gl_cpu_data;
    cpu_data->cpu$l_lapic_base = lapic_base_addr;

    mmg$gl_syspgd = mmu_pgd;
    mmg$gl_npagedyn = (void *)nonpaged_area;
    mmg$gl_npagnext = mmg$gl_npagedyn + NPAGEDYN;
    mmg$gl_pagedyn = (void *)paged_area;
    mmg$gl_maxmem = physical_memory_size;
    mmg$gl_minpfn = 0;
    mmg$gl_maxpfn = (pfn_t)NUM_PAGES(physical_memory_size);

    mmu_setup_4k_pages();

    ldr$gq_image_list_head = ldr$gq_boot_image_list_head;
    ldr$gq_image_list_tail = ldr$gq_boot_image_list_tail;
}

void system_setup(void)
{
    cpu_setup();
    smp_setup();

    addr_t last_used_address = LARGE_PAGE_ROUNDUP(modules_end_address);
    mmg_std$init_boot_pfn_no_db(ADDRESS2PFN(last_used_address), PFN_NO_DB_PAGES);
    int status = mmg_std$allocpfn_no_db(0, last_used_address, SYSTEM_PAGE_TABLE_PAGES);
    if (status != SS$_NORMAL)
    {
        boo$kprintf("Cannot allocate %d pages for system page tables!\n", SYSTEM_PAGE_TABLE_PAGES);
        cpu_halt();
    }

    mmu_pgd = (union pgd_entry *)last_used_address;
    mmu_pdp = (union pdp_entry *)(last_used_address + 1 * PAGE_SIZE);
    mmu_pd2m = (union pd2m_entry *)(last_used_address + (1 + MAX_PGD_ENTRIES) * PAGE_SIZE);
    mmu_boot_4k_pages = (union pt_entry *)(last_used_address + (1 + MAX_PGD_ENTRIES + MAX_PDP_ENTRIES) * PAGE_SIZE);
    mmu_setup_paging(physical_memory_size);

    mmu_pgd = (union pgd_entry *)mmu_map_system_data(mmu_pgd, 1);
    mmu_pdp = (union pdp_entry *)mmu_map_system_data(mmu_pdp, MAX_PGD_ENTRIES);
    mmu_pd2m = (union pd2m_entry *)mmu_map_system_data(mmu_pd2m, MAX_PDP_ENTRIES);
    mmu_boot_4k_pages = (union pt_entry *)mmu_map_system_data(mmu_boot_4k_pages, MAX_4K_PAGE_TABLES);

    pfn_db = (struct _pfn *)(last_used_address + (SYSTEM_PAGE_TABLE_PAGES * PAGE_SIZE));
    status = mmg_std$allocpfn_no_db(0, (addr_t)pfn_db, PFN_DATABASE_PAGES);
    if (status != SS$_NORMAL)
    {
        boo$kprintf("Cannot allocate %d pages for the PFN database!\n", PFN_DATABASE_PAGES);
        cpu_halt();
    }
    pfn_db = (struct _pfn *)mmu_map_system_data(pfn_db, PFN_DATABASE_PAGES);

    nonpaged_area = (struct _vlb *)(last_used_address + (SYSTEM_PAGE_TABLE_PAGES + PFN_DATABASE_PAGES) * PAGE_SIZE);
    status = mmg_std$allocpfn_no_db(0, (addr_t)nonpaged_area, NON_PAGED_POOL_PAGES);
    if (status != SS$_NORMAL)
    {
        boo$kprintf("Cannot allocate %d pages for the non-paged pool!\n", NON_PAGED_POOL_PAGES);
        cpu_halt();
    }
    mmu_reserve_system_space(NON_PAGED_RESERVED_PAGES);
    nonpaged_area = (struct _vlb *)mmu_map_system_data(nonpaged_area, NON_PAGED_POOL_PAGES);

    paged_area = mmu_reserve_system_space(PAGED_RESERVED_PAGES);

    lapic_map_base(&lapic_base_addr);
    interrupt_setup();
}

void pfn_db_setup(void)
{
    addr_t last_used_address = LARGE_PAGE_ROUNDUP(modules_end_address);
    pfn$gl_phypgcnt = (page_count_t)NUM_PAGES(physical_memory_size);
    pfn$a_base = pfn_db;
    memset(pfn$a_base, 0, pfn$gl_phypgcnt * sizeof(struct _pfn));
    for (pfn_t i = 0; i < ADDRESS2PFN((addr_t)(&_start)); i++)
    {
        pfn$a_base[i].pfn$id = i;
        mmg$ins_pfnt(i, 0, &pfn$a_base[i]);
    }
    for (pfn_t i = ADDRESS2PFN((addr_t)(&_start)); i < ADDRESS2PFN(PAGE_ROUNDUP(modules_end_address)); i++)
    {
        pfn$a_base[i].pfn$id = i;
        pfn$a_base[i].pfn$b_type.v_page_type = PFN$C_SYSTEM_PAGE;
        pfn$a_base[i].pfn$b_state.v_page_location = PFN$C_ACTIVE;
    }
    for (pfn_t i = ADDRESS2PFN(PAGE_ROUNDUP(modules_end_address)); i < ADDRESS2PFN(last_used_address); i++)
    {
        pfn$a_base[i].pfn$id = i;
        mmg$ins_pfnt(i, 0, &pfn$a_base[i]);
    }
    for (pfn_t i = ADDRESS2PFN(last_used_address); i < ADDRESS2PFN(last_used_address) + PFN_NO_DB_PAGES; i++)
    {
        pfn$a_base[i].pfn$id = i;
        pfn$a_base[i].pfn$b_type.v_page_type = PFN$C_SYSTEM_PAGE;
        pfn$a_base[i].pfn$b_state.v_page_location = PFN$C_ACTIVE;
    }
    for (pfn_t i = ADDRESS2PFN(last_used_address) + PFN_NO_DB_PAGES; i < pfn$gl_phypgcnt; i++)
    {
        pfn$a_base[i].pfn$id = i;
        mmg$ins_pfnt(i, 0, &pfn$a_base[i]);
    }
    mmg_std$setup_pfn_no_db();
}

void sys$boot(void *mb_info)
{
    boo$console_init();
    if (mb_info != 0)
    {
        parse_multiboot_info(mb_info);
        system_setup();

        struct _ldrimg *image;
        $DESCRIPTOR(base_image_desc, "BASE_IMAGE");
        union ldr$flags flags = { 0 };
        flags.ldr$v_pag = 1;
        if (ldr$load_image(&base_image_desc, flags, &image, mmu_pgd) == SS$_NORMAL)
        {
            image->ldrimg$v_delay_init = 0;
        }
        else
        {
            boo$kprintf("Failed to load %s!\n", base_image_desc.dsc$a_pointer);
            cpu_halt();
        }

        base_image.decc_memcmp = memcmp;
        base_image.decc_memcpy = memcpy;
        base_image.decc_memmove = memmove;
        base_image.decc_memset = memset;
        base_image.decc_strcat = strcat;
        base_image.decc_strcmp = strcmp;
        base_image.decc_strcpy = strcpy;
        base_image.decc_strlen = strlen;
        base_image.decc_strncat = strncat;
        base_image.decc_strncmp = strncmp;
        base_image.decc_strncpy = strncpy;
        base_image.decc_strnlen = strnlen;
        base_image.con_init_cty = boo$console_init;
        base_image.con_putchar = boo$putchar;
        base_image.exe_kprintf = boo$kprintf;
        base_image.exe_kvprintf = boo$kvprintf;
        base_image.mmg_alloc_pfn = primitive_alloc_pfn;
        base_image.mmg_alloc_zero_pfn = primitive_alloc_zero_pfn;
        base_image.mmg_dalloc_pfn = primitive_dalloc_pfn;
        base_image.mmg_dalloc_zero_pfn = primitive_dalloc_zero_pfn;
        base_image.mmg_ins_pfnh = primitive_ins_pfnh;
        base_image.mmg_ins_pfnt = primitive_ins_pfnt;
        base_image.mmg_rem_pfn = primitive_rem_pfn;
        base_image.mmg_rem_pfnh = primitive_rem_pfnh;

        $DESCRIPTOR(cpu_routines_desc, "CPU_ROUTINES");
        if (ldr$load_image(&cpu_routines_desc, flags, &image, mmu_pgd) != SS$_NORMAL)
        {
            boo$kprintf("Failed to load %s!\n", cpu_routines_desc.dsc$a_pointer);
            cpu_halt();
        }

        $DESCRIPTOR(system_primitives_desc, "SYSTEM_PRIMITIVES");
        if (ldr$load_image(&system_primitives_desc, flags, &image, mmu_pgd) != SS$_NORMAL)
        {
            boo$kprintf("Failed to load %s!\n", system_primitives_desc.dsc$a_pointer);
            cpu_halt();
        }

        $DESCRIPTOR(system_synchronization_desc, "SYSTEM_SYNCHRONIZATION");
        if (ldr$load_image(&system_synchronization_desc, flags, &image, mmu_pgd) != SS$_NORMAL)
        {
            boo$kprintf("Failed to load %s!\n", system_synchronization_desc.dsc$a_pointer);
            cpu_halt();
        }

        $DESCRIPTOR(io_routines_desc, "IO_ROUTINES");
        if (ldr$load_image(&io_routines_desc, flags, &image, mmu_pgd) != SS$_NORMAL)
        {
            boo$kprintf("Failed to load %s!\n", io_routines_desc.dsc$a_pointer);
            cpu_halt();
        }

        $DESCRIPTOR(sys_vm_desc, "SYS_VM");
        if (ldr$load_image(&sys_vm_desc, flags, &image, mmu_pgd) != SS$_NORMAL)
        {
            boo$kprintf("Failed to load %s!\n", sys_vm_desc.dsc$a_pointer);
            cpu_halt();
        }

        $DESCRIPTOR(exec_init_desc, "EXEC_INIT");
        if (ldr$load_image(&exec_init_desc, flags, &image, mmu_pgd) == SS$_NORMAL)
        {
            image->ldrimg$v_delay_init = 0;
            set_system_params();
            pfn_db_setup();

            boo$kprintf("jmp %s: 0x%x\n", image->ldrimg$s_imgnam, image->ldrimg$l_init_rtn);
            asm ("jmp   *%0" : : "r" (image->ldrimg$l_init_rtn));
        }
        else
        {
            boo$kprintf("Failed to load %s!\n", exec_init_desc.dsc$a_pointer);
            cpu_halt();
        }
    }
    else
    {
        boo$kprintf("Invalid multiboot info!\n");
    }

    boo$kprintf("\nSystem halted!\n");
    cpu_halt();
}
