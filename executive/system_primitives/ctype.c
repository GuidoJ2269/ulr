/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,  Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ctype.h>

#define CT_NUL  0x00    /* null */
#define CT_UPP  0x01    /* upper */
#define CT_LOW  0x02    /* lower */
#define CT_DGT  0x04    /* digit */
#define CT_CTL  0x08    /* cntrl */
#define CT_PCT  0x10    /* punct */
#define CT_WSP  0x20    /* white space (space/lf/tab) */
#define CT_HEX  0x40    /* hex digit */
#define CT_SPC  0x80    /* hard space (0x20) */

static unsigned char primitive_char_type[] =
{
    CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL,                                                                         /* 0-7 */
    CT_CTL, CT_CTL|CT_WSP, CT_CTL|CT_WSP, CT_CTL|CT_WSP, CT_CTL|CT_WSP, CT_CTL|CT_WSP, CT_CTL, CT_CTL,                                      /* 8-15 */
    CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL,                                                                         /* 16-23 */
    CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL, CT_CTL,                                                                         /* 24-31 */
    CT_WSP|CT_SPC, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,                                                                  /* 32-39 */
    CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,                                                                         /* 40-47 */
    CT_DGT, CT_DGT, CT_DGT, CT_DGT, CT_DGT, CT_DGT, CT_DGT, CT_DGT,                                                                         /* 48-55 */
    CT_DGT, CT_DGT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,                                                                         /* 56-63 */
    CT_PCT, CT_UPP|CT_HEX, CT_UPP|CT_HEX, CT_UPP|CT_HEX, CT_UPP|CT_HEX, CT_UPP|CT_HEX, CT_UPP|CT_HEX, CT_UPP,                               /* 64-71 */
    CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP,                                                                         /* 72-79 */
    CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP,                                                                         /* 80-87 */
    CT_UPP, CT_UPP, CT_UPP, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,                                                                         /* 88-95 */
    CT_PCT, CT_LOW|CT_HEX, CT_LOW|CT_HEX, CT_LOW|CT_HEX, CT_LOW|CT_HEX, CT_LOW|CT_HEX, CT_LOW|CT_HEX, CT_LOW,                               /* 96-103 */
    CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW,                                                                         /* 104-111 */
    CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW,                                                                         /* 112-119 */
    CT_LOW, CT_LOW, CT_LOW, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_CTL,                                                                         /* 120-127 */
    CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL,         /* 128-143 */
    CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL, CT_NUL,         /* 144-159 */
    CT_WSP|CT_SPC, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,  /* 160-175 */
    CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT, CT_PCT,         /* 176-191 */
    CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP,         /* 192-207 */
    CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_PCT, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_UPP, CT_LOW,         /* 208-223 */
    CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW,         /* 224-239 */
    CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_PCT, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW, CT_LOW};        /* 240-255 */

#define primitive_ismask(x,msk) (((primitive_char_type[(x) & 0xff]) & (msk)) != 0)

int isalnum(int c)
{
    return primitive_ismask(c, CT_UPP | CT_LOW | CT_DGT);
}

int isalpha(int c)
{
    return primitive_ismask(c, CT_UPP | CT_LOW);
}

int isascii(int c)
{
    return (c <= 0x7f);
}

int iscntrl(int c)
{
    return primitive_ismask(c, CT_CTL);
}

int isdigit(int c)
{
    return primitive_ismask(c, CT_DGT);
}

int isgraph(int c)
{
    return primitive_ismask(c, CT_UPP | CT_LOW | CT_DGT | CT_PCT);
}

int islower(int c)
{
    return primitive_ismask(c, CT_LOW);
}

int isprint(int c)
{
    return primitive_ismask(c, CT_UPP | CT_LOW | CT_DGT | CT_PCT | CT_SPC);
}

int ispunct(int c)
{
    return primitive_ismask(c, CT_PCT);
}

int isspace(int c)
{
    return primitive_ismask(c, CT_SPC);
}

int isupper(int c)
{
    return primitive_ismask(c, CT_UPP);
}

int isxdigit(int c)
{
    return primitive_ismask(c, CT_DGT | CT_HEX);
}

int toascii(int c)
{
    return (c & 0x7f);
}

int tolower(int c)
{
    if (isupper(c))
    {
        c += 'a'-'A';
    }
    return c;
}

int toupper(int c)
{
    if (islower(c))
    {
        c += 'A'-'a';
    }
    return c;
}
