/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <mutex.h>
#include <spinlocks.h>
#include <ssdef.h>
#include <system_routines.h>

void primitive_lockr(struct _mtx *mutex)
{
    smp$acquire(IPL$_SCHED);
    while (mutex->mtx$v_wrt)
    {
        smp$restore(IPL$_SCHED);
        /* TODO: stall the process, set to MWAIT and reschedule */
        smp$acquire(IPL$_SCHED);
    }
    mutex->mtx$w_owncnt++;
    /* TODO: increment mutex count in current process PCB */
    /* TODO: save current process PCB priorities */
    /* TODO: elevate current process PCB priorities to 16 */
    smp$restore(IPL$_SCHED);
}

void primitive_lockw(struct _mtx *mutex)
{
    smp$acquire(IPL$_SCHED);
    while ((mutex->mtx$v_wrt) || (mutex->mtx$w_owncnt > 0))
    {
        smp$restore(IPL$_SCHED);
        /* TODO: stall the process, set to MWAIT and reschedule */
        smp$acquire(IPL$_SCHED);
    }
    mutex->mtx$v_wrt = 1;
    mutex->mtx$w_owncnt++;
    /* TODO: increment mutex count in current process PCB */
    /* TODO: save current process PCB priorities */
    /* TODO: elevate current process PCB priorities to 16 */
    smp$restore(IPL$_SCHED);
}

int primitive_lockwnowait(struct _mtx *mutex)
{
    smp$acquire(IPL$_SCHED);
    if ((mutex->mtx$v_wrt) || (mutex->mtx$w_owncnt > 0))
    {
        return SS$_RETRY;
    }
    mutex->mtx$v_wrt = 1;
    mutex->mtx$w_owncnt++;
    /* TODO: increment mutex count in current process PCB */
    /* TODO: save current process PCB priorities */
    /* TODO: elevate current process PCB priorities to 16 */
    smp$restore(IPL$_SCHED);
    return SS$_NORMAL;
}

void primitive_unlock(struct _mtx *mutex)
{
    smp$acquire(IPL$_SCHED);
    /* TODO: decrement mutex count in current process PCB */
    /* TODO: if PCB$W_MTXCNT == 0, restore current process PCB priorities */
    /* TODO: atomic? */
    mutex->mtx$w_owncnt--;
    if (mutex->mtx$w_owncnt == 0)
    {
        if (mutex->mtx$v_wrt)
        {
            mutex->mtx$v_wrt = 0;
            /* TODO: make waiting processes computable */
        }
    }
    smp$restore(IPL$_SCHED);
    /* TODO: reschedule if there is a computable process with higher priority */
}
