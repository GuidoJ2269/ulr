/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <kprint.h>
#include <system_routines.h>

unsigned int primitive_div(unsigned long *n, unsigned int base);
void primitive_putnumber(unsigned long num, unsigned int base, unsigned int pad);
void primitive_putstr(const char *s);

unsigned int primitive_div(unsigned long *n, unsigned int base)
{
    unsigned int res = (unsigned int)((*n) % base);
    *n = (*n) / base;
    return res;
}

void primitive_putnumber(unsigned long num, unsigned int base, unsigned int pad)
{
    static const char *digits = "0123456789abcdef";
    static char tmp[16];
    unsigned int i = 0;
    unsigned int j;

    if (num == 0)
    {
        tmp[i++] = '0';
    }
    else
    {
        while (num != 0)
        {
            tmp[i++] = digits[primitive_div(&num, base)];
        }
    }
    for(j = i; j < pad; j++)
    {
        con$putchar('0');
    }
    while(i > 0)
    {
        i--;
        con$putchar(tmp[i]);
    }
}

void primitive_putstr(const char *s)
{
    while (*s != '\0')
    {
        con$putchar(*s++);
    }
}

void primitive_kprintf(const char *format, ...)
{
    va_list arg;
    va_start(arg, format);
    primitive_kvprintf(format, arg);
    va_end(arg);
}
void primitive_kvprintf(const char *format, va_list arg)
{
    for(; *format; format++)
    {
        if (*format != '%')
        {
            con$putchar(*format);
            continue;
        }

        format++;
        switch(*format)
        {
            case 'c':
            {
                int c = va_arg(arg, int);
                con$putchar((char)(c & 0xff));
                break;
            }
            case 'd':
            {
                long number = va_arg(arg, long);
                primitive_putnumber((unsigned long)number, 10, 0);
                break;
            }
            case 's':
            {
                char *str = va_arg(arg, char *);
                primitive_putstr(str);
                break;
            }
            case 'x':
            {
                unsigned long number = va_arg(arg, unsigned long);
                primitive_putnumber(number, 16, 0);
                break;
            }
            default:
                /* ignore unsupported flags */
                break;
        }
    }
}
