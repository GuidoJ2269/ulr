/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _ALLOC_PFN_H
#define _ALLOC_PFN_H

#include <mmu.h>
#include <pfndef.h>

extern int mmg_std$allocpfn_no_db(union pgd_entry *pgd, addr_t vaddr, size_t n);

extern pfn_t primitive_alloc_pfn(struct _pfn **pfndbe);
extern pfn_t primitive_alloc_zero_pfn(struct _pfn **pfndbe);
extern void primitive_dalloc_pfn(pfn_t n, struct _pfn *pfndbe);
extern void primitive_dalloc_zero_pfn(pfn_t n, struct _pfn *pfndbe);
extern void primitive_ins_pfnh(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
extern void primitive_ins_pfnt(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
extern void primitive_rem_pfn(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
extern pfn_t primitive_rem_pfnh(unsigned int lst, struct _pfn **pfndbe);

#ifdef BOOTLOADER
extern void mmg_std$init_boot_pfn_no_db(pfn_t pfn, size_t n);
extern void mmg_std$setup_pfn_no_db(void);
#endif

#endif /* _ALLOC_PFN_H */
