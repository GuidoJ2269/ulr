/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

#include <allocpfn.h>
#include <list.h>
#include <ssdef.h>
#include <sysparam.h>
#include <system_data_cells.h>
#include <system_routines.h>

#ifdef BOOTLOADER
struct _pmap boot_no_pfn_db_list;
#endif
struct _pmap free_no_pfn_db_list;

int mmg_std$allocpfn_no_db(union pgd_entry *pgd, addr_t vaddr, size_t n)
{
#ifdef BOOTLOADER
    if (vaddr == 0)
    {
        return SS$_ACCVIO;
    }
    if (n > boot_no_pfn_db_list.pmap$l_pfn_count)
    {
        return SS$_INSFMEM;
    }

    /* bootloader memory is identity mapped, so (re)mapping is skipped if there is no page global directory */
    if (pgd != 0)
    {
        mmu_map_pages(pgd, PFN2ADDRESS(boot_no_pfn_db_list.pmap$l_start_pfn), vaddr, 1, 0, n);
    }
    boot_no_pfn_db_list.pmap$l_start_pfn += (pfn_t)(n & PFN_MASK);
    boot_no_pfn_db_list.pmap$l_pfn_count -= (page_count_t)n;
#else
    if ((pgd == 0) || (vaddr == 0))
    {
        return SS$_ACCVIO;
    }
    if (n > free_no_pfn_db_list.pmap$l_pfn_count)
    {
        return SS$_INSFMEM;
    }
    union pt_flags f;
    f.read_write = 1;
    f.user_super = 0;
    boo$map(pgd, PFN2ADDRESS(free_no_pfn_db_list.pmap$l_start_pfn), vaddr, f, n);
    free_no_pfn_db_list.pmap$l_start_pfn += (pfn_t)(n & PFN_MASK);
    free_no_pfn_db_list.pmap$l_pfn_count -= (page_count_t)n;
#endif

    return SS$_NORMAL;
}

pfn_t primitive_alloc_pfn(struct _pfn **pfndbe)
{
    (*pfndbe)->pfn$b_state.v_page_location = PFN$C_ACTIVE;
    return primitive_rem_pfnh(PFN_FREE_LIST, pfndbe);
}

pfn_t primitive_alloc_zero_pfn(struct _pfn **pfndbe)
{
    (*pfndbe)->pfn$b_state.v_page_location = PFN$C_ACTIVE;
    pfn_t n = primitive_rem_pfnh(PFN_FREE_LIST, pfndbe);
    memset(*pfndbe, 0, PAGE_SIZE);
    return n;
}

void primitive_dalloc_pfn(pfn_t n, struct _pfn *pfndbe)
{
    primitive_ins_pfnt(n, PFN_FREE_LIST, pfndbe);
    pfndbe->pfn$b_state.v_page_location = PFN$C_FREE;
}

void primitive_dalloc_zero_pfn(pfn_t n, struct _pfn *pfndbe)
{
    memset(pfndbe, 0, PAGE_SIZE);
    primitive_ins_pfnt(n, PFN_FREE_LIST, pfndbe);
    pfndbe->pfn$b_state.v_page_location = PFN$C_FREE;
}

void primitive_ins_pfnh(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    struct _pfn *item = pfndbe;
    if (item == 0)
    {
        item = &pfn$a_base[n];
    }
    LIST_INSERT_HEAD(pfn, pfn$al_head[lst], pfn$al_tail[lst], item);
    pfn$al_count[lst]++;
}

void primitive_ins_pfnt(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    struct _pfn *item = pfndbe;
    if (item == 0)
    {
        item = &pfn$a_base[n];
    }
    LIST_INSERT_TAIL(pfn, pfn$al_head[lst], pfn$al_tail[lst], item);
    pfn$al_count[lst]++;
}

void primitive_rem_pfn(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    struct _pfn *item = pfndbe;
    if (item == 0)
    {
        item = &pfn$a_base[n];
    }
    LIST_REMOVE(pfn, pfn$al_head[lst], pfn$al_tail[lst], item);
    pfn$al_count[lst]--;
}

pfn_t primitive_rem_pfnh(unsigned int lst, struct _pfn **pfndbe)
{
    pfn_t retval = PFN_INVALID;
    *pfndbe = pfn$al_head[lst];
    if (*pfndbe != 0)
    {
        LIST_REMOVE(pfn, pfn$al_head[lst], pfn$al_tail[lst], *pfndbe);
        pfn$al_count[lst]--;
        retval = (*pfndbe)->pfn$id;
    }
    return retval;
}

#ifdef BOOTLOADER
void mmg_std$init_boot_pfn_no_db(pfn_t pfn, size_t n)
{
    boot_no_pfn_db_list.pmap$l_start_pfn = pfn;
    boot_no_pfn_db_list.pmap$l_pfn_count = (page_count_t)n;
}

void mmg_std$setup_pfn_no_db(void)
{
    free_no_pfn_db_list.pmap$l_start_pfn = boot_no_pfn_db_list.pmap$l_start_pfn;
    free_no_pfn_db_list.pmap$l_pfn_count = boot_no_pfn_db_list.pmap$l_pfn_count;
    mmg$gl_free_no_pfn_db_list = &free_no_pfn_db_list;
}
#endif
