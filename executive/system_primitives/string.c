/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

void *memchr(const void *s, int c, size_t n)
{
    unsigned char *p = (unsigned char *)(unsigned long)s;
    while (n--)
    {
        if (*p == (unsigned char)c)
        {
            return p;
        }
        else
        {
            p++;
        }
    }
    return 0;
}

int memcmp(const void* s1, const void* s2, size_t n)
{
    const unsigned char *p1 = s1;
    const unsigned char *p2 = s2;
    while (n--)
    {
        if (*p1 != *p2)
        {
            return *p1 - *p2;
        }
        p1++;
        p2++;
    }
    return 0;
}

void* memcpy(void* dest, const void* src, size_t n)
{
    unsigned char *d = dest;
    const unsigned char *s = src;

    while (n--)
    {
        *d++ = *s++;
    }
    return dest;
}

void *memmove(void *dest, const void *src, size_t n)
{
    unsigned char *d = dest;
    const unsigned char *s = src;
    if (s < d)
    {
        d += n;
        s += n;
        while (n--)
        {
            *--d = *--s;
        }
    }
    else
    {
        while (n--)
        {
            *d++ = *s++;
        }
    }
    return dest;
}

void *memset(void *s, int c, size_t n)
{
    unsigned char *p = s;
    while (n--)
    {
        *p++ = (unsigned char)c;
    }
    return s;
}

char *strcat(char *dest, const char *src)
{
    char *ret = dest;
    while (*dest != '\0')
    {
        dest++;
    }
    while (*src != '\0')
    {
        *dest++ = *src++;
    }
    *dest = '\0';
    return ret;
}

char *strchr(const char *s, int c)
{
    while (*s != (char)c)
    {
        if (*s == '\0')
        {
            return 0;
        }
        s++;
    }
    return (char *)(unsigned long)s;
}

int strcmp(const char* s1, const char* s2)
{
    while((*s1 != '\0') && (*s1 == *s2))
    {
        s1++;
        s2++;
    }
    return *s1 - *s2;
}

char *strcpy(char *dest, const char* src)
{
    char *ret = dest;
    while (*src != '\0')
    {
        *dest++ = *src++;
    }
    *dest = '\0';
    return ret;
}

size_t strcspn(const char *s1, const char *s2)
{
    size_t ret = 0;
    while (*s1 != '\0')
        if (strchr(s2, *s1) != 0)
        {
            return ret;
        }
        else
        {
            s1++;
            ret++;
        }
    return ret;
}

size_t strlen(const char *s)
{
    const char *p = s;
    while (*s)
    {
        ++s;
    }
    return (size_t)(s - p);
}

char *strncat(char *dest, const char *src, size_t n)
{
    char *ret = dest;
    while (*dest != '\0')
    {
        dest++;
    }
    while ((n > 0) && (*src != '\0'))
    {
        *dest++ = *src++;
        n--;
    }
    *dest = 0;
    return ret;
}

int strncmp(const char* s1, const char* s2, size_t n)
{
    while((n > 0) && (*s1 != '\0') && (*s1 == *s2))
    {
        n--;
        s1++;
        s2++;
    }
    if (n > 0)
    {
        return *s1 - *s2;
    }
    return 0;
}

char *strncpy(char *dest, const char* src, size_t n)
{
    char *ret = dest;
    while ((n > 0) && (*src != '\0'))
    {
        *dest++ = *src++;
        n--;
    }
    *dest = 0;
    return ret;
}

size_t strnlen(const char *s, size_t n)
{
    const char *p = s;
    while ((n > 0) && (*s != '\0'))
    {
        s++;
        n--;
    }
    return (size_t)(s - p);
}

char *strrchr(const char *s, int c)
{
    char *ret = 0;
    while (*s != '\0')
    {
        if (*s == (char)c)
        {
            ret = (char *)(unsigned long)s;
        }
        s++;
    }
    return ret;
}

size_t strspn(const char *s1, const char *s2)
{
    size_t ret = 0;
    while ((*s1 != '\0') && (strchr(s2, *s1++) != 0))
    {
        ret++;
    }
    return ret;
}

char *strstr(const char *s1, const char *s2)
{
    size_t n = strlen(s2);
    while (*s1 != '\0')
    {
        if (memcmp(s1, s2, n) != 0)
        {
            s1++;
        }
        else
        {
            return (char*)(unsigned long)s1;
        }
    }
    return 0;
}
