/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ssdef.h>

#include <memoryalc.h>
#include <system_data_cells.h>
#include <system_routines.h>
#include <util.h>

#define LOOKASIDE_LIST_SIZE 64
#define LOOKASIDE_LIST_SHIFT 6
#define ALLOC_MULTIPLE (1 << LOOKASIDE_LIST_SHIFT)
#define ALLOC_ALIGN_MASK (ALLOC_MULTIPLE - 1)
#define MAX_LAL_BLOCK_LENGTH (LOOKASIDE_LIST_SIZE * ALLOC_MULTIPLE)

struct _vlb * ioc$gq_listheads[LOOKASIDE_LIST_SIZE] = {0};

void primitive_lal_insert_first(struct _vlb ** head, struct _vlb *block)
{
    block->vlb$ps_next = *head;
    *head = block;
}

int primitive_lal_remove_first(struct _vlb ** head, struct _vlb **block)
{
    *block = *head;
    *head = (*head)->vlb$ps_next;
    return SS$_NORMAL;
}

int primitive_allocate(struct _vlb **head, size_t *size, void **ptr)
{
    *ptr = 0;
    struct _vlb *prev = 0;
    struct _vlb *curr = *head;

    smp$acquire(IPL$_POOL);
    /* find the first fit */
    while (curr != 0)
    {
        if (curr->vlb$l_size >= *size)
        {
            /* free block found */
            *ptr = (void *)curr;
            if ((curr->vlb$l_size - *size) > ALLOC_MULTIPLE)
            {
                /* large block: split it up */
                if (prev != 0)
                {
                    prev->vlb$ps_next = (struct _vlb *)((void *)curr + *size);
                    prev->vlb$ps_next->vlb$l_size = curr->vlb$l_size - *size;
                    prev->vlb$ps_next->vlb$ps_next = curr->vlb$ps_next;
                }
                else
                {
                    *head = (struct _vlb *)((void *)curr + *size);
                    (*head)->vlb$l_size = curr->vlb$l_size - *size;
                    (*head)->vlb$ps_next = curr->vlb$ps_next;
                }
            }
            else
            {
                /* close enough fit: return the entire block */
                *size = curr->vlb$l_size;
                if (prev != 0)
                {
                    prev->vlb$ps_next = curr->vlb$ps_next;
                }
                else
                {
                    *head = curr->vlb$ps_next;
                }
            }
            smp$restore(IPL$_POOL);
            return SS$_NORMAL;
        }
        prev = curr;
        curr = curr->vlb$ps_next;
    }
    smp$restore(IPL$_POOL);
    /* no large enough free blocks */
    return SS$_INSFMEM;
}

int primitive_deallocate(struct _vlb **head, void *ptr, size_t size)
{
    smp$acquire(IPL$_POOL);
    if (*head != 0)
    {
        struct _vlb *prev = 0;
        struct _vlb *curr = 0;
        struct _vlb *next = *head;
        /* find the previous and next blocks, based on the addresses of the blocks */
        while ((next != 0) && ((void*)next < ptr))
        {
            prev = next;
            next = next->vlb$ps_next;
        }
        /* if previous block is adjacent ... */
        if ((prev != 0) && ((void *)prev + prev->vlb$l_size) == ptr)
        {
            /* ... merge */
            prev->vlb$l_size += size;
            curr = prev;
        }
        else
        {
            /* ... else insert the block after the previous one */
            curr = (struct _vlb *)ptr;
            curr->vlb$ps_next = 0;
            curr->vlb$l_size = size;
            if (prev != 0)
            {
                prev->vlb$ps_next = curr;
            }
            else
            {
                *head = curr;
            }
        }
        /* if next block is adjacent ... */
        if ((next != 0) && ((void *)curr + curr->vlb$l_size) == (void *)next)
        {
            /* ... merge */
            curr->vlb$ps_next = next->vlb$ps_next;
            curr->vlb$l_size += next->vlb$l_size;
        }
        else
        {
            /* ... else link to the next block */
            curr->vlb$ps_next = next;
        }
    }
    else
    {
        /* start a new free block list */
        *head = (struct _vlb *)ptr;
        (*head)->vlb$ps_next = 0;
        (*head)->vlb$l_size = size;
    }
    smp$restore(IPL$_POOL);
    return SS$_NORMAL;
}

void primitive_extendpool(struct _vlb **head)
{
    smp$acquire(IPL$_POOL);
    SUPPRESS_UNUSED_WARNING(head);
    /* TODO */
    smp$restore(IPL$_POOL);
}

void primitive_flushlists(struct _vlb **head)
{
    smp$acquire(IPL$_POOL);
    for (unsigned int i = 0; i < LOOKASIDE_LIST_SIZE; i++)
    {
        struct _vlb **lal = &ioc$gq_listheads[i];
        while (*lal != 0)
        {
            struct _vlb *block = 0;
            if (primitive_lal_remove_first(lal, &block) == SS$_NORMAL)
            {
                primitive_deallocate(head, block, block->vlb$l_size);
            }
        }
    }
    smp$restore(IPL$_POOL);
}

void primitive_reclaimlists(struct _vlb **head, int aggressive)
{
    smp$acquire(IPL$_POOL);
    for (unsigned int i = 0; i < LOOKASIDE_LIST_SIZE; i++)
    {
        struct _vlb **lal = &ioc$gq_listheads[i];
        if ((*lal != 0) && (aggressive || (((*lal)->vlb$ps_next != 0) && ((*lal)->vlb$ps_next->vlb$ps_next != 0))))
        {
            struct _vlb *block = 0;
            if (primitive_lal_remove_first(lal, &block) == SS$_NORMAL)
            {
                primitive_deallocate(head, block, block->vlb$l_size);
            }
        }
    }
    smp$restore(IPL$_POOL);
}

int primitive_alononpaged(size_t *size, void **ptr)
{
    /* adjust size and force alignment by rounding up */
    *size = (*size + ALLOC_ALIGN_MASK) & (size_t)~ALLOC_ALIGN_MASK;

    /* try to allocate from lookaside list */
    if (*size <= MAX_LAL_BLOCK_LENGTH)
    {
        smp$acquire(IPL$_POOL);
        struct _vlb **lal = &ioc$gq_listheads[(*size >> LOOKASIDE_LIST_SHIFT) - 1];
        if (*lal != 0)
        {
            struct _vlb *block = 0;
            if (primitive_lal_remove_first(lal, &block) != SS$_NORMAL)
            {
                smp$restore(IPL$_POOL);
                *ptr = 0;
                return SS$_SSFAIL;
            }
            smp$restore(IPL$_POOL);
            *ptr = (void *)block;
            *size = block->vlb$l_size;
            return SS$_NORMAL;
        }
        smp$restore(IPL$_POOL);
    }

    /* no appropriate block found in lookaside lists, use nonpaged pool */
    int status = primitive_allocate(&exe$gl_nonpaged, size, ptr);
    if (status == SS$_INSFMEM)
    {
        primitive_reclaimlists(&exe$gl_nonpaged, 1);
        status = primitive_allocate(&exe$gl_nonpaged, size, ptr);
        if (status == SS$_INSFMEM)
        {
            primitive_extendpool(&exe$gl_nonpaged);
            status = primitive_allocate(&exe$gl_nonpaged, size, ptr);
            if (status == SS$_INSFMEM)
            {
                primitive_flushlists(&exe$gl_nonpaged);
                status = primitive_allocate(&exe$gl_nonpaged, size, ptr);
            }
        }
    }

    return status;
}

int primitive_deanonpaged(void *ptr, size_t size)
{
    int status = SS$_NORMAL;
    if (size <= MAX_LAL_BLOCK_LENGTH)
    {
        smp$acquire(IPL$_POOL);
        /* deallocate block to lookaside list */
        struct _vlb **lal = &ioc$gq_listheads[(size >> LOOKASIDE_LIST_SHIFT) - 1];
        struct _vlb *block = ptr;
        block->vlb$l_size = size;
        primitive_lal_insert_first(lal, block);
        smp$restore(IPL$_POOL);
    }
    else
    {
        /* block is too large for lookaside lists, add to nonpaged pool */
        status = primitive_deallocate(&exe$gl_nonpaged, ptr, size);
    }
    return status;
}
