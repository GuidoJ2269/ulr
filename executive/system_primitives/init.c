/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <allocpfn.h>
#include <ctype.h>
#include <doinit.h>
#include <kprint.h>
#include <memmap.h>
#include <memoryalc.h>
#include <mutex.h>
#include <stdlib.h>
#include <string.h>
#include <system_routines.h>

void init_system_primitives(void);

struct _inivec ini$a_vector_table[] = {{init_system_primitives, {{0}}}, {0, {{0}}}};

void init_system_primitives(void)
{
    base_image.decc_isalnum = isalnum;
    base_image.decc_isalpha = isalpha;
    base_image.decc_iscntrl = iscntrl;
    base_image.decc_isdigit = isdigit;
    base_image.decc_isgraph = isgraph;
    base_image.decc_islower = islower;
    base_image.decc_isprint = isprint;
    base_image.decc_ispunct = ispunct;
    base_image.decc_isspace = isspace;
    base_image.decc_isupper = isupper;
    base_image.decc_isxdigit = isxdigit;
    base_image.decc_isascii = isascii;
    base_image.decc_toascii = toascii;

    base_image.decc_tolower = tolower;
    base_image.decc_toupper = toupper;

    base_image.decc_abs = abs;
    base_image.decc_labs = labs;

    base_image.decc_memchr = memchr;
    base_image.decc_memcmp = memcmp;
    base_image.decc_memcpy = memcpy;
    base_image.decc_memmove = memmove;
    base_image.decc_memset = memset;

    base_image.decc_strcat = strcat;
    base_image.decc_strchr = strchr;
    base_image.decc_strcmp = strcmp;
    base_image.decc_strcpy = strcpy;
    base_image.decc_strcspn = strcspn;
    base_image.decc_strlen = strlen;
    base_image.decc_strncat = strncat;
    base_image.decc_strncmp = strncmp;
    base_image.decc_strncpy = strncpy;
    base_image.decc_strnlen = strnlen;
    base_image.decc_strrchr = strrchr;
    base_image.decc_strspn = strspn;
    base_image.decc_strstr = strstr;

    base_image.boo_map = primitive_map;
    base_image.boo_unmap = primitive_unmap;

    base_image.exe_kprintf = primitive_kprintf;
    base_image.exe_kvprintf = primitive_kvprintf;

    base_image.exe_allocate = primitive_allocate;
    base_image.exe_deallocate = primitive_deallocate;
    base_image.exe_alononpaged = primitive_alononpaged;
    base_image.exe_deanonpaged = primitive_deanonpaged;

    base_image.exe_extendpool = primitive_extendpool;
    base_image.exe_flushlists = primitive_flushlists;;
    base_image.exe_reclaimlists = primitive_reclaimlists;

    base_image.mmg_alloc_pfn = primitive_alloc_pfn;
    base_image.mmg_alloc_zero_pfn = primitive_alloc_zero_pfn;
    base_image.mmg_dalloc_pfn = primitive_dalloc_pfn;
    base_image.mmg_dalloc_zero_pfn = primitive_dalloc_zero_pfn;
    base_image.mmg_ins_pfnh = primitive_ins_pfnh;
    base_image.mmg_ins_pfnt = primitive_ins_pfnt;
    base_image.mmg_rem_pfn = primitive_rem_pfn;
    base_image.mmg_rem_pfnh = primitive_rem_pfnh;

    base_image.sch_lockr = primitive_lockr;
    base_image.sch_lockw = primitive_lockw;
    base_image.sch_locknowait = primitive_lockwnowait;
    base_image.sch_unlock = primitive_unlock;
}
