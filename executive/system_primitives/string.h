/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _PRIMITIVE_STRING_H
#define _PRIMITIVE_STRING_H

#include <stddef.h>

extern void *memchr(const void *s, int c, size_t n);
extern int memcmp(const void* s1, const void* s2, size_t n);
extern void *memcpy(void *dest, const void *src, size_t n);
extern void *memmove(void *dest, const void *src, size_t n);
extern void *memset(void *s, int c, size_t n);

extern char *strcat(char *dest, const char *src);
extern char *strchr(const char *s, int c);
extern int strcmp(const char * s1, const char * s2);
extern char *strcpy(char *dest, const char *src);
extern size_t strcspn(const char *s1, const char *s2);
extern size_t strlen(const char *s);
extern char *strncat(char *dest, const char* src, size_t n);
extern int strncmp(const char * s1, const char * s2, size_t n);
extern char *strncpy(char *dest, const char* src, size_t n);
extern size_t strnlen(const char *s, size_t n);
extern char *strrchr(const char *s, int c);
extern size_t strspn(const char *s1, const char *s2);
extern char *strstr(const char *s1, const char *s2);

#endif /* _PRIMITIVE_STRING_H */
