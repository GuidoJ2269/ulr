/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _MEMORY_ALC_H
#define _MEMORY_ALC_H

#include <stddef.h>

struct _vlb
{
    struct _vlb *vlb$ps_next;
    size_t       vlb$l_size;
};

extern void primitive_lal_insert_first(struct _vlb ** head, struct _vlb *block);
extern int primitive_lal_remove_first(struct _vlb ** head, struct _vlb **block);

extern int primitive_allocate(struct _vlb **head, size_t *size, void **ptr);
extern int primitive_deallocate(struct _vlb **head, void *ptr, size_t size);

extern void primitive_extendpool(struct _vlb **head);
extern void primitive_flushlists(struct _vlb **head);
extern void primitive_reclaimlists(struct _vlb **head, int aggressive);

extern int primitive_alononpaged(size_t *size, void **ptr);
extern int primitive_deanonpaged(void *ptr, size_t size);

#endif /* _MEMORY_ALC_H */
