/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _IRPDEF_H
#define _IRPDEF_H

#include <iodef.h>

/* I/O Request Packet */
typedef struct _irp
{
    struct _irp *irp$ps_flink;          /* I/O queue forward link */
    struct _irp *irp$ps_blink;          /* I/O queue backward link */
    union _iodef irp$ps_func;           /* I/O function code */
} IRP;

/*
 * Original definition:
 *
 * \*+                                                                         *\
 * \* IRP - I/O REQUEST PACKET                                                 *\
 * \*                                                                          *\
 * \* I/O REQUEST PACKETS ARE CONSTRUCTED BY THE QUEUE I/O REQUEST SYSTEM      *\
 * \* SERVICE. THE CONTENT OF AN I/O REQUEST PACKET DESCRIBES A FUNCTION TO    *\
 * \* BE PERFORMED ON A DEVICE UNIT.                                           *\
 * \*                                                                          *\
 * \* NOTE: Several fields of the IRP must be at the same offsets as their     *\
 * \* corresponding fields in the IRPE and CDRP.  The equivalency of these     *\
 * \* offsets is verified by ASSUME statements in the [LIB]VFY_IRP_A_LIKES.MAR *\
 * \* module.  These ASSUMEs may need to be altered as well whenever an IRP    *\
 * \* field is removed or altered.                                             *\
 * \*-                                                                         *\
 * #include <diobmdef.h>        \* Define the DIOBM type; IRP contains an embedded DIOBM type *\
 * #define IRP$M_WLE_REUSE 0x1
 * #define IRP$M_WLE_SUPWL 0x2
 * #define IRP$M_BUFIO 0x1
 * #define IRP$M_FUNC 0x2
 * #define IRP$M_PAGIO 0x4
 * #define IRP$M_COMPLX 0x8
 * #define IRP$M_VIRTUAL 0x10
 * #define IRP$M_CHAINED 0x20
 * #define IRP$M_SWAPIO 0x40
 * #define IRP$M_DIAGBUF 0x80
 * #define IRP$M_PHYSIO 0x100
 * #define IRP$M_TERMIO 0x200
 * #define IRP$M_MBXIO 0x400
 * #define IRP$M_EXTEND 0x800
 * #define IRP$M_FILACP 0x1000
 * #define IRP$M_MVIRP 0x2000
 * #define IRP$M_SRVIO 0x4000
 * #define IRP$M_CCB_LOOKED_UP 0x8000
 * #define IRP$M_CACHE_PAGIO 0x10000
 * #define IRP$M_FILL_BIT 0x20000
 * #define IRP$M_BUFOBJ 0x40000
 * #define IRP$M_TRUSTED 0x80000
 * #define IRP$M_FASTIO_DONE 0x100000
 * #define IRP$M_FASTIO 0x200000
 * #define IRP$M_FAST_FINISH 0x400000
 * #define IRP$M_DOPMS 0x800000
 * #define IRP$M_HIFORK 0x1000000
 * #define IRP$M_SRV_ABORT 0x2000000
 * #define IRP$M_LOCK_RELEASEABLE 0x4000000
 * #define IRP$M_DID_FAST_FDT 0x8000000
 * #define IRP$M_SYNCSTS 0x10000000
 * #define IRP$M_FINIPL8 0x20000000
 * #define IRP$M_START_PAST_HWM 0x1
 * #define IRP$M_END_PAST_HWM 0x2
 * #define IRP$M_ERASE 0x4
 * #define IRP$M_PART_HWM 0x8
 * #define IRP$M_LCKIO 0x10
 * #define IRP$M_SHDIO 0x20
 * #define IRP$M_CACHEIO 0x40
 * #define IRP$M_WLE 0x80
 * #define IRP$M_CACHE_SAFE 0x100
 * #define IRP$M_NOCACHE 0x200
 * #define IRP$M_ABORTIO 0x400
 * #define IRP$M_FORCEMV 0x800
 * #define IRP$M_HBRIO 0x1000
 * #define IRP$M_GOTSTK 0x2000
 * #define IRP$M_FCODE 0x3F
 * #define IRP$K_CDRP 304                  \* Offset to the CDRP within the IRP  *\
 * #define IRP$C_CDRP 304                  \* Offset to the CDRP within the IRP *\
 * #define IRP$M_PIO_ERROR 0x1
 * #define IRP$M_PIO_FANOUT 0x2
 * #define IRP$M_PIO_NOQUE 0x4
 * #define IRP$M_PIO_CANCEL 0x8
 * #define IRP$M_PIO_CTHRDOK 0x10
 * #define IRP$M_PIO_PHASEII 0x20
 * #define IRP$M_SHD_EXPEL_REMOVED 0x1
 * #define IRP$M_CLN_READY 0x1
 * #define IRP$M_CLN_DONE 0x2
 * #define IRP$M_CPY_FINI 0x4
 * #define IRP$K_BT_LEN 400
 * #define IRP$C_BT_LEN 400
 * #define IRP$K_CD_LEN 408
 * #define IRP$C_CD_LEN 408
 *
 *
 * #ifdef __cplusplus           \* Define structure prototypes *\
 * struct _wcb;
 * struct _ucb;
 * struct _ctxb;
 * struct _shad;
 * struct _hrb;
 * struct _bufio;
 * struct _irpe;
 * struct _fdt_context;
 * struct _arb;
 * struct _kpb;
 * struct _ccb;
 * struct _fkb;
 * struct _cdt;
 * struct _cdrp;
 * #endif       \* #ifdef __cplusplus *\
 *
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 * typedef struct _irp {
 * #pragma __nomember_alignment
 *     struct _irp *irp$l_ioqfl;           \*I/O QUEUE FORWARD LINK            *\
 *     struct _irp *irp$l_ioqbl;           \*I/O QUEUE BACKWARD LINK           *\
 *     unsigned short int irp$w_size;      \*SIZE OF IRP IN BYTES              *\
 *     unsigned char irp$b_type;           \*STRUCTURE TYPE FOR IRP            *\
 *     __union  {
 *         unsigned char irp$b_rmod;       \*ACCESS MODE OF REQUEST            *\
 *         __struct  {
 *             unsigned irp$v_mode : 2;    \* MODE SUBFIELD                    *\
 *             unsigned irp$v_fill_22 : 6;
 *             } irp$r_rmod_bits;
 *         } irp$r_rmod_overlay;
 *     unsigned int irp$l_pid;             \*PROCESS ID OF REQUESTING PROCESS  *\
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_param_0;          \*For PAGEFAULT and IOCIOPOST       *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_acb64x_offset;    \* Offset to ACB64X structure embedded in this IRP *\
 *             char irp$b_fill_23 [4];
 *             } irp$r_fill_1;
 *         } irp$r_fill_0;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_param_1;          \*For PAGEFAULT and IOCIOPOST       *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             unsigned int irp$l_acb_flags; \* ACB flags; valid only if ACB$M_FLAGS_VALID in RMOD set *\
 *             unsigned int irp$l_thread_pid; \* (Reserved for Kernel Threads) *\
 *             } irp$r_fill_3;
 *         } irp$r_fill_2;
 *     __union  {
 *         struct _wcb *irp$l_wind;        \*ADDRESS OF WINDOW BLOCK           *\
 *         struct _irp *irp$l_mirp;        \*LINK TO MASTER IRP                *\
 *         void (*irp$l_kast)();           \*PIGGY BACK KERNEL AST ADDRESS     *\
 *         } irp$r_wind_overlay;
 *     struct _ucb *irp$l_ucb;             \*ADDRESS OF DEVICE UCB             *\
 *     __union  {
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 * #ifdef __INITIAL_POINTER_SIZE             \* Defined whenever ptr size pragmas supported *\
 * #pragma __required_pointer_size __long        \* And set ptr size default to 64-bit pointers *\
 *         void (*irp$pq_acb64_ast)();     \* 64-bit user AST routine address  *\
 * #else
 *  unsigned __int64 irp$pq_acb64_ast;
 * #endif
 * #pragma __nomember_alignment
 * #ifdef __INITIAL_POINTER_SIZE             \* Defined whenever ptr size pragmas supported *\
 * #pragma __required_pointer_size __short       \* And set ptr size default to 32-bit pointers *\
 * #endif
 *         struct _irp *irp$l_shd_iofl;    \* Link to clone IRPs               *\
 *         struct _ctxb *irp$l_ctxb;       \* Link to CTXB                     *\
 *         int irp$l_iirp_p0;              \* Generic parameter cell in internal IRPs *\
 *         } irp$r_acb64_ast_overlay;
 *     __union  {
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *         unsigned __int64 irp$q_acb64_astprm; \* 64-bit user AST parameter value *\
 * #pragma __nomember_alignment
 *         struct _shad *irp$l_shad;       \* SHAD address                     *\
 *         struct _hrb *irp$l_hrb;         \* HRB address                      *\
 *         int irp$l_mv_tmo;               \* Timeout value in internal mount verification IRPs *\
 *         int irp$l_iirp_p1;              \* Generic parameter cell in internal IRPs *\
 *         } irp$r_acb64_astprm_overlay;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     unsigned __int64 irp$q_user_thread_id; \* Unique user thread identifier *\
 * #pragma __nomember_alignment
 *     unsigned char irp$b_efn;            \*EVENT FLAG NUMBER AND EVENT GROUP  *\
 *     unsigned char irp$b_pri;            \*BASE PRIORITY OF REQUESTING PROCESS  *\
 *     unsigned char irp$b_cln_indx;       \*Shadow Clone membership index     *\
 *     __union  {                          \* Write log flags.                 *\
 *         unsigned char irp$b_wlg_flags;  \* These flags are shared by DUDRIVER and SHDRIVER and MSCP. *\
 *         __struct  {                     \* Write log Flags Status Bits      *\
 *             unsigned irp$v_wle_reuse : 1; \* Reuse writelog entry           *\
 *             unsigned irp$v_wle_supwl : 1; \* Supplementary writelog         *\
 *             unsigned irp$v_fill_24 : 6;
 *             } irp$r_wlg_flag_bits;
 * \*                                                                          *\
 *         } irp$r_wlg_flags_overlay;
 * \*                                                                          *\
 *     unsigned int irp$l_chan;            \* Process I/O channel              *\
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 * #ifdef __INITIAL_POINTER_SIZE             \* Defined whenever ptr size pragmas supported *\
 * #pragma __required_pointer_size __long        \* And set ptr size default to 64-bit pointers *\
 *         void *irp$pq_iosb;              \* 64-bit address of caller's IOSB  *\
 * #else
 *  unsigned __int64 irp$pq_iosb;
 * #endif
 *         __struct  {
 * #pragma __nomember_alignment
 *             __union  {
 *                 unsigned int irp$l_cln_wle; \* write log entry              *\
 *                 __int64 irp$q_param_2;  \* For PAGEFAULT and IOCIOPOST (Kthreads) *\
 *                 int irp$l_iirp_p2;      \* Generic parameter cell in internal IRPs *\
 *                 } irp$r_iosb_overlay;
 *             } irp$r_fill_5;
 *         } irp$r_fill_4;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         unsigned __int64 irp$q_status;  \*Big time REQUEST STATUS           *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             __union  {
 *                 unsigned int irp$l_sts; \* Status                           *\
 *                 __struct  {
 *                     unsigned irp$v_bufio : 1; \* BUFFERED I/O FLAG ;THESE BITS  *\
 *                     unsigned irp$v_func : 1; \* 1=>READ FUNCTION ;MUST BE ADJACENT  *\
 *                     unsigned irp$v_pagio : 1; \* PAGING I/O FLAG ;AND IN ORDER  *\
 *                     unsigned irp$v_complx : 1; \* COMPLEX BUFFERED I/O      *\
 *                     unsigned irp$v_virtual : 1; \* VIRTUAL I/O FUNCTION     *\
 *                     unsigned irp$v_chained : 1; \* CHAINED BUFFERED I/O OPERATION  *\
 *                     unsigned irp$v_swapio : 1; \* SWAP I/O OPERATION        *\
 *                     unsigned irp$v_diagbuf : 1; \* DIAGNOSTIC BUFFER ALLOCATED  *\
 *                     unsigned irp$v_physio : 1; \* PHYSICAL I/O              *\
 *                     unsigned irp$v_termio : 1; \* TERMINAL I/O (FOR SELECTING PRIORITY INC)  *\
 *                     unsigned irp$v_mbxio : 1; \* MAILBOX BUFFERED READ      *\
 *                     unsigned irp$v_extend : 1; \* AN IRPE IS LINKED TO THIS IRP  *\
 *                     unsigned irp$v_filacp : 1; \* FILE ACP I/O (BOTH DIOCNT AND BIOCNT)  *\
 *                     unsigned irp$v_mvirp : 1; \* MOUNT VERIFICATION IRP     *\
 *                     unsigned irp$v_srvio : 1; \* SERVER TYPE I/O (TRIGGER MOUNTVER ON ERROR BUT DON'T STALL) *\
 *                     unsigned irp$v_ccb_looked_up : 1; \* Set if IRP$PS_CCB contains valid CCB address *\
 *                     unsigned irp$v_cache_pagio : 1; \* Cached page i/o      *\
 *                     unsigned irp$v_fill_bit : 1; \* Unused                  *\
 *                     unsigned irp$v_bufobj : 1; \* Set if buffer object I/O  *\
 *                     unsigned irp$v_trusted : 1; \* Set if trusted Component I/O *\
 *                     unsigned irp$v_fastio_done : 1; \* Set if this is an available Fast-IO IRP *\
 *                     unsigned irp$v_fastio : 1; \* Set if IRP created by $IO_SETUP -- special delete action *\
 *                     unsigned irp$v_fast_finish : 1; \* Set if IPL8 completion is expected *\
 *                     unsigned irp$v_dopms : 1; \* =1 if this IRP should call PMS$ logging routines *\
 *                     unsigned irp$v_hifork : 1; \* Device fork IPL > IPL$C_SCS *\
 *                     unsigned irp$v_srv_abort : 1; \* Server I/O should be aborted *\
 *                     unsigned irp$v_lock_releaseable : 1; \* Forklock can be released in favor of PM spinlock on Start I/O *\
 *                     unsigned irp$v_did_fast_fdt : 1; \* Fast-IO may have locked buffers via standard FDT dispatch *\
 *                     unsigned irp$v_syncsts : 1; \* VIOC can return SS$_SYNC on HIT if set. *\
 *                     unsigned irp$v_finipl8 : 1; \* Finish at IPL8 hook      *\
 *                     unsigned irp$v_fill_25 : 2;
 *                     } irp$r_fill_9;
 *                 } irp$r_fill_8;
 *             __union  {
 *                 unsigned int irp$l_sts2; \* EXTENSION OF STATUS WORD        *\
 *                 __struct  {
 *                     unsigned irp$v_start_past_hwm : 1; \* I/O STARTS PAST HIGHWATER MARK *\
 *                     unsigned irp$v_end_past_hwm : 1; \* I/O ENDS PAST HIGHWATER MARK *\
 *                     unsigned irp$v_erase : 1; \* ERASE I/O FUNCTION         *\
 *                     unsigned irp$v_part_hwm : 1; \* PARTIAL HIGHWATER MARK UPDATE *\
 *                     unsigned irp$v_lckio : 1; \* Locked I/O request (DECnet) *\
 *                     unsigned irp$v_shdio : 1; \* This is a shadowing IRP    *\
 *                     unsigned irp$v_cacheio : 1; \* uses VBN cache buffers   *\
 *                     unsigned irp$v_wle : 1; \* I/O USES A WRITE LOG ENTRY   *\
 *                     unsigned irp$v_cache_safe : 1; \* this indicates that   *\
 * \* the request has been                                                     *\
 * \* checked as regards                                                       *\
 * \* caching.                                                                 *\
 *                     unsigned irp$v_nocache : 1; \* IO$M_NOVCACHE was        *\
 * \* set in QIO function                                                      *\
 *                     unsigned irp$v_abortio : 1; \* set in EXE$ABORTIO       *\
 *                     unsigned irp$v_forcemv : 1; \* set to indicate forced MV in progress *\
 *                     unsigned irp$v_hbrio : 1; \* This is a host based raid IRP. *\
 *          unsigned irp$v_gotstk : 1;
 *                     unsigned irp$v_fill_26 : 2;
 *                     } irp$r_fill_11;
 *                 } irp$r_fill_10;
 *             } irp$r_fill_7;
 *         } irp$r_fill_6;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 * #ifdef __INITIAL_POINTER_SIZE             \* Defined whenever ptr size pragmas supported *\
 * #pragma __required_pointer_size __long        \* And set ptr size default to 64-bit pointers *\
 *     struct _pte *irp$pq_va_pte;         \* 64-bit process virtual addr of PTE  *\
 * #else
 *  unsigned __int64 irp$pq_va_pte;
 * #endif
 * #pragma __nomember_alignment
 *     __union  {
 * #ifdef __INITIAL_POINTER_SIZE             \* Defined whenever ptr size pragmas supported *\
 * #pragma __required_pointer_size __short       \* And set ptr size default to 32-bit pointers *\
 * #endif
 *         void *irp$l_svapte;             \* 32-bit S0/S1 address of first PTE  *\
 *         struct _bufio *irp$ps_bufio_pkt; \* Pointer to buffered I/O packet  *\
 *         } irp$r_svapte_overlay;
 *     unsigned int irp$l_bcnt;            \*BYTE COUNT OF TRANSFER            *\
 *     unsigned int irp$l_boff;            \* Byte offset                      *\
 *     __union  {
 *         unsigned int irp$l_oboff;       \* Original BOFF, for segmented DIO *\
 *         unsigned int irp$l_aboff;       \* "Ambient" BOFF, for NETDRIVER    *\
 *         } irp$r_oboff_overlay;
 *     struct _irpe *irp$l_extend;         \* ADDRESS OF IRPE                  *\
 *     struct _fdt_context *irp$ps_fdt_context; \* Contains addr of the FDT Context structure *\
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     DIOBM irp$r_diobm;                  \* Embedded DIOBM to handle cross-process 32-bit PTE access *\
 * #pragma __nomember_alignment
 *     __union  {
 *         unsigned int irp$l_iost1;       \*FIRST I/O STATUS LONGWORD (FOR I/O POST)  *\
 *         int irp$l_media;                \*MEDIA ADDRESS                     *\
 *         } irp$r_iost1_overlay;
 *     __union  {
 *         unsigned int irp$l_iost2;       \*SECOND I/O STATUS LONGWORD        *\
 *         __union  {
 *             int irp$l_tt_term;          \*ADDRESS OF READ TERMINATORS MASK  *\
 *             unsigned char irp$b_carcon; \*CARRIAGE CONTROL                  *\
 *             } irp$r_tt_term_overlay;
 *         __union  {
 *             unsigned short int irp$w_shd_copy_type; \*TYPE OF COPY TO PERFORM *\
 *             __struct  {
 *                 unsigned short int irp$w_shd_vun; \* VIRTUAL UNIT NUMBER    *\
 *                 __union  {
 *                     unsigned short int irp$w_shd_dev_type; \*DEVICE TYPE    *\
 *                     unsigned short int irp$w_shd_mscp_disk_modifier; \*FIELD FOR MODIFIERS *\
 *                     } irp$r_shd_iost2_inner;
 *                 } irp$r_shd_iost2_stuff;
 *             } irp$r_shd_iost2_overlay;
 *         } irp$r_iost2_overlay;
 *     __union  {
 *         unsigned __int64 irp$q_nt_prvmsk; \* PRIVILEGE MASK FOR DECNET      *\
 *         unsigned __int64 irp$q_station; \* STATION FIELD FOR DECNET DRIVERS *\
 *         __union  {
 *             unsigned __int64 irp$q_tt_state; \* TERMINAL STATE DEFINITIONS  *\
 *             __struct  {
 *                 unsigned int irp$l_abcnt; \* ACCUMULATED BYTES TRANSFERED   *\
 *                 unsigned int irp$l_obcnt; \* ORIGINAL TRANSFER BYTE COUNT   *\
 *                 } irp$r_tt_state_fields;
 *             } irp$r_tt_state_overlay;
 *         } irp$r_nt_prvmsk_overlay;
 *     __union  {
 *         unsigned int irp$l_func;        \* I/O function code                *\
 *         __struct  {
 *             unsigned irp$v_fcode : 6;   \* FUNCTION CODE FIELD              *\
 *             unsigned irp$v_fmod : 10;   \* FUNCTION MODIFIER FIELD          *\
 *             } irp$r_func_bits;
 *         } irp$r_func_overlay;
 *     unsigned int irp$l_segvbn;          \* VIRTUAL BLOCK NUMBER OF CURRENT SEGMENT  *\
 *     __union  {
 *         void *irp$l_diagbuf;            \* DIAGNOSTIC BUFFER ADDRESS        *\
 *         void *irp$l_scb_buf;            \* SCB BUFFER ADDRESS               *\
 *         unsigned short int irp$w_tt_prmpt; \* PROMPT SIZE                   *\
 *         } irp$r_diagbuf_overlay;
 *     __union  {
 *         unsigned int irp$l_seqnum;      \* SEQUENCE NUMBER                  *\
 *         struct _ucb *irp$l_dcd_src_ucb; \* DISK COPY DATA SOURCE UCB        *\
 *         } irp$r_seqnum_overlay;
 *     struct _arb *irp$l_arb;             \* ACCESS RIGHTS BLOCK ADDRESS      *\
 *     __union  {
 *         void *irp$l_keydesc;            \* ADDRESS OF ENCRYPTION DESCRIPTOR *\
 *         unsigned int irp$l_wle_ptr;     \* Clone Write log index            *\
 *         unsigned char irp$b_cpy_mode;   \* Copy mode identifier             *\
 *         } irp$r_keydesc_overlay;
 *     struct _kpb *irp$ps_kpb;            \* Pointer to KP block              *\
 *     struct _ccb *irp$ps_ccb;            \* Pointer to CCB for this I/O      *\
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_qio_p1;           \* QIO argument #1 (64-bits)        *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_qio_p1;           \*     (low-order 32-bit)           *\
 *             char irp$b_fill_27 [4];
 *             } irp$r_fill_13;
 *         } irp$r_fill_12;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_qio_p2;           \* QIO argument #2 (64-bits)        *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_qio_p2;           \*     (low-order 32-bit)           *\
 *             char irp$b_fill_28 [4];
 *             } irp$r_fill_15;
 *         } irp$r_fill_14;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 * #pragma __nomember_alignment
 *         __int64 irp$q_qio_p3;           \* QIO argument #3 (64-bits)        *\
 *         int irp$l_qio_p3;               \*     (low-order 32-bit)           *\
 *         __int64 irp$q_param_3;          \*     (for PAGEFAULT and IOCIOPOST) *\
 *         } irp$r_qio_p3_overlay;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_qio_p4;           \* QIO argument #4 (64-bits)        *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_qio_p4;           \*     (low-order 32-bit)           *\
 *             char irp$b_fill_29 [4];
 *             } irp$r_fill_17;
 *         } irp$r_fill_16;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_qio_p5;           \* QIO argument #5 (64-bits)        *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_qio_p5;           \*     (low-order 32-bit)           *\
 *             char irp$b_fill_30 [4];
 *             } irp$r_fill_19;
 *         } irp$r_fill_18;
 * #if !defined(__NOBASEALIGN_SUPPORT)  && !defined(__cplusplus)   \* If using pre DECC V4.0 or C++ *\
 * #pragma __nomember_alignment __quadword
 * #else
 * #pragma __nomember_alignment
 * #endif
 *     __union  {
 *         __int64 irp$q_qio_p6;           \* QIO argument #6 (64-bits)        *\
 *         __struct  {
 * #pragma __nomember_alignment
 *             int irp$l_qio_p6;           \*     (low-order 32-bit)           *\
 *             char irp$b_fill_31 [4];
 *             } irp$r_fill_21;
 *         } irp$r_fill_20;
 * \* ALL FIELDS INSERTED ABOVE THIS POINT IN THE IRP                   *\
 * \* MUST BE CHANGED IN THE CDRPDEF.SDL FILE.                      *\
 *
 * \* Standard IRP must contain space for Class Driver CDRP fields.            *\
 *     struct _fkb *irp$l_fqfl;            \* Fork Queue FLINK                 *\
 *     struct _fkb *irp$l_fqbl;            \* Fork Queue Blink                 *\
 *     unsigned short int irp$w_cdrpsize;  \* Size field for positive section only  *\
 *     unsigned char irp$b_cd_type;        \* Type, always of interest         *\
 *     unsigned char irp$b_flck;           \* Fork Lock number                 *\
 *     void (*irp$l_fpc)();                \* Fork PC                          *\
 *     __int64 irp$q_fr3;                  \* Fork R3                          *\
 *     __int64 irp$q_fr4;                  \* Fork R4                          *\
 *     void (*irp$l_savd_rtn)();           \* Saved return address from level 1 JSB  *\
 *     void *irp$l_msg_buf;                \* Address of allocated MSCP buffer  *\
 *     unsigned int irp$l_rspid;           \* Allocated Request ID             *\
 *     struct _cdt *irp$l_cdt;             \* Address of Connection Descriptor Table  *\
 *     unsigned __int64 irp$q_res_wait_state; \* SCS Resource Wait State       *\
 *     int irp$l_scs_stall_data;           \* Data cell used by SCS to save data over a stall *\
 *     void *irp$l_rwcptr;                 \* RWAITCNT pointer                 *\
 *     void *irp$l_bd_addr;                \* Address of Buffer Descriptor that maps I/O buffer *\
 *     void *irp$l_rbun;                   \* Address of Resource Bundle       *\
 *     void *irp$l_lbufh_ad;               \* Local BUFfer Handle ADress       *\
 *     char irp$b_fill_32 [4];
 *
 * \*   Extensions to the CDRP within the IRP                               *\
 *     __union  {
 * \* Host-Based Shadowing Extension                                           *\
 *         __struct  {
 *             unsigned char irp$b_shd_pio_cnt; \* Tot num phys IRPs assoc.    *\
 *             unsigned char irp$b_shd_pio_act; \* Tot num phys IRPs active.   *\
 * \* Note Keep SHD_PIO_FLAGS, SHD_PIO_ERRCNT,  contiguous.                    *\
 *             __union  {
 *                 unsigned char irp$b_shd_pio_flags; \* Master Flags Byte     *\
 *                 __struct  {
 *                     unsigned irp$v_pio_error : 1; \* Errant clone in Chain  *\
 *                     unsigned irp$v_pio_fanout : 1; \* Chained Clones.       *\
 *                     unsigned irp$v_pio_noque : 1; \* Don't queue to server  *\
 *                     unsigned irp$v_pio_cancel : 1; \* This master cancelled *\
 *                     unsigned irp$v_pio_cthrdok : 1; \* Copy thread validated. *\
 *                     unsigned irp$v_pio_phaseii : 1; \* Bi-phasic Phase II write *\
 *                     unsigned irp$v_fill_33 : 2;
 *                     } irp$r_pio_bits;
 *                 } irp$r_pio_flags_overlay;
 *             unsigned char irp$b_shd_pio_errcnt; \* Number of errors in chain *\
 *             unsigned char irp$b_shd_pio_errindex; \* Index of erring device *\
 *             unsigned char irp$b_shd_pio_errsev; \* Relative error severity  *\
 *             short int irp$w_shd_filler;
 *             unsigned __int64 irp$q_shd_lock_fr0; \* Lock fork R0            *\
 *             unsigned __int64 irp$q_shd_lock_fr1; \* Lock fork R1            *\
 *             unsigned __int64 irp$q_shd_lock_fr2; \* Lock fork R2            *\
 *             unsigned __int64 irp$q_shd_lock_fr4; \* Lock fork R4            *\
 *             unsigned __int64 irp$q_shd_lock_fr5; \* Lock fork R5            *\
 *             void (*irp$l_shd_lock_fpc)(); \* Lock fork PC                   *\
 *             unsigned int irp$l_shd_pio_error; \* BCNT and Error Status (SS$_) *\
 *             struct _irp *irp$l_shd_pio_lnk; \* Link to clone IRP(s)         *\
 *             int (*irp$l_shdspc)();      \* Shadowing return PC              *\
 *             struct _irp *irp$l_shd_control_irp; \* address of control IRP   *\
 *             int irp$l_shd_temp;         \* used for temporary storage       *\
 *             unsigned __int64 irp$q_shd_saved_r1; \* second save area for WLG *\
 *             unsigned __int64 irp$q_shd_saved_r2;
 *             unsigned __int64 irp$q_shd_saved_r4;
 *             unsigned int irp$l_shd_svd_cnt_irp; \* save SHD_CONTROL_IRP     *\
 *             unsigned int irp$l_shd_saved_status; \* save area for status    *\
 *             unsigned int irp$l_shd_wlg_mode_fpc; \* saved PC for WLG_MODE fork *\
 *             unsigned int irp$l_shd_perlkid; \* holds sublock id for         *\
 * \* per-disk                                                                 *\
 *             unsigned int irp$l_shd_expel_timer; \* Clone error timer        *\
 *             __union  {
 *                 unsigned int irp$l_shd_expel_flags; \* Clone IRP flags      *\
 *                 __struct  {
 *                     unsigned irp$v_shd_expel_removed : 1; \* Device is expelled *\
 *                     unsigned irp$v_fill_34 : 7;
 *                     } irp$r_expel_bits;
 *                 } irp$r_expel_flags_overlay;
 *             unsigned int irp$l_shd_expel_mask; \* indicate units to be expelled in MIRP *\
 *             unsigned __int64 irp$q_shd_reserv_q8; \* will be needed for 64-bit saves *\
 *             unsigned __int64 irp$q_shd_reserv_q9; \* will be needed for 64-bit saves *\
 *             unsigned __int64 irp$q_shd_reserv_q10; \* will be needed for 64-bit saves *\
 *             __union  {
 *                 unsigned char irp$b_shd_flags; \* Shadow Clone Flags        *\
 *                 __struct  {             \* Clone Flags Status Bits          *\
 *                     unsigned irp$v_cln_ready : 1; \* Clone is ready for I/O *\
 *                     unsigned irp$v_cln_done : 1; \* Clone has done I/O      *\
 *                     unsigned irp$v_cpy_fini : 1; \* Copy is complete.       *\
 *                     unsigned irp$v_fill_35 : 5;
 *                     } irp$r_shd_flag_bits;
 * \*                                                                          *\
 *                 } irp$r_shd_flags_overlay;
 *             unsigned long irp$l_curcspx; \* Current context stack pointer *\
 *      long irp$l_stkflgsx;
 * \*            struct stkfs {*\
 * \*                   unsigned irp$v_onstackx : 1;*\ \* current data is on the ctx stk *\
 * \*                   unsigned irp$v_csfil : 31; *\
 * \*            } irp$l_stkflgsx; *\
 *             unsigned long irp$a_ctxstkx[IRP_CTXSTKSIZ];
 *
\*                                                                          *\
 *             } irp$r_shadowing_extension;
 * \*   Block Transfer Extension                                            *\
 *         __struct  {
 *             unsigned int irp$l_lboff;   \* Local Byte OFFset                *\
 *             __union  {
 *                 void *irp$l_rbufh_ad;   \* Remote BUFfer Handle ADress      *\
 *                 struct _cdrp *irp$l_cdrpfl;
 *                 } irp$r_rbufh_ad_overlay;
 *             unsigned int irp$l_rboff;   \* Remote Byte OFFset               *\
 *             unsigned int irp$l_xct_len; \* Transfer length in bytes         *\
 *             } irp$r_blk_xfer_extension;
 * \*   Class Driver Extension                                              *\
 *         __struct  {
 *             char irp$t_lbufhndl [12];   \* Local buffer handle              *\
 *             unsigned int irp$l_ubarsrce; \* Scratch Cell used for DU/TUDRIVER convenience *\
 *             unsigned int irp$l_dutuflags; \* Class driver status flags:     *\
 *             unsigned short int irp$w_dutucntr; \* General purpose counter   *\
 *             unsigned short int irp$w_endmsgsiz; \* Size of most recent MSCP end message *\
 *             } irp$r_cls_drv_extension;
 * \* File system extensions                                                   *\
 *         unsigned int irp$l_erase_vbn;   \* VBN to start HWM erase           *\
 *         } irp$r_cdrp_extensions;
 *     char irp$b_fill_36 [3];
 *     } IRP;
 *
 * #define irp$b_rmod irp$r_rmod_overlay.irp$b_rmod
 * #define irp$v_mode irp$r_rmod_overlay.irp$r_rmod_bits.irp$v_mode
 * #define irp$q_param_0 irp$r_fill_0.irp$q_param_0
 * #define irp$l_acb64x_offset irp$r_fill_0.irp$r_fill_1.irp$l_acb64x_offset
 * #define irp$q_param_1 irp$r_fill_2.irp$q_param_1
 * #define irp$l_acb_flags irp$r_fill_2.irp$r_fill_3.irp$l_acb_flags
 * #define irp$l_thread_pid irp$r_fill_2.irp$r_fill_3.irp$l_thread_pid
 * #define irp$l_wind irp$r_wind_overlay.irp$l_wind
 * #define irp$l_mirp irp$r_wind_overlay.irp$l_mirp
 * #define irp$l_kast irp$r_wind_overlay.irp$l_kast
 * #define irp$pq_acb64_ast irp$r_acb64_ast_overlay.irp$pq_acb64_ast
 * #define irp$l_shd_iofl irp$r_acb64_ast_overlay.irp$l_shd_iofl
 * #define irp$l_ctxb irp$r_acb64_ast_overlay.irp$l_ctxb
 * #define irp$l_iirp_p0 irp$r_acb64_ast_overlay.irp$l_iirp_p0
 * #define irp$q_acb64_astprm irp$r_acb64_astprm_overlay.irp$q_acb64_astprm
 * #define irp$l_shad irp$r_acb64_astprm_overlay.irp$l_shad
 * #define irp$l_hrb irp$r_acb64_astprm_overlay.irp$l_hrb
 * #define irp$l_mv_tmo irp$r_acb64_astprm_overlay.irp$l_mv_tmo
 * #define irp$l_iirp_p1 irp$r_acb64_astprm_overlay.irp$l_iirp_p1
 * #define irp$b_wlg_flags irp$r_wlg_flags_overlay.irp$b_wlg_flags
 * #define irp$v_wle_reuse irp$r_wlg_flags_overlay.irp$r_wlg_flag_bits.irp$v_wle_reuse
 * #define irp$v_wle_supwl irp$r_wlg_flags_overlay.irp$r_wlg_flag_bits.irp$v_wle_supwl
 * #define irp$pq_iosb irp$r_fill_4.irp$pq_iosb
 * #define irp$l_cln_wle irp$r_fill_4.irp$r_fill_5.irp$r_iosb_overlay.irp$l_cln_wle
 * #define irp$q_param_2 irp$r_fill_4.irp$r_fill_5.irp$r_iosb_overlay.irp$q_param_2
 * #define irp$l_iirp_p2 irp$r_fill_4.irp$r_fill_5.irp$r_iosb_overlay.irp$l_iirp_p2
 * #define irp$q_status irp$r_fill_6.irp$q_status
 * #define irp$l_sts irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$l_sts
 * #define irp$v_bufio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_bufio
 * #define irp$v_func irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_func
 * #define irp$v_pagio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_pagio
 * #define irp$v_complx irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_complx
 * #define irp$v_virtual irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_virtual
 * #define irp$v_chained irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_chained
 * #define irp$v_swapio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_swapio
 * #define irp$v_diagbuf irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_diagbuf
 * #define irp$v_physio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_physio
 * #define irp$v_termio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_termio
 * #define irp$v_mbxio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_mbxio
 * #define irp$v_extend irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_extend
 * #define irp$v_filacp irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_filacp
 * #define irp$v_mvirp irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_mvirp
 * #define irp$v_srvio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_srvio
 * #define irp$v_ccb_looked_up irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_ccb_looked_up
 * #define irp$v_cache_pagio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_cache_pagio
 * #define irp$v_fill_bit irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_fill_bit
 * #define irp$v_bufobj irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_bufobj
 * #define irp$v_trusted irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_trusted
 * #define irp$v_fastio_done irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_fastio_done
 * #define irp$v_fastio irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_fastio
 * #define irp$v_fast_finish irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_fast_finish
 * #define irp$v_dopms irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_dopms
 * #define irp$v_hifork irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_hifork
 * #define irp$v_srv_abort irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_srv_abort
 * #define irp$v_lock_releaseable irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_lock_releaseable
 * #define irp$v_did_fast_fdt irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_did_fast_fdt
 * #define irp$v_syncsts irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_syncsts
 * #define irp$v_finipl8 irp$r_fill_6.irp$r_fill_7.irp$r_fill_8.irp$r_fill_9.irp$v_finipl8
 * #define irp$l_sts2 irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$l_sts2
 * #define irp$v_start_past_hwm irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_start_past_hwm
 * #define irp$v_end_past_hwm irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_end_past_hwm
 * #define irp$v_erase irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_erase
 * #define irp$v_part_hwm irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_part_hwm
 * #define irp$v_lckio irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_lckio
 * #define irp$v_shdio irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_shdio
 * #define irp$v_cacheio irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_cacheio
 * #define irp$v_wle irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_wle
 * #define irp$v_cache_safe irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_cache_safe
 * #define irp$v_nocache irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_nocache
 * #define irp$v_abortio irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_abortio
 * #define irp$v_forcemv irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_forcemv
 * #define irp$v_hbrio irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_hbrio
 * #define irp$v_gotstk irp$r_fill_6.irp$r_fill_7.irp$r_fill_10.irp$r_fill_11.irp$v_gotstk
 * #define irp$l_svapte irp$r_svapte_overlay.irp$l_svapte
 * #define irp$ps_bufio_pkt irp$r_svapte_overlay.irp$ps_bufio_pkt
 * #define irp$l_oboff irp$r_oboff_overlay.irp$l_oboff
 * #define irp$l_aboff irp$r_oboff_overlay.irp$l_aboff
 * #define irp$l_iost1 irp$r_iost1_overlay.irp$l_iost1
 * #define irp$l_media irp$r_iost1_overlay.irp$l_media
 * #define irp$l_iost2 irp$r_iost2_overlay.irp$l_iost2
 * #define irp$l_tt_term irp$r_iost2_overlay.irp$r_tt_term_overlay.irp$l_tt_term
 * #define irp$b_carcon irp$r_iost2_overlay.irp$r_tt_term_overlay.irp$b_carcon
 * #define irp$w_shd_copy_type irp$r_iost2_overlay.irp$r_shd_iost2_overlay.irp$w_shd_copy_type
 * #define irp$w_shd_vun irp$r_iost2_overlay.irp$r_shd_iost2_overlay.irp$r_shd_iost2_stuff.irp$w_shd_vun
 * #define irp$w_shd_dev_type irp$r_iost2_overlay.irp$r_shd_iost2_overlay.irp$r_shd_iost2_stuff.irp$r_shd_iost2_inner.irp$w_shd_dev_ty\
 * pe
 * #define irp$w_shd_mscp_disk_modifier irp$r_iost2_overlay.irp$r_shd_iost2_overlay.irp$r_shd_iost2_stuff.irp$r_shd_iost2_inner.irp$w_\
 * shd_mscp_disk_modifier
 * #define irp$q_nt_prvmsk irp$r_nt_prvmsk_overlay.irp$q_nt_prvmsk
 * #define irp$q_station irp$r_nt_prvmsk_overlay.irp$q_station
 * #define irp$q_tt_state irp$r_nt_prvmsk_overlay.irp$r_tt_state_overlay.irp$q_tt_state
 * #define irp$l_abcnt irp$r_nt_prvmsk_overlay.irp$r_tt_state_overlay.irp$r_tt_state_fields.irp$l_abcnt
 * #define irp$l_obcnt irp$r_nt_prvmsk_overlay.irp$r_tt_state_overlay.irp$r_tt_state_fields.irp$l_obcnt
 * #define irp$l_func irp$r_func_overlay.irp$l_func
 * #define irp$v_fcode irp$r_func_overlay.irp$r_func_bits.irp$v_fcode
 * #define irp$v_fmod irp$r_func_overlay.irp$r_func_bits.irp$v_fmod
 * #define irp$l_diagbuf irp$r_diagbuf_overlay.irp$l_diagbuf
 * #define irp$l_scb_buf irp$r_diagbuf_overlay.irp$l_scb_buf
 * #define irp$w_tt_prmpt irp$r_diagbuf_overlay.irp$w_tt_prmpt
 * #define irp$l_seqnum irp$r_seqnum_overlay.irp$l_seqnum
 * #define irp$l_dcd_src_ucb irp$r_seqnum_overlay.irp$l_dcd_src_ucb
 * #define irp$l_keydesc irp$r_keydesc_overlay.irp$l_keydesc
 * #define irp$l_wle_ptr irp$r_keydesc_overlay.irp$l_wle_ptr
 * #define irp$b_cpy_mode irp$r_keydesc_overlay.irp$b_cpy_mode
 * #define irp$q_qio_p1 irp$r_fill_12.irp$q_qio_p1
 * #define irp$l_qio_p1 irp$r_fill_12.irp$r_fill_13.irp$l_qio_p1
 * #define irp$q_qio_p2 irp$r_fill_14.irp$q_qio_p2
 * #define irp$l_qio_p2 irp$r_fill_14.irp$r_fill_15.irp$l_qio_p2
 * #define irp$q_qio_p3 irp$r_qio_p3_overlay.irp$q_qio_p3
 * #define irp$l_qio_p3 irp$r_qio_p3_overlay.irp$l_qio_p3
 * #define irp$q_param_3 irp$r_qio_p3_overlay.irp$q_param_3
 * #define irp$q_qio_p4 irp$r_fill_16.irp$q_qio_p4
 * #define irp$l_qio_p4 irp$r_fill_16.irp$r_fill_17.irp$l_qio_p4
 * #define irp$q_qio_p5 irp$r_fill_18.irp$q_qio_p5
 * #define irp$l_qio_p5 irp$r_fill_18.irp$r_fill_19.irp$l_qio_p5
 * #define irp$q_qio_p6 irp$r_fill_20.irp$q_qio_p6
 * #define irp$l_qio_p6 irp$r_fill_20.irp$r_fill_21.irp$l_qio_p6
 * #define irp$b_shd_pio_cnt irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$b_shd_pio_cnt
 * #define irp$b_shd_pio_act irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$b_shd_pio_act
 * #define irp$b_shd_pio_flags irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$b_shd_pio_flags
 * #define irp$v_pio_error irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_error
 * #define irp$v_pio_fanout irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_fanout
 * #define irp$v_pio_noque irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_noque
 * #define irp$v_pio_cancel irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_cancel
 * #define irp$v_pio_cthrdok irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_cthrdok
 * #define irp$v_pio_phaseii irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_pio_flags_overlay.irp$r_pio_bits.irp$v_pio_phaseii
 * #define irp$b_shd_pio_errcnt irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$b_shd_pio_errcnt
 * #define irp$b_shd_pio_errindex irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$b_shd_pio_errindex
 * #define irp$b_shd_pio_errsev irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$b_shd_pio_errsev
 * #define irp$q_shd_lock_fr0 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_lock_fr0
 * #define irp$q_shd_lock_fr1 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_lock_fr1
 * #define irp$q_shd_lock_fr2 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_lock_fr2
 * #define irp$q_shd_lock_fr4 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_lock_fr4
 * #define irp$q_shd_lock_fr5 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_lock_fr5
 * #define irp$l_shd_lock_fpc irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_lock_fpc
 * #define irp$l_shd_pio_error irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_pio_error
 * #define irp$l_shd_pio_lnk irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_pio_lnk
 * #define irp$l_shdspc irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shdspc
 * #define irp$l_shd_control_irp irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_control_irp
 * #define irp$l_shd_temp irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_temp
 * #define irp$q_shd_saved_r1 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_saved_r1
 * #define irp$q_shd_saved_r2 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_saved_r2
 * #define irp$q_shd_saved_r4 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_saved_r4
 * #define irp$l_shd_svd_cnt_irp irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_svd_cnt_irp
 * #define irp$l_shd_saved_status irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_saved_status
 * #define irp$l_shd_wlg_mode_fpc irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_wlg_mode_fpc
 * #define irp$l_shd_perlkid irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_perlkid
 * #define irp$l_shd_expel_timer irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_expel_timer
 * #define irp$l_shd_expel_flags irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_expel_flags_overlay.irp$l_shd_expel_flags
 * #define irp$v_shd_expel_removed irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_expel_flags_overlay.irp$r_expel_bits.irp$v_sh\
 * d_expel_removed
 * #define irp$l_shd_expel_mask irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_shd_expel_mask
 * #define irp$q_shd_reserv_q8 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_reserv_q8
 * #define irp$q_shd_reserv_q9 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_reserv_q9
 * #define irp$q_shd_reserv_q10 irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$q_shd_reserv_q10
 * #define irp$b_shd_flags irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_shd_flags_overlay.irp$b_shd_flags
 * #define irp$v_cln_ready irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_shd_flags_overlay.irp$r_shd_flag_bits.irp$v_cln_ready
 * #define irp$v_cln_done irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_shd_flags_overlay.irp$r_shd_flag_bits.irp$v_cln_done
 * #define irp$v_cpy_fini irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$r_shd_flags_overlay.irp$r_shd_flag_bits.irp$v_cpy_fini
 * #define irp$l_curcsp irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_curcspx
 * #define irp$v_onstack (irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_stkflgsx & 1)
 * #define irp$a_ctxstk irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$a_ctxstkx[0]
 * #define irp$l_stkflgs irp$r_cdrp_extensions.irp$r_shadowing_extension.irp$l_stkflgsx
 * #define irp$l_cdrpfl irp$r_cdrp_extensions.irp$r_blk_xfer_extension.irp$r_rbufh_ad_overlay.irp$l_cdrpfl
 * #define irp$l_rboff irp$r_cdrp_extensions.irp$r_blk_xfer_extension.irp$l_rboff
 * #define irp$l_xct_len irp$r_cdrp_extensions.irp$r_blk_xfer_extension.irp$l_xct_len
 * #define irp$t_lbufhndl irp$r_cdrp_extensions.irp$r_cls_drv_extension.irp$t_lbufhndl
 * #define irp$l_ubarsrce irp$r_cdrp_extensions.irp$r_cls_drv_extension.irp$l_ubarsrce
 * #define irp$l_dutuflags irp$r_cdrp_extensions.irp$r_cls_drv_extension.irp$l_dutuflags
 * #define irp$w_dutucntr irp$r_cdrp_extensions.irp$r_cls_drv_extension.irp$w_dutucntr
 * #define irp$w_endmsgsiz irp$r_cdrp_extensions.irp$r_cls_drv_extension.irp$w_endmsgsiz
 * #define irp$l_erase_vbn irp$r_cdrp_extensions.irp$l_erase_vbn
 *
 * #define IRP$K_LENGTH 744                \* LENGTH OF STANDARD IRP           *\
 * #define IRP$C_LENGTH 744                \* LENGTH OF STANDARD IRP           *\
 * #define IRP$S_IRPDEF 744                \* OLD IRP SIZE FOR COMPATIBILITY   *\
 *
 */

#endif /* _IRPDEF_H */
