/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _CCBDEF_H
#define _CCBDEF_H

#include <stdint.h>

typedef uint16_t channel_t;

/* Channel Control Block */
typedef struct _ccb
{
    struct _ucb *ccb$ps_ucb;            /* address of assigned device UCB */
    uint8_t      ccb$b_amod;            /* access mode that assigned channel */
    channel_t    ccb$w_chan;            /* associated channel number */
} CCB;

/*
 * Original definition:
 *
 * typedef struct _ccb {
 *     struct _ucb *ccb$l_ucb;             \*ADDRESS OF ASSIGNED DEVICE UCB    *\
 *     __union  {
 *         unsigned int ccb$l_sts;         \*CHANNEL STATUS                    *\
 *         __struct  {
 *             unsigned ccb$v_amb : 1;     \* MAILBOX ASSOCIATED WITH CHANNEL  *\
 *             unsigned ccb$v_imgtmp : 1;  \* IMAGE TEMPORARY                  *\
 *             unsigned ccb$v_rdchkdon : 1; \* READ PROTECTION CHECK COMPLETED *\
 *             unsigned ccb$v_wrtchkdon : 1; \* WRITE PROTECTION CHECK COMPLETED *\
 *             unsigned ccb$v_logchkdon : 1; \* LOGICAL I/O ACCESS CHECK DONE  *\
 *             unsigned ccb$v_phychkdon : 1; \* PHYSICAL I/O ACCESS CHECK DONE *\
 *             unsigned ccb$v_noreadacc : 1; \* READ ACCESS TO DEVICE DISABLED *\
 *             unsigned ccb$v_nowriteacc : 1; \* WRITE ACCESS TO DEVICE DISABLED *\
 *             unsigned ccb$v_clone : 1;   \* Clone channel after Posix fork() *\
 *             unsigned ccb$v_fill_0_ : 7;
 *             } ccb$r_sts_bits;
 *         } ccb$r_sts_overlay;
 *     unsigned int ccb$l_ioc;             \* Number of outstanding I/O requests on channel  *\
 *     struct _irp *ccb$l_dirp;            \*DEACCESS I/O REQUEST PACKET ADDRESS  *\
 *     char ccb$b_amod;                    \*ACCESS MODE THAT ASSIGNED CHANNEL  *\
 *     unsigned char ccb$b_spare_1;
 *     unsigned short int ccb$w_spare_2;
 *     struct _wcb *ccb$l_wind;            \* Address of Window Control Block  *\
 *     __union  {
 *         int ccb$l_chan;                 \* associated channel number        *\
 *         unsigned short int ccb$w_chan;
 *         } ccb$r_chan_overlay;
 *     int ccb$l_reserved;
 *     } CCB;
 *
 * #define ccb$l_sts ccb$r_sts_overlay.ccb$l_sts
 * #define ccb$v_amb ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_amb
 * #define ccb$v_imgtmp ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_imgtmp
 * #define ccb$v_rdchkdon ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_rdchkdon
 * #define ccb$v_wrtchkdon ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_wrtchkdon
 * #define ccb$v_logchkdon ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_logchkdon
 * #define ccb$v_phychkdon ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_phychkdon
 * #define ccb$v_noreadacc ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_noreadacc
 * #define ccb$v_nowriteacc ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_nowriteacc
 * #define ccb$v_clone ccb$r_sts_overlay.ccb$r_sts_bits.ccb$v_clone
 * #define ccb$l_chan ccb$r_chan_overlay.ccb$l_chan
 * #define ccb$w_chan ccb$r_chan_overlay.ccb$w_chan
 *
 * #define CCB$K_LENGTH 32                 \*LENGTH OF CCB                     *\
 * #define CCB$S_CCBDEF 32                 \*OLD SIZE NAME, SYNONYM FOR CCB$S_CCB *\
 */

#endif /* _CCBDEF_H */
