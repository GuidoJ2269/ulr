/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _UCBDEF_H
#define _UCBDEF_H

#include <crbdef.h>
#include <dcdef.h>
#include <devdef.h>

/* Unit Control Block */
typedef struct _ucb
{
    struct _crb   *ucb$ps_crb;
    struct _ddb   *ucb$ps_ddb;
    union _devchar ucb$q_devchar;
    enum _devclass ucb$b_devclass;
} UCB;

/*
 * Original definition:
 *
 * struct UCB {
 *  union {
 *      unsigned long   UCB_un2;
 * #define  ucb$l_rqfl UCB_un1.UCB_un2
 *      unsigned short  UCB_un3;
 * #define  ucb$w_mb_seed UCB_un1.UCB_un3
 *      unsigned short  UCB_un4;
 * #define  ucb$w_unit_seed UCB_un1.UCB_un4
 *      struct FKB *    UCB_un5;
 * #define  ucb$l_fqfl UCB_un1.UCB_un5
 *  } UCB_un1;
 *  union {
 *      unsigned long   UCB_un7;
 * #define  ucb$l_rqbl UCB_un6.UCB_un7
 *      struct FKB *    UCB_un8;
 * #define  ucb$l_fqbl UCB_un6.UCB_un8
 *  } UCB_un6;
 *  unsigned short  ucb$w_size;
 *  unsigned char   ucb$b_type;
 *  unsigned char   ucb$b_fipl;
 *  union {
 *      unsigned char   UCB_un10[1];
 * #define  ucb$t_partner UCB_un9.UCB_un10
 *      unsigned long   UCB_un11;
 * #define  ucb$l_astqfl UCB_un9.UCB_un11
 *      unsigned long   UCB_un12;
 * #define  ucb$l_fpc UCB_un9.UCB_un12
 *  } UCB_un9;
 *  union {
 *      unsigned long   UCB_un14;
 * #define  ucb$l_astqbl UCB_un13.UCB_un14
 *      unsigned long   UCB_un15;
 * #define  ucb$l_fr3 UCB_un13.UCB_un15
 *  } UCB_un13;
 *  union {
 *      struct {
 *          char UCB_un17[2];
 *          unsigned short  UCB_un18;
 * #define  ucb$w_msgcnt UCB_un16.UCB_un19.UCB_un18
 *      } UCB_un19;
 *      unsigned long   UCB_un20;
 * #define  ucb$l_first UCB_un16.UCB_un20
 *      unsigned short  UCB_un21;
 * #define  ucb$w_msgmax UCB_un16.UCB_un21
 *      unsigned long   UCB_un22;
 * #define  ucb$l_fr4 UCB_un16.UCB_un22
 *  } UCB_un16;
 *  union {
 *      unsigned short  UCB_un24;
 * #define  ucb$w_dstaddr UCB_un23.UCB_un24
 *      unsigned short  UCB_un25;
 * #define  ucb$w_bufquo UCB_un23.UCB_un25
 *  } UCB_un23;
 *  unsigned short  ucb$w_srcaddr;
 *  unsigned long   ucb$l_orb;
 *  union {
 *      unsigned long   UCB_un27;
 * #define  ucb$l_cpid UCB_un26.UCB_un27
 *      unsigned long   UCB_un28;
 * #define  ucb$l_lockid UCB_un26.UCB_un28
 *  } UCB_un26;
 *  struct CRB *    ucb$l_crb;
 *  struct DDB *    ucb$l_ddb;
 *  unsigned long   ucb$l_pid;
 *  struct UCB *    ucb$l_link;
 *  struct VCB *    ucb$l_vcb;
 *  union {
 *      struct {
 *          char UCB_un30[4];
 *          unsigned long   UCB_un31;
 * #define  ucb$l_devchar2 UCB_un29.UCB_un32.UCB_un31
 *      } UCB_un32;
 *      unsigned long   UCB_un33;
 * #define  ucb$l_devchar UCB_un29.UCB_un33
 *      unsigned long   UCB_un34[2];
 * #define  ucb$q_devchar UCB_un29.UCB_un34
 *  } UCB_un29;
 *  unsigned char   ucb$b_devclass;
 *  unsigned char   ucb$b_devtype;
 *  unsigned short  ucb$w_devbufsiz;
 *  union {
 *      struct {
 *          char UCB_un36[4];
 *          unsigned long   UCB_un37;
 * #define  ucb$l_devdepnd2 UCB_un35.UCB_un38.UCB_un37
 *      } UCB_un38;
 *      struct {
 *          char UCB_un39[4];
 *          unsigned long   UCB_un40;
 * #define  ucb$l_tt_devdp1 UCB_un35.UCB_un41.UCB_un40
 *      } UCB_un41;
 *      struct {
 *          char UCB_un42[3];
 *          unsigned char   UCB_un43;
 * #define  ucb$b_vertsz UCB_un35.UCB_un44.UCB_un43
 *      } UCB_un44;
 *      struct {
 *          char UCB_un45[2];
 *          unsigned short  UCB_un46;
 * #define  ucb$w_cylinders UCB_un35.UCB_un47.UCB_un46
 *      } UCB_un47;
 *      struct {
 *          char UCB_un48[2];
 *          unsigned short  UCB_un49;
 * #define  ucb$w_bytestogo UCB_un35.UCB_un50.UCB_un49
 *      } UCB_un50;
 *      struct {
 *          char UCB_un51[1];
 *          unsigned char   UCB_un52;
 * #define  ucb$b_tracks UCB_un35.UCB_un53.UCB_un52
 *      } UCB_un53;
 *      struct {
 *          char UCB_un54[1];
 *          unsigned char   UCB_un55;
 * #define  ucb$b_remsrv UCB_un35.UCB_un56.UCB_un55
 *      } UCB_un56;
 *      unsigned long   UCB_un57;
 * #define  ucb$l_jnl_seqno UCB_un35.UCB_un57
 *      unsigned char   UCB_un58;
 * #define  ucb$b_locsrv UCB_un35.UCB_un58
 *      unsigned char   UCB_un59[1];
 * #define  ucb$r_net_devdepend UCB_un35.UCB_un59
 *      unsigned char   UCB_un60[1];
 * #define  ucb$r_term_devdepend UCB_un35.UCB_un60
 *      unsigned char   UCB_un61;
 * #define  ucb$b_sectors UCB_un35.UCB_un61
 *      unsigned char   UCB_un62[1];
 * #define  ucb$r_disk_devdepend UCB_un35.UCB_un62
 *      unsigned long   UCB_un63;
 * #define  ucb$l_devdepend UCB_un35.UCB_un63
 *      unsigned char   UCB_un64[1];
 * #define  ucb$r_devdepend_q_block UCB_un35.UCB_un64
 *      unsigned long   UCB_un65[2];
 * #define  ucb$q_devdepend UCB_un35.UCB_un65
 *  } UCB_un35;
 *  struct IRP *    ucb$l_ioqfl;
 *  struct IRP *    ucb$l_ioqbl;
 *  unsigned short  ucb$w_unit;
 *  union {
 *      struct {
 *          char UCB_un67[1];
 *          unsigned char   UCB_un68;
 * #define  ucb$b_cm2 UCB_un66.UCB_un69.UCB_un68
 *      } UCB_un69;
 *      unsigned char   UCB_un70;
 * #define  ucb$b_cm1 UCB_un66.UCB_un70
 *      unsigned short  UCB_un71;
 * #define  ucb$w_rwaitcnt UCB_un66.UCB_un71
 *      unsigned short  UCB_un72;
 * #define  ucb$w_charge UCB_un66.UCB_un72
 *  } UCB_un66;
 *  unsigned long   ucb$l_irp;
 *  unsigned short  ucb$w_refc;
 *  union {
 *      unsigned char   UCB_un74;
 * #define  ucb$b_state UCB_un73.UCB_un74
 *      unsigned char   UCB_un75;
 * #define  ucb$b_dipl UCB_un73.UCB_un75
 *  } UCB_un73;
 *  unsigned char   ucb$b_amod;
 *  struct UCB *    ucb$l_amb;
 *  union {
 *      unsigned short  UCB_un77;
 * #define  ucb$w_sts UCB_un76.UCB_un77
 *      unsigned long   UCB_un78;
 * #define  ucb$l_sts UCB_un76.UCB_un78
 *  } UCB_un76;
 *  unsigned short  ucb$w_devsts;
 *  unsigned short  ucb$w_qlen;
 *  unsigned long   ucb$l_duetim;
 *  unsigned long   ucb$l_opcnt;
 *  union {
 *      unsigned long   UCB_un80;
 * #define  ucb$l_logadr UCB_un79.UCB_un80
 *      char *  UCB_un81;
 * #define  ucb$l_svpn UCB_un79.UCB_un81
 *  } UCB_un79;
 *  char *  ucb$l_svapte;
 *  unsigned short  ucb$w_boff;
 *  unsigned short  ucb$w_bcnt;
 *  unsigned char   ucb$b_ertcnt;
 *  unsigned char   ucb$b_ertmax;
 *  unsigned short  ucb$w_errcnt;
 *  union {
 *      unsigned long   UCB_un83;
 * #define  ucb$l_jnl_mcsid UCB_un82.UCB_un83
 *      unsigned long   UCB_un84;
 * #define  ucb$l_pdt UCB_un82.UCB_un84
 *  } UCB_un82;
 *  struct DDT *    ucb$l_ddt;
 *  unsigned long   ucb$l_media_id;
 *  union {
 *      struct {
 *          char UCB_un86[3];
 *          unsigned char   UCB_un87;
 * #define  ucb$b_cex UCB_un85.UCB_un88.UCB_un87
 *      } UCB_un88;
 *      struct {
 *          char UCB_un89[2];
 *          unsigned char   UCB_un90;
 * #define  ucb$b_fex UCB_un85.UCB_un91.UCB_un90
 *      } UCB_un91;
 *      struct {
 *          char UCB_un92[1];
 *          unsigned char   UCB_un93;
 * #define  ucb$b_spr UCB_un85.UCB_un94.UCB_un93
 *      } UCB_un94;
 *      unsigned long   UCB_un95;
 * #define  ucb$l_ni_hwaptr UCB_un85.UCB_un95
 *      unsigned long   UCB_un96;
 * #define  ucb$l_nt_datssb UCB_un85.UCB_un96
 *      unsigned char   UCB_un97;
 * #define  ucb$b_slave UCB_un85.UCB_un97
 *      unsigned long   UCB_un98;
 * #define  ucb$l_mb_wast UCB_un85.UCB_un98
 *  } UCB_un85;
 *  union {
 *      unsigned long   UCB_un100;
 * #define  ucb$l_ni_mltptr UCB_un99.UCB_un100
 *      unsigned long   UCB_un101;
 * #define  ucb$l_nt_intssb UCB_un99.UCB_un101
 *      unsigned long   UCB_un102;
 * #define  ucb$l_emb UCB_un99.UCB_un102
 *      unsigned long   UCB_un103;
 * #define  ucb$l_mb_rast UCB_un99.UCB_un103
 *  } UCB_un99;
 *  union {
 *      struct {
 *          char UCB_un105[2];
 *          unsigned short  UCB_un106;
 * #define  ucb$w_func UCB_un104.UCB_un107.UCB_un106
 *      } UCB_un107;
 *      unsigned short  UCB_un108;
 * #define  ucb$w_nt_chan UCB_un104.UCB_un108
 *      unsigned long   UCB_un109;
 * #define  ucb$l_mb_mbx UCB_un104.UCB_un109
 *  } UCB_un104;
 *  union {
 *      unsigned long   UCB_un111;
 * #define  ucb$l_dpc UCB_un110.UCB_un111
 *      unsigned long   UCB_un112;
 * #define  ucb$l_mb_shb UCB_un110.UCB_un112
 *  } UCB_un110;
 *  union {
 *      unsigned long   UCB_un114;
 * #define  ucb$l_jnl_adl UCB_un113.UCB_un114
 *      unsigned long   UCB_un115;
 * #define  ucb$l_jnl_bcb UCB_un113.UCB_un115
 *      unsigned long   UCB_un116;
 * #define  ucb$l_2p_ddb UCB_un113.UCB_un116
 *      unsigned long   UCB_un117;
 * #define  ucb$l_dp_ddb UCB_un113.UCB_un117
 *      unsigned long   UCB_un118;
 * #define  ucb$l_mb_wioqfl UCB_un113.UCB_un118
 *  } UCB_un113;
 *  union {
 *      unsigned long   UCB_un120;
 * #define  ucb$l_jnl_rul UCB_un119.UCB_un120
 *      unsigned long   UCB_un121;
 * #define  ucb$l_2p_link UCB_un119.UCB_un121
 *      unsigned long   UCB_un122;
 * #define  ucb$l_dp_link UCB_un119.UCB_un122
 *      unsigned long   UCB_un123;
 * #define  ucb$l_mb_wioqbl UCB_un119.UCB_un123
 *  } UCB_un119;
 *  union {
 *      unsigned long   UCB_un125;
 * #define  ucb$l_jnl_wqfl UCB_un124.UCB_un125
 *      unsigned long   UCB_un126;
 * #define  ucb$l_2p_altucb UCB_un124.UCB_un126
 *      unsigned long   UCB_un127;
 * #define  ucb$l_dp_altucb UCB_un124.UCB_un127
 *      unsigned long   UCB_un128;
 * #define  ucb$l_mb_port UCB_un124.UCB_un128
 *  } UCB_un124;
 *  union {
 *      struct {
 *          char UCB_un130[2];
 *          unsigned char   UCB_un131;
 * #define  ucb$b_onlcnt UCB_un129.UCB_un132.UCB_un131
 *      } UCB_un132;
 *      unsigned long   UCB_un133;
 * #define  ucb$l_jnl_wqbl UCB_un129.UCB_un133
 *      unsigned short  UCB_un134;
 * #define  ucb$w_dirseq UCB_un129.UCB_un134
 *  } UCB_un129;
 *  union {
 *      unsigned long   UCB_un136;
 * #define  ucb$l_jnl_fqfl UCB_un135.UCB_un136
 *      unsigned long   UCB_un137;
 * #define  ucb$l_record UCB_un135.UCB_un137
 *      unsigned long   UCB_un138;
 * #define  ucb$l_maxblock UCB_un135.UCB_un138
 *  } UCB_un135;
 *  union {
 *      unsigned long   UCB_un140;
 * #define  ucb$l_jnl_fqbl UCB_un139.UCB_un140
 *      unsigned long   UCB_un141;
 * #define  ucb$l_maxbcnt UCB_un139.UCB_un141
 *  } UCB_un139;
 *  union {
 *      struct {
 *          char UCB_un143[1];
 *          unsigned char   UCB_un144;
 * #define  ucb$b_jnl_nam UCB_un142.UCB_un145.UCB_un144
 *      } UCB_un145;
 *      unsigned char   UCB_un146[1];
 * #define  ucb$t_jnl_nam UCB_un142.UCB_un146
 *      unsigned long   UCB_un147;
 * #define  ucb$l_dccb UCB_un142.UCB_un147
 *  } UCB_un142;
 *  union {
 *      struct {
 *          char UCB_un149[2];
 *          unsigned short  UCB_un150;
 * #define  ucb$w_dc UCB_un148.UCB_un151.UCB_un150
 *      } UCB_un151;
 *      unsigned long   UCB_un152;
 * #define  ucb$l_cddb UCB_un148.UCB_un152
 *      unsigned short  UCB_un153;
 * #define  ucb$w_da UCB_un148.UCB_un153
 *      unsigned long   UCB_un154;
 * #define  ucb$l_media UCB_un148.UCB_un154
 *  } UCB_un148;
 *  union {
 *      unsigned long   UCB_un156;
 * #define  ucb$l_2p_cddb UCB_un155.UCB_un156
 *      unsigned short  UCB_un157;
 * #define  ucb$w_bcr UCB_un155.UCB_un157
 *      unsigned long   UCB_un158;
 * #define  ucb$l_bcr UCB_un155.UCB_un158
 *  } UCB_un155;
 *  union {
 *      struct {
 *          char UCB_un160[2];
 *          unsigned short  UCB_un161;
 * #define  ucb$w_ec2 UCB_un159.UCB_un162.UCB_un161
 *      } UCB_un162;
 *      unsigned long   UCB_un163;
 * #define  ucb$l_cddb_link UCB_un159.UCB_un163
 *      unsigned short  UCB_un164;
 * #define  ucb$w_ec1 UCB_un159.UCB_un164
 *  } UCB_un159;
 *  union {
 *      struct {
 *          char UCB_un166[3];
 *          unsigned char   UCB_un167;
 * #define  ucb$b_offrtc UCB_un165.UCB_un168.UCB_un167
 *      } UCB_un168;
 *      struct {
 *          char UCB_un169[2];
 *          unsigned char   UCB_un170;
 * #define  ucb$b_offndx UCB_un165.UCB_un171.UCB_un170
 *      } UCB_un171;
 *      unsigned long   UCB_un172;
 * #define  ucb$l_cdt UCB_un165.UCB_un172
 *      unsigned short  UCB_un173;
 * #define  ucb$w_offset UCB_un165.UCB_un173
 *  } UCB_un165;
 *  union {
 *      struct {
 *          char UCB_un175[6];
 *          unsigned short  UCB_un176;
 * #define  ucb$w_jnl_munit UCB_un174.UCB_un177.UCB_un176
 *      } UCB_un177;
 *      struct {
 *          char UCB_un178[4];
 *          unsigned long   UCB_un179;
 * #define  ucb$l_dx_bfpnt UCB_un174.UCB_un180.UCB_un179
 *      } UCB_un180;
 *      struct {
 *          char UCB_un181[4];
 *          unsigned short  UCB_un182;
 * #define  ucb$w_jnl_id UCB_un174.UCB_un183.UCB_un182
 *      } UCB_un183;
 *      unsigned long   UCB_un184;
 * #define  ucb$l_jnl_quot UCB_un174.UCB_un184
 *      unsigned long   UCB_un185[2];
 * #define  ucb$q_unit_id UCB_un174.UCB_un185
 *      unsigned long   UCB_un186;
 * #define  ucb$l_dx_buf UCB_un174.UCB_un186
 *  } UCB_un174;
 *  union {
 *      struct {
 *          char UCB_un188[2];
 *          unsigned short  UCB_un189;
 * #define  ucb$w_2p_mscpunit UCB_un187.UCB_un190.UCB_un189
 *      } UCB_un190;
 *      unsigned long   UCB_un191;
 * #define  ucb$l_jnl_mask UCB_un187.UCB_un191
 *      unsigned short  UCB_un192;
 * #define  ucb$w_mscpunit UCB_un187.UCB_un192
 *      unsigned long   UCB_un193;
 * #define  ucb$l_dx_rxdb UCB_un187.UCB_un193
 *  } UCB_un187;
 *  union {
 *      struct {
 *          char UCB_un195[2];
 *          unsigned char   UCB_un196;
 * #define  ucb$b_dx_sctcnt UCB_un194.UCB_un197.UCB_un196
 *      } UCB_un197;
 *      unsigned long   UCB_un198;
 * #define  ucb$l_jnl_ndl UCB_un194.UCB_un198
 *      unsigned long   UCB_un199;
 * #define  ucb$l_jnl_asid UCB_un194.UCB_un199
 *      unsigned long   UCB_un200;
 * #define  ucb$l_mscpdevparam UCB_un194.UCB_un200
 *      unsigned short  UCB_un201;
 * #define  ucb$w_dx_bcr UCB_un194.UCB_un201
 *  } UCB_un194;
 *  union {
 *      unsigned long   UCB_un203;
 * #define  ucb$l_jnl_refc UCB_un202.UCB_un203
 *      unsigned long   UCB_un204;
 * #define  ucb$l_wait_cddb UCB_un202.UCB_un204
 *  } UCB_un202;
 *  union {
 *      unsigned long   UCB_un206;
 * #define  ucb$l_jnl_trefc UCB_un205.UCB_un206
 *      unsigned short  UCB_un207;
 * #define  ucb$w_unit_flags UCB_un205.UCB_un207
 *  } UCB_un205;
 *  union {
 *      struct {
 *          char UCB_un209[4];
 *          unsigned long   UCB_un210;
 * #define  ucb$l_jnl_wrcnt UCB_un208.UCB_un211.UCB_un210
 *      } UCB_un211;
 *      struct {
 *          char UCB_un212[2];
 *          unsigned short  UCB_un213;
 * #define  ucb$w_jnl_prot UCB_un208.UCB_un214.UCB_un213
 *      } UCB_un214;
 *      unsigned short  UCB_un215;
 * #define  ucb$w_jnl_mxent UCB_un208.UCB_un215
 *      unsigned long   UCB_un216[2];
 * #define  ucb$q_mscp_resv UCB_un208.UCB_un216
 *  } UCB_un208;
 *  unsigned long   ucb$l_jnl_bwcnt;
 *  unsigned long   ucb$l_jnl_excnt;
 *  unsigned long   ucb$l_jnl_failqfl;
 *  unsigned long   ucb$l_jnl_failqbl;
 *  union {
 *      unsigned long   UCB_un218;
 * #define  ucb$l_jnl_btxseqno UCB_un217.UCB_un218
 *      unsigned long   UCB_un219;
 * #define  ucb$l_jnl_lseqno UCB_un217.UCB_un219
 *  } UCB_un217;
 *  unsigned long   ucb$l_jnl_acbm;
 *  unsigned long   ucb$l_jnl_rmblk;
 *  unsigned long   ucb$l_jnl_cwqfl;
 *  unsigned long   ucb$l_jnl_cwqbl;
 *  unsigned long   ucb$l_jnl_wcbfl;
 *  unsigned long   ucb$l_jnl_wcbbl;
 *  };
 *
 * #define  UCB$M_TIM   1
 * #define  UCB$M_INT   2
 * #define  UCB$M_ERLOGIP   4
 * #define  UCB$M_CANCEL    8
 * #define  UCB$M_ONLINE    16
 * #define  UCB$M_POWER 32
 * #define  UCB$M_TIMOUT    64
 * #define  UCB$M_INTTYPE   128
 * #define  UCB$M_BSY   256
 * #define  UCB$M_MOUNTING  512
 * #define  UCB$M_DEADMO    1024
 * #define  UCB$M_VALID 2048
 * #define  UCB$M_UNLOAD    4096
 * #define  UCB$M_TEMPLATE  8192
 * #define  UCB$M_MNTVERIP  16384
 * #define  UCB$M_WRONGVOL  32768
 * #define  UCB$M_DELETEUCB 65536
 * #define  UCB$M_LCL_VALID 131072
 * #define  UCB$M_SUPMVMSG  262144
 * #define  UCB$M_MNTVERPND 524288
 * #define  UCB$M_DISMOUNT  1048576
 * #define  UCB$M_CLUTRAN   2097152
 * #define  UCB$M_JOB   1
 * #define  UCB$M_TEMPL_BSY 64
 * #define  UCB$M_PRMMBX    1
 * #define  UCB$M_DELMBX    2
 * #define  UCB$M_SHMMBX    8
 * #define  UCB$M_TT_TIMO   2
 * #define  UCB$M_TT_NOTIF  4
 * #define  UCB$M_TT_HANGUP 8
 * #define  UCB$M_TT_NOLOGINS   32768
 * #define  UCB$M_NT_BFROVF 4
 * #define  UCB$M_NT_NAME   16
 * #define  UCB$M_NT_BREAK  32
 * #define  UCB$M_ECC   1
 * #define  UCB$M_DIAGBUF   2
 * #define  UCB$M_NOCNVRT   4
 * #define  UCB$M_DX_WRITE  8
 * #define  UCB$M_DATACACHE 16
 * #define  UCB$M_MSCP_MNTVERIP 256
 * #define  UCB$M_MSCP_INITING  512
 * #define  UCB$M_MSCP_WAITBMP  1024
 * #define  UCB$M_MSCP_FLOVR    2048
 * #define  UCB$M_MSCP_PKACK    4096
 * #define  UCB$M_MSCP_WRTP 8192
 * #define  UCB$M_TU_OVRSQCHK   1
 * #define  UCB$M_TU_TRACEACT   2
 * #define  UCB$M_TU_SEQNOP 4
 * #define  UCB$M_PERM_JNL  16
 * #define  UCB$M_KNOWN_JNL 32
 * #define  UCB$M_JNL_CLS   64
 * #define  UCB$M_JNL_SLV   128
 * #define  UCB$M_CDELE_PND 256
 * #define  UCB$M_JNL_UNMAST    512
 * #define  UCB$K_LENGTH    144
 * #define  UCB$C_LENGTH    144
 * #define  UCB$K_MB_LENGTH 172
 * #define  UCB$C_MB_LENGTH 172
 * #define  UCB$K_ERL_LENGTH    160
 * #define  UCB$C_ERL_LENGTH    160
 * #define  UCB$K_DP_LENGTH 172
 * #define  UCB$C_DP_LENGTH 172
 * #define  UCB$K_2P_LENGTH 172
 * #define  UCB$C_2P_LENGTH 172
 * #define  UCB$M_AST_ARMED 32768
 * #define  UCB$K_LCL_TAPE_LENGTH   180
 * #define  UCB$C_LCL_TAPE_LENGTH   180
 * #define  UCB$K_LCL_DISK_LENGTH   204
 * #define  UCB$C_LCL_DISK_LENGTH   204
 * #define  UCB$K_MSCP_DISK_LENGTH  236
 * #define  UCB$K_MSCP_TAPE_LENGTH  236
 * #define  UCB$M_BACKP 32
 * #define  UCB$C_LOGLNK    1
 * #define  UCB$C_JNL_LENGTH    284
 * #define  UCB$K_NI_LENGTH 152
 * #define  UCB$C_NI_LENGTH 152
 */

#endif /* _UCBDEF_H */
