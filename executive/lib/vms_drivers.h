/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _VMS_DRIVERS_H
#define _VMS_DRIVERS_H

#include <driver_routines.h>

/* Prototype driver DDT, DPT, and FDT structures */
extern DDT driver$ddt;
extern DPT driver$dpt;
extern FDT driver$fdt;

/* Short hand macro's definitions for initialization routines */

/* Driver Dispatch Table initialization routines */
#define ini_ddt_altstart(ddt,func) driver$ini_ddt_altstart(ddt,func)
#define ini_ddt_aux_routine(ddt,func) driver$ini_ddt_aux_routine(ddt,func)
#define ini_ddt_aux_storage(ddt,addr) driver$ini_ddt_aux_storage(ddt,addr)
#define ini_ddt_cancel(ddt,func) driver$ini_ddt_cancel(ddt,func)
#define ini_ddt_cancel_selective(ddt,func) driver$ini_ddt_cancel_selective(ddt,func)
#define ini_ddt_channel_assign(ddt,func) driver$ini_ddt_channel_assign(ddt,func)
#define ini_ddt_cloneducb(ddt,func) driver$ini_ddt_cloneducb(ddt,func)
#define ini_ddt_ctrlinit(ddt,func) driver$ini_ddt_ctrlinit(ddt,func)
#define ini_ddt_diagbf(ddt,value) driver$ini_ddt_diagbf(ddt,value)
#define ini_ddt_erlgbf(ddt,value) driver$ini_ddt_erlgbf(ddt,value)
#define ini_ddt_kp_reg_mask(ddt,value) driver$ini_ddt_kp_reg_mask(ddt,value)
#define ini_ddt_kp_stack_size(ddt,value) driver$ini_ddt_kp_stack_size(ddt,value)
#define ini_ddt_kp_startio(ddt,func) driver$ini_ddt_kp_startio(ddt,func)
#define ini_ddt_mntv_for(ddt,func) driver$ini_ddt_mntv_for(ddt,func)
#define ini_ddt_mntver(ddt,func) driver$ini_ddt_mntver(ddt,func)
#define ini_ddt_regdmp(ddt,func) driver$ini_ddt_regdmp(ddt,func)
#define ini_ddt_start(ddt,func) driver$ini_ddt_start(ddt,func)
#define ini_ddt_unitinit(ddt,func) driver$ini_ddt_unitinit(ddt,func)
#define ini_ddt_end(ddt) driver$ini_ddt_end(ddt)

/* Driver Prologue Table initialization routines */
#define ini_dpt_adapt(dpt,value) driver$ini_dpt_adapt(dpt,value)
#define ini_dpt_bt_order(dpt,value) driver$ini_dpt_bt_order(dpt,value)
#define ini_dpt_decode(dpt,value) driver$ini_dpt_decode(dpt,value)
#define ini_dpt_defunits(dpt,value) driver$ini_dpt_defunits(dpt,value)
#define ini_dpt_deliver(dpt,func) driver$ini_dpt_deliver(dpt,func)
#define ini_dpt_flags(dpt,value) driver$ini_dpt_flags(dpt,value)
#define ini_dpt_idb_crams(dpt,value) driver$ini_dpt_idb_crams(dpt,value)
#define ini_dpt_maxunits(dpt,value) driver$ini_dpt_maxunits(dpt,value)
#define ini_dpt_name(dpt,string_ptr) driver$ini_dpt_name(dpt,string_ptr)
#define ini_dpt_struc_init(dpt,func) driver$ini_dpt_struc_init(dpt,func)
#define ini_dpt_struc_reinit(dpt,func) driver$ini_dpt_struc_reinit(dpt,func)
#define ini_dpt_ucb_crams(dpt,value) driver$ini_dpt_ucb_crams(dpt,value)
#define ini_dpt_ucbsize(dpt,value) driver$ini_dpt_ucbsize(dpt,value)
#define ini_dpt_unload(dpt,func) driver$ini_dpt_unload(dpt,func)
#define ini_dpt_vector(dpt,func) driver$ini_dpt_vector(dpt,func)
#define ini_dpt_end(dpt) driver$ini_dpt_end(dpt)

/* Function Decision Table initialization routines */
#define ini_fdt_act(fdt,iofunc,action,bufflag) driver$ini_fdt_act(fdt,(IODEF){iofunc},action,bufflag)
#define ini_fdt_end(fdt) driver$ini_fdt_end(fdt)

#endif /* _VMS_DRIVERS_H */
