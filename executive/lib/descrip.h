/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _DESCRIP_H
#define _DESCRIP_H

#include <stdint.h>

/*
 *  General descriptor format - each class of descriptor consists of at
 *  least the following fields, sometimes under variant nams:
 */
struct dsc$descriptor {
    uint16_t  dsc$w_length;   /* specific to descriptor class;  typically a 16-bit (unsigned) length */
    uint8_t   dsc$b_dtype;    /* data type code */
    uint8_t   dsc$b_class;    /* descriptor class code */
    void     *dsc$a_pointer;  /* address of first byte of data element */
};


/*
 *  Scalar or static string descriptor:
 */
struct dsc$descriptor_s {
    uint16_t  dsc$w_length;   /* length of data item in bytes,              */
                              /*  or if dsc$b_dtype is V, in bits,              */
                              /*  or if dsc$b_dtype is P, in digits (4 bit nibbles) */
    uint8_t   dsc$b_dtype;    /* data type code */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_S */
    char     *dsc$a_pointer;  /* address of first byte of data storage */
};


/*
 *  Dynamic string descriptor:
 */
struct dsc$descriptor_d {
    uint16_t  dsc$w_length;   /* allocated length, maintained by RTL */
    uint8_t   dsc$b_dtype;    /* data type code */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_D */
    char     *dsc$a_pointer;  /* address */
};


/*
 *  Array descriptor:
 *
 * One or two optional blocks of information may follow contiguously
 * at this point; the first block contains information about the
 * dimension multipliers (if present, dsc$b_aflags.dsc$v_fl_coeff is
 * set), the second block contains information about the dimension
 * bounds (if present, dsc$b_aflags.dsc$v_fl_bounds is set).  If the
 * bounds information is present, the multipliers information must
 * also be present.
 *
 * The multipliers block has the following format:
 *  char    *dsc$a_a0;      Address of the element whose subscripts are all zero
 *  int32_t    dsc$l_m [DIMCT];    Addressing coefficients (multipliers)
 *
 * The bounds block has the following format:
 *  struct {
 *      int32_t    dsc$l_l;    Lower bound
 *      int32_t    dsc$l_u;    Upper bound
 *  } dsc$bounds [DIMCT];
 *
 * (DIMCT represents the value contained in dsc$b_dimct.)
 */
struct dsc$descriptor_a {
    uint16_t  dsc$w_length;   /* length of an array element in bytes, bits, or digits */
    uint8_t   dsc$b_dtype;    /* data type code */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_A */
    void     *dsc$a_pointer;  /* address of first actual byte of data storage */
    char      dsc$b_scale;    /* scale multiplier to convert from internal to external form */
    uint8_t   dsc$b_digits;   /* number of decimal digits in internal representation */
    struct {
        uint8_t                 : 4;  /* reserved, must be zero */
        uint8_t dsc$v_fl_redim  : 1;  /* if set, indicates the array can be redimensioned */
        uint8_t dsc$v_fl_column : 1;  /* if set, indicates column-major order (FORTRAN) */
        uint8_t dsc$v_fl_coeff  : 1;  /* if set, indicates the multipliers block is present */
        uint8_t dsc$v_fl_bounds : 1;  /* if set, indicates the bounds block is present */
    }         dsc$b_aflags;
    uint8_t   dsc$b_dimct;    /* number of dimensions */
    uint32_t  dsc$l_arsize;   /* total size of array in bytes, or if              */
                              /*  dsc$b_dtype is DSC$K_DTYPE_P, in digits (4 bit nibbles) */
};


/*
 *  Procedure descriptor:
 */
struct dsc$descriptor_p {
    uint16_t  dsc$w_length;   /* length associated with the function value */
    uint8_t   dsc$b_dtype;    /* function value data type code */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_P */
    void     *dsc$a_pointer;  /* address of function entry mask */
};


/*
 *  Decimal scalar string descriptor:
 */
struct dsc$descriptor_sd {
    uint16_t  dsc$w_length;   /* length of data item in bytes, bits, or digits */
    uint8_t   dsc$b_dtype;    /* data type code */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_SD */
    char     *dsc$a_pointer;  /* address of first byte of data storage */
    char      dsc$b_scale;    /* scale multiplier to convert from internal to external form */
    uint8_t   dsc$b_digits;   /* number of decimal digits in internal representation */
    uint16_t  dsc$w_reserved; /* reserved for future use, must be zero */
};


/*
 *  Noncontiguous array descriptor:
 *
 * Two blocks of information must follow contiguously at this point;
 * the first block contains information about the difference between
 * addresses of two adjacent elements in each dimension (the stride).
 * The second block contains information about the dimension bounds.
 *
 * The strides block has the following format:
 *  char        *dsc$a_a0;      Address of the element whose subscripts are all zero
 *  uint32_t   dsc$l_s [DIMCT];    Strides
 *
 * The bounds block has the following format:
 *  struct {
 *      int32_t    dsc$l_l;        Lower bound
 *      int32_t    dsc$l_u;        Upper bound
 *  } dsc$bounds [DIMCT];
 *
 * (DIMCT represents the value contained in dsc$b_dimct.)
 */
struct dsc$descriptor_nca {
    uint16_t  dsc$w_length;   /* length of an array element in bytes, bits, or digits */
    uint8_t   dsc$b_dtype;    /* data type code */
                              /*  dsc$b_dtype is DSC$K_DTYPE_P, in digits (4 bit nibbles) */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_NCA */
    void     *dsc$a_pointer;  /* address of first actual byte of data storage */
    char      dsc$b_scale;    /* scale multiplier to convert from internal to external form */
    uint8_t   dsc$b_digits;   /* number of decimal digits in internal representation */
    struct  {
        uint8_t                 : 4;  /* reserved for future standardization, must be zero */
        uint8_t dsc$v_fl_redim  : 1;  /* must be zero */
        uint8_t                 : 3;  /* reserved for future standardization, must be zero */
    }         dsc$b_aflags;
    uint8_t   dsc$b_dimct;    /* number of dimensions */
    uint32_t  dsc$l_arsize;   /* total size of array in bytes, or if              */
};


/*
 *  Varying string descriptor:
 *
 *  The varying string descriptor and varying string array descriptor are
 *  used with strings of the following form:
 *
 *      struct {
 *      uint16_t  CURLEN;     The current length of BODY in bytes
 *      char    BODY[MAXSTRLEN];    A fixed-length area containing the string
 *      };
 *
 *  where MAXSTRLEN is the value contained in the dsc$w_maxstrlen field in
 *  the descriptor and CURLEN is never greater than MAXSTRLEN.
 */
struct dsc$descriptor_vs {
    uint16_t  dsc$w_maxstrlen;    /* maximum length of the BODY field of varying string, in bytes */
    uint8_t   dsc$b_dtype;        /* data type code = DSC$K_DTYPE_VT */
    uint8_t   dsc$b_class;        /* class code = DSC$K_CLASS_VS */
    void     *dsc$a_pointer;      /* address of the CURLEN field of the varying string */
};


/*
 *  Varying string array descriptor:
 *
 * Two blocks of information must follow contiguously at this point;
 * the first block contains information about the difference between
 * addresses of two adjacent elements in each dimension (the stride).
 * The second block contains information about the dimension bounds.
 *
 * The strides block has the following format:
 *  char        *dsc$a_a0;      Address of element whose subscripts are all zero
 *  uint32_t   dsc$l_s [DIMCT];    Strides
 *
 * The bounds block has the following format:
 *  struct {
 *      int32_t    dsc$l_l;        Lower bound
 *      int32_t    dsc$l_u;        Upper bound
 *  } dsc$bounds [DIMCT];
 *
 * (DIMCT represents the value contained in dsc$b_dimct.)
 */
struct dsc$descriptor_vsa {
    uint16_t  dsc$w_maxstrlen;    /* maximum length of the BODY field of an array element in bytes */
    uint8_t   dsc$b_dtype;        /* data type code = DSC$K_DTYPE_VT */
    uint8_t   dsc$b_class;        /* class code = DSC$K_CLASS_VSA */
    void     *dsc$a_pointer;      /* address of first actual byte of data storage */
    char      dsc$b_scale;        /* reserved for future standardization, must be zero */
    uint8_t   dsc$b_digits;       /* reserved for future standardization, must be zero */
    struct {
        uint8_t                 : 4;  /* reserved for future standardization, must be zero */
        uint8_t dsc$v_fl_redim  : 1;  /* must be zero */
        uint8_t                 : 3;  /* reserved for future standardization, must be zero */
    }         dsc$b_aflags;
    uint8_t   dsc$b_dimct;        /* number of dimensions */
    uint32_t  dsc$l_arsize;       /* total size of array in bytes */
};


/*
 *  Unaligned bit string descriptor:
 */
struct dsc$descriptor_ubs {
    uint16_t  dsc$w_length;   /* length of data item in bits */
    uint8_t   dsc$b_dtype;    /* data type code = DSC$K_DTYPE_VU */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_UBS */
    char     *dsc$a_base;     /* address to which dsc$l_pos is relative */
    int32_t   dsc$l_pos;      /* bit position relative to dsc$a_base of first bit in string */
};


/*
 *  Unaligned bit array descriptor:
 *
 * Three blocks of information must follow contiguously at this point;
 * the first block contains information about the difference between
 * the bit addresses of two adjacent elements in each dimension (the
 * stride).  The second block contains information about the dimension
 * bounds.  The third block is the relative bit position with respect
 * to dsc$a_base of the first actual bit of the array.
 *
 * The strides block has the following format:
 *  int32_t        dsc$l_v0;       Bit offset of the element whose subscripts
 *                      are all zero, with respect to dsc$a_base
 *  uint32_t   dsc$l_s [DIMCT];    Strides
 *
 * The bounds block has the following format:
 *  struct {
 *      int32_t    dsc$l_l;        Lower bound
 *      int32_t    dsc$l_u;        Upper bound
 *  } dsc$bounds [DIMCT];
 *
 * The last block has the following format:
 *  int32_t    dsc$l_pos;
 *
 * (DIMCT represents the value contained in dsc$b_dimct.)
 */
struct dsc$descriptor_uba {
    uint16_t  dsc$w_length;   /* length of data item in bits */
    uint8_t   dsc$b_dtype;    /* data type code = DSC$K_DTYPE_VU */
    uint8_t   dsc$b_class;    /* class code = DSC$K_CLASS_UBA */
    char     *dsc$a_base;     /* address to which effective bit offset is relative */
    char      dsc$b_scale;    /* reserved for future standardization, must be zero */
    uint8_t   dsc$b_digits;   /* reserved for future standardization, must be zero */
    struct {
        uint8_t                 : 4;  /* reserved for future standardization, must be zero */
        uint8_t dsc$v_fl_redim  : 1;  /* must be zero */
        uint8_t                 : 3;  /* reserved for future standardization, must be zero */
    }         dsc$b_aflags;
    uint8_t   dsc$b_dimct;    /* number of dimensions */
    uint32_t  dsc$l_arsize;   /* total size of array in bits */
};

/*
 *  Codes for data type, dsc$b_dtype:
 */
    /*  Atomic data types: */
#define DSC$K_DTYPE_Z   0       /* unspecified */
#define DSC$K_DTYPE_BU  2       /* byte logical;  8-bit unsigned quantity */
#define DSC$K_DTYPE_WU  3       /* word logical;  16-bit unsigned quantity */
#define DSC$K_DTYPE_LU  4       /* longword logical;  32-bit unsigned quantity */
#define DSC$K_DTYPE_QU  5       /* quadword logical;  64-bit unsigned quantity */
#define DSC$K_DTYPE_OU  25      /* octaword logical;  128-bit unsigned quantity */
#define DSC$K_DTYPE_B   6       /* byte integer;  8-bit signed 2's-complement integer */
#define DSC$K_DTYPE_W   7       /* word integer;  16-bit signed 2's-complement integer */
#define DSC$K_DTYPE_L   8       /* longword integer;  32-bit signed 2's-complement integer */
#define DSC$K_DTYPE_Q   9       /* quadword integer;  64-bit signed 2's-complement integer */
#define DSC$K_DTYPE_O   26      /* octaword integer;  128-bit signed 2's-complement integer */
#define DSC$K_DTYPE_F   10      /* F_floating;  32-bit single-precision floating point */
#define DSC$K_DTYPE_D   11      /* D_floating;  64-bit double-precision floating point */
#define DSC$K_DTYPE_G   27      /* G_floating;  64-bit double-precision floating point */
#define DSC$K_DTYPE_H   28      /* H_floating;  128-bit quadruple-precision floating point */
#define DSC$K_DTYPE_FC  12      /* F_floating complex */
#define DSC$K_DTYPE_DC  13      /* D_floating complex */
#define DSC$K_DTYPE_GC  29      /* G_floating complex */
#define DSC$K_DTYPE_HC  30      /* H_floating complex */
#define DSC$K_DTYPE_CIT 31      /* COBOL Intermediate Temporary */
    /*  String data types: */
#define DSC$K_DTYPE_T   14      /* character-coded text;  a single character or a string */
#define DSC$K_DTYPE_T2  38      /* 16-bit Text (Asian workstations) */
#define DSC$K_DTYPE_VT  37      /* varying character-coded text;  16-bit count, followed by a string */
#define DSC$K_DTYPE_NU  15      /* numeric string, unsigned */
#define DSC$K_DTYPE_NL  16      /* numeric string, left separate sign */
#define DSC$K_DTYPE_NLO 17      /* numeric string, left overpunched sign */
#define DSC$K_DTYPE_NR  18      /* numeric string, right separate sign */
#define DSC$K_DTYPE_NRO 19      /* numeric string, right overpunched sign */
#define DSC$K_DTYPE_NZ  20      /* numeric string, zoned sign */
#define DSC$K_DTYPE_P   21      /* packed decimal string */
#define DSC$K_DTYPE_V   1       /* bit;  aligned bit string */
#define DSC$K_DTYPE_VU  34      /* bit unaligned;  arbitrary bit string */
    /*  Miscellaneous data types: */
#define DSC$K_DTYPE_ZI  22      /* sequence of instructions */
#define DSC$K_DTYPE_ZEM 23      /* procedure entry mask */
#define DSC$K_DTYPE_DSC 24      /* descriptor */
#define DSC$K_DTYPE_BPV 32      /* bound procedure value */
#define DSC$K_DTYPE_BLV 33      /* bound label value */
#define DSC$K_DTYPE_ADT 35      /* absolute date and time */
 /* DSC$K_DTYPE_??? 36      \* DTYPE code 36 is reserved for future definition */
    /*  Introduced with Alpha-AXP: */
#define DSC$K_DTYPE_FS  52      /* 32-bit S_float IEEE single precision */
#define DSC$K_DTYPE_FT  53      /* 64-bit T_float IEEE double precision */
#define DSC$K_DTYPE_FSC 54      /* 64-bit S_complex IEEE <S-real,S-imag> */
#define DSC$K_DTYPE_FTC 55      /* 128-bit T_complex IEEE */
#define DSC$K_DTYPE_FX  57      /* 128-bit X_float IEEE quadruple precision */
#define DSC$K_DTYPE_FXC 58      /* 256-bit X_complex IEEE */
    /*  Used by the VAX/VMS Debug Facility: */
#define DSC$K_DTYPE_CAD 178     /* Address calculation command (see $DSTDEF) */
#define DSC$K_DTYPE_ENT 179     /* Entry global definition */
#define DSC$K_DTYPE_GBL 180     /* Global symbol definition */
#define DSC$K_DTYPE_EPT 181     /* Entry point to routine. */
#define DSC$K_DTYPE_R11 182     /* Line number relative to R11 correlation table. */
#define DSC$K_DTYPE_FLD 183     /* BLISS FIELD name. */
#define DSC$K_DTYPE_PCT 184     /* PSECT information. */
#define DSC$K_DTYPE_DPC 185     /* PC correlation table for FORTRAN IV+ */
#define DSC$K_DTYPE_LBL 186     /* LITERAL or LABEL */
#define DSC$K_DTYPE_SLB 187     /* Label in non-assembly language modules */
#define DSC$K_DTYPE_MOD 188     /* Beginning of new module */
#define DSC$K_DTYPE_EOM 189     /* End of module */
#define DSC$K_DTYPE_RTN 190     /* Beginning of new routine */
#define DSC$K_DTYPE_EOR 191     /* End of routine */
    /*  Reserved data type codes: */
    /*       39-177 are reserved to DEC;            */
    /*      192-255 are reserved to CSS and customers.  */


/*
 *  Descriptor Class Codes for dsc$b_class:
 */
#define DSC$K_CLASS_S   1       /* scalar or string descriptor */
#define DSC$K_CLASS_D   2       /* dynamic string descriptor */
#define DSC$K_CLASS_V   3       /* variable buffer descriptor;  reserved for use by DIGITAL */
#define DSC$K_CLASS_A   4       /* array descriptor */
#define DSC$K_CLASS_P   5       /* procedure descriptor */
#define DSC$K_CLASS_PI  6       /* procedure incarnation descriptor;  obsolete */
#define DSC$K_CLASS_J   7       /* label descriptor;  reserved for use by the VAX-11 Debugger */
#define DSC$K_CLASS_JI  8       /* label incarnation descriptor;  obsolete */
#define DSC$K_CLASS_SD  9       /* decimal scalar string descriptor */
#define DSC$K_CLASS_NCA 10      /* noncontiguous array descriptor */
#define DSC$K_CLASS_VS  11      /* varying string descriptor */
#define DSC$K_CLASS_VSA 12      /* varying string array descriptor */
#define DSC$K_CLASS_UBS 13      /* unaligned bit string descriptor */
#define DSC$K_CLASS_UBA 14      /* unaligned bit array descriptor */
#define DSC$K_CLASS_SB  15      /* string with bounds descriptor */
#define DSC$K_CLASS_UBSB 16     /* unaligned bit string with bounds descriptor */
    /*  for internal use only by RTL and debugger */
#define DSC$K_CLASS_BFA 191     /* BASIC File Array Descriptor */
    /*  Reserved descriptor class codes: */
    /*       17-190 are reserved to DEC;            */
    /*      192-255 are reserved to CSS and customers.  */


/*
 *  A simple macro to construct a string descriptor,
 *  for compatability with VAX C's <descrip.h>:
 */
#define $DESCRIPTOR(name,string) struct dsc$descriptor_s name =\
 {sizeof(string)-1,DSC$K_DTYPE_T,DSC$K_CLASS_S,string}

#endif  /* _DESCRIP_H */
