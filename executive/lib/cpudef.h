/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _CPUDEF_H
#define _CPUDEF_H

#include <ipldef.h>

#define MAX_NUM_CPUS 32

enum _cpu_state
{
    CPU$C_INIT,
    CPU$C_RUN,
    CPU$C_BOOTED,
    CPU$C_STOPPED,
    CPU$C_TIMEOUT,
    CPU$C_REJECTED
} __attribute__((packed));

struct _cpu
{
    enum _cpu_state cpu$b_state;            /* CPU state */
    ipl_t           cpu$b_curipl;           /* current IPL */
    uint8_t         cpu$b_iplnr ;           /* number of saved IPLs */
    ipl_t           cpu$b_prev_ipl[32];     /* saved IPLs */
    uint32_t        cpu$l_phy_cpuid;        /* CPU number */
    unsigned long   cpu$l_lapic_base;       /* CPU local APIC base address */
    uint32_t        cpu$l_lapic_ticks;      /* Local APIC timer ticks per second */
    uint32_t        cpu$l_rank_vec;         /* spinlocks currently held */
    uint32_t        cpu$l_ipl_vec;          /* IPLs of currently held spinlocks */
    uint32_t        cpu$l_ipl_array[32];    /* number of spinlocks held at each IPL */
    struct _pcb    *cpu$l_curpcb;           /* current PCB */
};

#endif /* _CPUDEF_H */
