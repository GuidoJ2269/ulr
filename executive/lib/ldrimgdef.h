/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _LDRIMGDEF_H
#define _LDRIMGDEF_H

#include <stdint.h>

#define MAX_IMAGE_NAME_LEN 39

typedef void (*init_routine)(void);

struct _ldrimg
{
    struct _ldrimg     *ldrimg$ps_flink;
    struct _ldrimg     *ldrimg$ps_blink;
    uint64_t            ldrimg$l_length;
    char                ldrimg$s_imgnam[MAX_IMAGE_NAME_LEN + 1];
    uint64_t            ldrimg$l_base;
    union
    {
        uint32_t        ldrimg$l_flags;
        struct
        {
            uint32_t    ldrimg$v_not_xqp: 1;
            uint32_t    ldrimg$v_delay_init: 1;
            uint32_t    ldrimg$v_no_pfn_db: 1;
            uint32_t    ldrimg$v_fix_ups_done: 1;
            uint32_t    ldrimg$v_nonpaged_fixup: 1;
            uint32_t    ldrimg$v_part_load: 1;
            uint32_t    ldrimg$v_reserved: 26;
        };
    };
    init_routine        ldrimg$l_init_rtn;
};

#endif /* _LDRIMGDEF_H */
