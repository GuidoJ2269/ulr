/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _INIRTNDEF_H
#define _INIRTNDEF_H

#include <stdint.h>

typedef struct _inirtn
{
    union
    {
        uint8_t inirtn$l_bits;
        struct
        {
            uint8_t inirtn$v_called: 1;
            uint8_t inirtn$v_no_recall: 1;
            uint8_t inirtn$v_fill: 6;
        };
    };
} INIRTN;

#endif /* _INIRTNDEF_H */
