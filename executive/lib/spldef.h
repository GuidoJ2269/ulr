/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SPLDEF_H
#define _SPLDEF_H

#include <cpudef.h>

/* spinlock types */
enum _spl_index
{
    SPL$C_QUEUEAST,
    SPL$C_FILSYS,
    SPL$C_TIMER,
    SPL$C_JIB,
    SPL$C_MMG,
    SPL$C_SCHED,
    SPL$C_MAILBOX,
    SPL$C_POOL,
    SPL$C_HWCLK,
    SPL$C_MEGA
};

#define SPNLKCNT ((unsigned int)SPL$C_MEGA + 1)

/* spinlock ranks */
enum _spl_rank
{
    SPL$C_RANK_QUEUEAST,
    SPL$C_RANK_FILSYS,
    SPL$C_RANK_TIMER,
    SPL$C_RANK_JIB,
    SPL$C_RANK_MMG,
    SPL$C_RANK_SCHED,
    SPL$C_RANK_MAILBOX,
    SPL$C_RANK_POOL,
    SPL$C_RANK_HWCLK,
    SPL$C_RANK_MEGA,
    SPL$C_RANK_DEVICE = 31
} __attribute__((packed));

/* spinlock subtype */
enum _spl_subtype
{
    SPL$C_STATICLOCK,
    SPL$C_FORKLOCK,
    SPL$C_DEVICELOCK,
} __attribute__((packed));

struct _spl
{
    volatile uint8_t    spl$b_spinlock;     /* the actual spinlock */
    ipl_t               spl$b_ipl;          /* IPL of the spinlock */
    enum _spl_rank      spl$b_rank;         /* rank of the spinlock */
    enum _spl_subtype   spl$b_subtype;      /* subtype of the spinlock */
    uint16_t            spl$w_own_cnt;      /* number of concurrent/nested spinlocks */
    uint16_t            spl$w_wait_cpus;    /* number of CPUs waiting for the spinlock */
    struct _cpu *       spl$l_own_cpu;      /* address of CPU data */
};

#endif /* _SPLDEF_H */
