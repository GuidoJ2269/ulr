/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _MMG_ROUTINES_H
#define _MMG_ROUTINES_H

#include <pfndef.h>

pfn_t mmg$alloc_pfn(struct _pfn **pfndbe);
pfn_t mmg$alloc_zero_pfn(struct _pfn **pfndbe);
void mmg$dalloc_pfn(pfn_t n, struct _pfn *pfndbe);
void mmg$dalloc_zero_pfn(pfn_t n, struct _pfn *pfndbe);

void mmg$ins_pfnh(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
void mmg$ins_pfnt(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
void mmg$rem_pfn(pfn_t n, unsigned int lst, struct _pfn *pfndbe);
pfn_t mmg$rem_pfnh(unsigned int lst, struct _pfn **pfndbe);

#endif /* _MMG_ROUTINES_H */
