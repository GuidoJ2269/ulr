/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _PCBDEF_H
#define _PCBDEF_H

#include <stdint.h>

typedef uint32_t pid_t;

/* Process Control Block */
typedef struct _pcb
{
    struct _pcb    *pcb$ps_flink;   /* queue forward link */
    struct _pcb    *pcb$ps_blink;   /* queue backward link */
    uint16_t        pcb$w_mtxcnt;   /* number of owned mutexes */
    pid_t           pcb$l_pid;      /* process id */
} PCB;

/*
 * Original definition:
 *
 * struct PCB {
 *  struct PCB *    pcb$l_sqfl;
 *  struct PCB *    pcb$l_sqbl;
 *  unsigned short  pcb$w_size;
 *  unsigned char   pcb$b_type;
 *  unsigned char   pcb$b_pri;
 *  unsigned char   pcb$b_astact;
 *  unsigned char   pcb$b_asten;
 *  unsigned short  pcb$w_mtxcnt;
 *  struct AST *    pcb$l_astqfl;
 *  struct AST *    pcb$l_astqbl;
 *  struct PCB *    pcb$l_phypcb;
 *  unsigned long   pcb$l_owner;
 *  unsigned long   pcb$l_wsswp;
 *  unsigned long   pcb$l_sts;
 *  union {
 *      struct {
 *          char PCB_un2[3];
 *          unsigned char   PCB_un3;
 * #define  pcb$b_authpri PCB_un1.PCB_un4.PCB_un3
 *      } PCB_un4;
 *      struct {
 *          char PCB_un5[2];
 *          unsigned char   PCB_un6;
 * #define  pcb$b_dpc PCB_un1.PCB_un7.PCB_un6
 *      } PCB_un7;
 *      struct {
 *          char PCB_un8[1];
 *          unsigned char   PCB_un9;
 * #define  pcb$b_pribsav PCB_un1.PCB_un10.PCB_un9
 *      } PCB_un10;
 *      unsigned char   PCB_un11;
 * #define  pcb$b_prisav PCB_un1.PCB_un11
 *      unsigned long   PCB_un12;
 * #define  pcb$l_wtime PCB_un1.PCB_un12
 *  } PCB_un1;
 *  unsigned short  pcb$w_state;
 *  unsigned char   pcb$b_wefc;
 *  unsigned char   pcb$b_prib;
 *  unsigned short  pcb$w_aptcnt;
 *  unsigned short  pcb$w_tmbu;
 *  unsigned short  pcb$w_gpgcnt;
 *  unsigned short  pcb$w_ppgcnt;
 *  unsigned short  pcb$w_astcnt;
 *  unsigned short  pcb$w_biocnt;
 *  unsigned short  pcb$w_biolm;
 *  unsigned short  pcb$w_diocnt;
 *  unsigned short  pcb$w_diolm;
 *  unsigned short  pcb$w_prccnt;
 *  unsigned char   pcb$t_terminal[8];
 *  union {
 *      unsigned long   PCB_un14;
 * #define  pcb$l_efwm PCB_un13.PCB_un14
 *      unsigned long   PCB_un15;
 * #define  pcb$l_pqb PCB_un13.PCB_un15
 *  } PCB_un13;
 *  unsigned long   pcb$l_efcs;
 *  unsigned long   pcb$l_efcu;
 *  union {
 *      struct {
 *          char PCB_un17[2];
 *          unsigned char   PCB_un18;
 * #define  pcb$b_pgflindex PCB_un16.PCB_un19.PCB_un18
 *      } PCB_un19;
 *      unsigned long   PCB_un20;
 * #define  pcb$l_efc2p PCB_un16.PCB_un20
 *      unsigned short  PCB_un21;
 * #define  pcb$w_pgflchar PCB_un16.PCB_un21
 *  } PCB_un16;
 *  union {
 *      unsigned long   PCB_un23;
 * #define  pcb$l_efc3p PCB_un22.PCB_un23
 *      unsigned long   PCB_un24;
 * #define  pcb$l_swapsize PCB_un22.PCB_un24
 *  } PCB_un22;
 *  unsigned long   pcb$l_pid;
 *  unsigned long   pcb$l_epid;
 *  unsigned long   pcb$l_eowner;
 *  struct PHD *    pcb$l_phd;
 *  unsigned char   pcb$t_lname[16];
 *  struct JIB *    pcb$l_jib;
 *  unsigned long   pcb$q_priv[2];
 *  struct ARB *    pcb$l_arb;
 *  unsigned char PCB_un25[44];
 *  union {
 *      struct {
 *          char PCB_un27[2];
 *          unsigned short  PCB_un28;
 * #define  pcb$w_grp PCB_un26.PCB_un29.PCB_un28
 *      } PCB_un29;
 *      unsigned short  PCB_un30;
 * #define  pcb$w_mem PCB_un26.PCB_un30
 *      unsigned long   PCB_un31;
 * #define  pcb$l_uic PCB_un26.PCB_un31
 *  } PCB_un26;
 *  unsigned char PCB_un32[60];
 *  unsigned long   pcb$l_aclfl;
 *  unsigned long   pcb$l_aclbl;
 *  unsigned long   pcb$l_lockqfl;
 *  unsigned long   pcb$l_lockqbl;
 *  unsigned long   pcb$l_dlckpri;
 *  unsigned long   pcb$l_ipast;
 *  unsigned long   pcb$l_defprot;
 *  unsigned long   pcb$l_waitime;
 *  unsigned long   pcb$l_pmb;
 *  };
 *
 * #define  PCB$M_RES   1
 * #define  PCB$M_DELPEN    2
 * #define  PCB$M_FORCPEN   4
 * #define  PCB$M_INQUAN    8
 * #define  PCB$M_PSWAPM    16
 * #define  PCB$M_RESPEN    32
 * #define  PCB$M_SSFEXC    64
 * #define  PCB$M_SSFEXCE   128
 * #define  PCB$M_SSFEXCS   256
 * #define  PCB$M_SSFEXCU   512
 * #define  PCB$M_SSRWAIT   1024
 * #define  PCB$M_SUSPEN    2048
 * #define  PCB$M_WAKEPEN   4096
 * #define  PCB$M_WALL  8192
 * #define  PCB$M_BATCH 16384
 * #define  PCB$M_NOACNT    32768
 * #define  PCB$M_SWPVBN    65536
 * #define  PCB$M_ASTPEN    131072
 * #define  PCB$M_PHDRES    262144
 * #define  PCB$M_HIBER 524288
 * #define  PCB$M_LOGIN 1048576
 * #define  PCB$M_NETWRK    2097152
 * #define  PCB$M_PWRAST    4194304
 * #define  PCB$M_NODELET   8388608
 * #define  PCB$M_DISAWS    16777216
 * #define  PCB$M_INTER 33554432
 * #define  PCB$M_RECOVER   67108864
 * #define  PCB$M_SECAUDIT  134217728
 * #define  PCB$M_EPID_WILD -2147483648
 * #define  PCB$K_LENGTH    288
 * #define  PCB$C_LENGTH    288
 * #define  PCB$M_EPID_PROC 1
 * #define  PCB$M_EPID_NODE_IDX 2097152
 * #define  PCB$M_EPID_NODE_SEQ 536870912
 *
 */

#endif /* _PCBDEF_H */
