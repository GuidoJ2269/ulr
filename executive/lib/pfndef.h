/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _PFNDEF_H
#define _PFNDEF_H

#include <page.h>

typedef uint32_t pfn_t;
typedef uint32_t page_count_t;

#define PFN_INVALID (pfn_t)-1

#define PFN_MASK 0x00000000ffffffffUL
#define ADDRESS2PFN(x) (pfn_t)(((x) >> PAGE_SHIFT) & PFN_MASK)
#define PFN2ADDRESS(x) ((x) << PAGE_SHIFT)

/* location of page */
#define PFN$C_FREE              0  /* page on free page list */
#define PFN$C_MODIFIED          1  /* page on modified page list */
#define PFN$C_BAD               2  /* page on bad page list */
#define PFN$C_RELEASE_PENDING   3  /* release pending */
#define PFN$C_READ_ERROR        4  /* page read error */
#define PFN$C_WRITE_IN_PROGRESS 5  /* write in progress by modified page writer */
#define PFN$C_READ_IN_PROGRESS  6  /* read in progress by page fault handler */
#define PFN$C_ACTIVE            7  /* page is active and valid */

union _pfn_state
{
    uint8_t b_state;
    struct
    {
        uint8_t v_page_location: 3;
        uint8_t v_reserved0: 1;
        uint8_t v_delete: 1;
        uint8_t v_reserved1: 2;
        uint8_t v_modify: 1;
    };
};

/* type of page */
#define PFN$C_PROCESS_PAGE            0
#define PFN$C_SYSTEM_PAGE             1
#define PFN$C_GLOBAL_READONLY_PAGE    2
#define PFN$C_GLOBAL_READWRITE_PAGE   3
#define PFN$C_PROCESS_PAGE_TABLE_PAGE 4
#define PFN$C_GLOBAL_PAGE_TABLE_PAGE  5
#define PFN$C_RESERVED0               6
#define PFN$C_RESERVED1               7

union _pfn_type
{
    uint8_t b_type;
    struct
    {
        uint8_t v_page_type: 3;
        uint8_t v_reserved0: 1;
        uint8_t v_collided: 1;
        uint8_t v_bad: 1;
        uint8_t v_io_completion: 1;
        uint8_t v_reserved1: 1;
    };
};

struct _pfn
{
    pfn_t               pfn$id;
    struct _pfn        *pfn$ps_flink;
    struct _pfn        *pfn$ps_blink;
    union pt_entry     *pfn$l_pte;
    union _pfn_state    pfn$b_state;
    union _pfn_type     pfn$b_type;
    uint8_t             pfn$b_refcnt;
};

struct _pmap
{
    pfn_t           pmap$l_start_pfn;
    page_count_t    pmap$l_pfn_count;
};

#endif /* _PFNDEF_H */
