/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _FDTDEF_H
#define _FDTDEF_H

#include <ccbdef.h>
#include <irpdef.h>
#include <pcbdef.h>
#include <ucbdef.h>

typedef int (*fdt$action_routine_func)(IRP *, PCB *, UCB *, CCB *);

/* Function Decision Table */
typedef struct _fdt
{
    uint64_t                fdt$q_buffered;         /* 64 bit map set for buffered I/O function codes */
    fdt$action_routine_func fdt$ps_func_rtn[64];    /* Pointers to upper level FDT action routines */
} FDT;

/*
 * Original definition:
 *
 * typedef struct _fdt {
 * #pragma __nomember_alignment
 *     __int64 fdt$q_buffered;             \* 64 bit map set for buffered I/O function codes *\
 *      int (*fdt$ps_func_rtn [64])();     \* Pointers to upper level FDT routines *\
 *     __int64 fdt$q_ok64bit;              \* Corresponding bit set if function supports 64-bit $QIO P1  *\
 *     } FDT;
 * #define FDT$K_LENGTH 272                \* Length constant                  *\
 */

#endif /* _FDTDEF_H */
