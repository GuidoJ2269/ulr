/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _VMS_MACROS_H
#define _VMS_MACROS_H

#include <cpu.h>
#include <system_data_cells.h>
#include <trace.h>

#define find_cpu_data() \
    ({ \
        /* TODO: get the correct CPU with SMP */ \
        unsigned int index = 0; \
        &smp$gl_cpu_data[index]; \
    })

#define disable_interrupts(value) \
    { \
        /* value must be > 0 */ \
        struct _cpu *cpu_data = find_cpu_data(); \
        cpu_disable_interrupts(); \
        cpu_data->cpu$b_prev_ipl[cpu_data->cpu$b_iplnr++] = cpu_data->cpu$b_curipl; \
        cpu_data->cpu$b_curipl = value; \
    }

#define enable_interrupts() \
    { \
        struct _cpu *cpu_data = find_cpu_data(); \
        cpu_data->cpu$b_curipl = cpu_data->cpu$b_prev_ipl[--cpu_data->cpu$b_iplnr]; \
        if (cpu_data->cpu$b_curipl == 0) \
        { \
            cpu_enable_interrupts(); \
        } \
    }

#define reset_ipl() \
    find_cpu_data()->cpu$b_curipl = 0; \
    cpu_enable_interrupts();

#define set_ipl(value) \
    cpu_disable_interrupts(); \
    find_cpu_data()->cpu$b_curipl = value;

#endif /* _VMS_MACROS_H */
