/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <stdarg.h>

#include <system_routines.h>
#include <trace.h>

unsigned int div(unsigned long *n, unsigned int base);
void tr_putchar(char c);
void tr_putnumber(unsigned long num, unsigned int base, unsigned int pad);
void tr_putstr(const char *s);
void tr_vprint(const char *fmt, va_list arg);

void tr_init(void)
{
    con$init_cty();
}

unsigned int div(unsigned long *n, unsigned int base)
{
    unsigned int res = (unsigned int)((*n) % base);
    *n = (*n) / base;
    return res;
}

void tr_putchar(char c)
{
    con$putchar(c);
}

void tr_putnumber(unsigned long num, unsigned int base, unsigned int pad)
{
    static const char *digits = "0123456789abcdef";
    static char tmp[16];
    unsigned int i = 0;
    unsigned int j;

    if (num == 0)
    {
        tmp[i++] = '0';
    }
    else
    {
        while (num != 0)
        {
            tmp[i++] = digits[div(&num, base)];
        }
    }
    for(j = i; j < pad; j++)
    {
        tr_putchar('0');
    }
    while(i > 0)
    {
        i--;
        tr_putchar(tmp[i]);
    }
}

void tr_putstr(const char *s)
{
    while (*s != '\0')
    {
        tr_putchar(*s++);
    }
}

void tr_vprint(const char *fmt, va_list arg)
{
    for(; *fmt; fmt++)
    {
        if (*fmt != '%')
        {
            tr_putchar(*fmt);
            continue;
        }

        fmt++;
        switch(*fmt)
        {
            case 'c':
            {
                int c = va_arg(arg, int);
                tr_putchar((char)(c & 0xff));
                break;
            }
            case 'd':
            {
                long num = va_arg(arg, long);
                tr_putnumber((unsigned long)num, 10, 0);
                break;
            }
            case 's':
            {
                char *str = va_arg(arg, char *);
                tr_putstr(str);
                break;
            }
            case 'x':
            {
                unsigned long num = va_arg(arg, unsigned long);
                tr_putnumber(num, 16, 0);
                break;
            }
            default:
                /* ignore unsupported flags */
                break;
        }
    }
}

void tr_print(const char *fmt, ...)
{
    va_list arg;
    va_start(arg, fmt);
    tr_vprint(fmt, arg);
    va_end(arg);
}
