/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _IPLDEF_H
#define _IPLDEF_H

#include <stdint.h>

/* IPLs */
#define IPL$_NORMAL      0
#define IPL$_ASTDEL      2
#define IPL$_RESCHED     3
#define IPL$_IOPOST      4
#define IPL$_QUEUEAST    6
#define IPL$_FILSYS      8
#define IPL$_TIMER       8
#define IPL$_JIB         8
#define IPL$_MMG         8
#define IPL$_SCHED       8
#define IPL$_SYNCH       8
#define IPL$_MAILBOX    11
#define IPL$_POOL       11
#define IPL$_HWCLK      24
#define IPL$_MEGA       31
#define IPL$_POWER      31

typedef uint8_t ipl_t;

#endif /* _IPLDEF_H */
