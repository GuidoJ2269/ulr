/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <string.h>

#include <allocpfn.h>
#include <elf64.h>
#include <list.h>
#include <ssdef.h>
#include <system_data_cells.h>
#include <system_routines.h>
#include <util.h>

#ifdef BOOTLOADER
#include <boo_routines.h>
#endif

#include <sysldr.h>

/* private functions */
int ldr$check_elf(struct _ldrimg *image);
struct _ldrimg * ldr$find_image(struct dsc$descriptor_s *name);
int ldr$load_elf(struct _ldrimg *image, union pgd_entry *pgd);

#ifdef BOOTLOADER
struct _ldrimg *ldr$gq_boot_image_list_head = 0;
struct _ldrimg *ldr$gq_boot_image_list_tail = 0;
#endif

unsigned long ldr$next_s0_module_address;

void ldr$insert_image(struct _ldrimg *image)
{
#ifdef BOOTLOADER
    LIST_INSERT_TAIL(ldrimg, ldr$gq_boot_image_list_head, ldr$gq_boot_image_list_tail, image);
#else
    LIST_INSERT_TAIL(ldrimg, ldr$gq_image_list_head, ldr$gq_image_list_tail, image);
#endif
}

struct _ldrimg * ldr$find_image(struct dsc$descriptor_s *name)
{
#ifdef BOOTLOADER
    struct _ldrimg *image = ldr$gq_boot_image_list_head;
#else
    struct _ldrimg *image = ldr$gq_image_list_head;
#endif
    while ((image != 0) &&
           (strncmp(name->dsc$a_pointer, image->ldrimg$s_imgnam, MIN(name->dsc$w_length, strlen(image->ldrimg$s_imgnam))) != 0))
    {
        image = image->ldrimg$ps_flink;
    }
    return image;
}

int ldr$load_nonpaged(void *data, size_t data_len, void *vaddr, size_t vsize, union pgd_entry *pgd, unsigned int rw, unsigned int us)
{
    size_t num_pages = PAGE_ROUNDUP(vsize) / PAGE_SIZE;
    int retval = mmg_std$allocpfn_no_db(pgd, (addr_t)vaddr, num_pages);
    if (retval == SS$_NORMAL)
    {
        memcpy(vaddr, data, data_len);
        mmu_set_access_pages(pgd, (addr_t)vaddr, rw, us, num_pages);
    }
    return retval;
}

int ldr$check_elf(struct _ldrimg *image)
{
    Elf64_Ehdr *elf_hdr = (Elf64_Ehdr *)(image->ldrimg$l_base);
    unsigned long size = image->ldrimg$l_length;
    return ((elf_hdr != 0) &&
            (size >= (unsigned long)(elf_hdr->e_ehsize + (elf_hdr->e_phnum * elf_hdr->e_phentsize) + (elf_hdr->e_shnum * elf_hdr->e_shentsize))) &&
            (elf_hdr->e_ident[EI_MAG0] == 0x7f) && (elf_hdr->e_ident[EI_MAG1] == 'E') && (elf_hdr->e_ident[EI_MAG2] == 'L')  && (elf_hdr->e_ident[EI_MAG3] == 'F'));
}

int ldr$load_elf(struct _ldrimg *image, union pgd_entry *pgd)
{
    Elf64_Ehdr *elf_hdr = (Elf64_Ehdr *)image->ldrimg$l_base;
    unsigned long offset = 0;

    if (elf_hdr->e_entry < LARGE_PAGE_SIZE)
    {
        offset = ldr$next_s0_module_address;
    }
#ifdef BOOTLOADER
    boo$kprintf("LoadELF: addr=%x, size=%d, entry=%x, offset=%x\n", image->ldrimg$l_base, image->ldrimg$l_length, elf_hdr->e_entry, offset);
#endif

    image->ldrimg$l_init_rtn = (void *)elf_hdr->e_entry + offset;
    for (unsigned int i = 0; i < elf_hdr->e_phnum; i++)
    {
        Elf64_Phdr *elf_program_hdr = (Elf64_Phdr *)(image->ldrimg$l_base + elf_hdr->e_phoff + elf_hdr->e_phentsize * i);
        unsigned long vaddr = PAGE_ROUNDDOWN(elf_program_hdr->p_vaddr) + offset;
        unsigned long vaddr_end = PAGE_ROUNDUP(vaddr + elf_program_hdr->p_memsz);

        ldr$load_nonpaged((void *)(image->ldrimg$l_base + elf_program_hdr->p_offset), elf_program_hdr->p_filesz, (void *)vaddr, elf_program_hdr->p_memsz, pgd, (elf_program_hdr->p_flags & PF_W) ? 1 : 0, 0);
#ifdef BOOTLOADER
        //boo$print("Program: type=%d flags=%x off=%x (%d) vaddr=%x (%d) align=%d\n", elf_program_hdr->p_type, elf_program_hdr->p_flags, elf_program_hdr->p_offset, elf_program_hdr->p_filesz, vaddr, elf_program_hdr->p_memsz, elf_program_hdr->p_align);
#endif
        if (vaddr_end > ldr$next_s0_module_address)
        {
            ldr$next_s0_module_address = vaddr_end;
        }
    }

    void *elf_symbol_table = 0;
    Elf64_Xword elf_symbol_table_size = 0;
    Elf64_Xword elf_symbol_table_entsize = 0;
    char *elf_string_table = 0;
    for (unsigned int i = 0; i < elf_hdr->e_shnum; i++)
    {
        Elf64_Shdr *elf_section_hdr = (Elf64_Shdr *)(image->ldrimg$l_base + elf_hdr->e_shoff + elf_hdr->e_shentsize * i);
        if (elf_section_hdr->sh_type > 0)
        {
#ifdef BOOTLOADER
            //boo$print("Section %d, %x: type=%x off=%x size=%x addr=%x\n", i, elf_section_hdr->sh_name, elf_section_hdr->sh_type, elf_section_hdr->sh_offset, elf_section_hdr->sh_size, elf_section_hdr->sh_addr);
#endif
        }
        if (elf_section_hdr->sh_type == SHT_SYMTAB)
        {
            elf_symbol_table = (void *)(image->ldrimg$l_base + elf_section_hdr->sh_offset);
            elf_symbol_table_size = elf_section_hdr->sh_size;
            elf_symbol_table_entsize = elf_section_hdr->sh_entsize;
        }
        if (elf_section_hdr->sh_type == SHT_STRTAB)
        {
            elf_string_table = (void *)(image->ldrimg$l_base + elf_section_hdr->sh_offset);
        }
    }

    for (unsigned long l = 0; l < elf_symbol_table_size; l += elf_symbol_table_entsize)
    {
        Elf64_Sym *elf_symbol = elf_symbol_table + l;
        if ((elf_symbol->st_shndx > SHN_UNDEF) && (elf_symbol->st_shndx < SHN_LOPROC) &&
            ((elf_symbol->st_info >> 4) == STB_GLOBAL) && ((elf_symbol->st_info & 0x0f) != STT_NOTYPE))
        {
#ifdef BOOTLOADER
            //boo$print("Symbol %x: name=%s info=%x index=%x value=%x size=%x\n", elf_symbol->st_name, elf_string_table + elf_symbol->st_name, elf_symbol->st_info, elf_symbol->st_shndx, elf_symbol->st_value, elf_symbol->st_size);
#endif
            if (strncmp(elf_string_table + elf_symbol->st_name, "ini$doinit\0", 11) == 0)
            {
#ifdef BOOTLOADER
                //boo$print("Symbol %x: name=%s info=%x index=%x value=%x size=%x\n", elf_symbol->st_name, elf_string_table + elf_symbol->st_name, elf_symbol->st_info, elf_symbol->st_shndx, elf_symbol->st_value, elf_symbol->st_size);
#endif
                image->ldrimg$l_init_rtn = (void *)elf_symbol->st_value + offset;
            }
        }
    }

    return SS$_NORMAL;
}

int ldr$load_image(struct dsc$descriptor_s *name, union ldr$flags flags, struct _ldrimg **ref, union pgd_entry *pgd)
{
    int retval = SS$_BADPARAM;
    if (ref != 0)
    {
        *ref = ldr$find_image(name);
        if ((*ref != 0) && (flags.ldr$v_pag == 1))
        {
            if (ldr$check_elf(*ref))
            {
                retval = ldr$load_elf(*ref, pgd);
                if (retval == SS$_NORMAL)
                {
                    (*ref)->ldrimg$v_delay_init = 1;
                }
            }
            else
            {
                retval = SS$_BADIMGHDR;
            }
        }
    }

    return retval;
}

void ldr$init_all(void)
{
#ifdef BOOTLOADER
    struct _ldrimg *image = ldr$gq_boot_image_list_head;
#else
    struct _ldrimg *image = ldr$gq_image_list_head;
#endif
    while (image != 0)
    {
        if ((image->ldrimg$l_init_rtn != 0) && (image->ldrimg$v_delay_init == 1))
        {
            exe$kprintf("Load %s\n", image->ldrimg$s_imgnam);
            image->ldrimg$l_init_rtn();
        }
        image = image->ldrimg$ps_flink;
    }
}

void ldr$init_single(struct _ldrimg *image)
{
    if (image != 0)
    {
        if (image->ldrimg$l_init_rtn != 0)
        {
            image->ldrimg$l_init_rtn();
        }
    }
}
