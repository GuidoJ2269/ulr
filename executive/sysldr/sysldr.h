/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSLDR_H
#define _SYSLDR_H

#include <descrip.h>
#include <ldrimgdef.h>
#include <mmu.h>

union ldr$flags
{
    uint32_t value;
    struct
    {
        uint32_t ldr$v_pag : 1;
        uint32_t ldr$v_unl : 1;
        uint32_t ldr$v_ovr : 1;
        uint32_t ldr$v_user_buf : 1;
        uint32_t ldr$v_no_slice : 1;
    };
};

#define MAXNUM_BOOT_MODULES    16

#ifdef BOOTLOADER
struct _ldrimg *ldr$gq_boot_image_list_head;
struct _ldrimg *ldr$gq_boot_image_list_tail;
#endif

void ldr$init_all(void);
void ldr$init_single(struct _ldrimg *image);

void ldr$insert_image(struct _ldrimg *image);
int ldr$load_image(struct dsc$descriptor_s *name, union ldr$flags flags, struct _ldrimg **ref, union pgd_entry *pgd);
int ldr$load_nonpaged(void *data, size_t data_len, void *vaddr, size_t vsize, union pgd_entry *pgd, unsigned int rw, unsigned int us);

#endif /* _SYSLDR_H */
