/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _COUNTED_STRING_H
#define _COUNTED_STRING_H

#include <stdint.h>

#define COUNTED_STRING(structure,length) \
    union \
    { \
        char structure ## $t_name[length]; \
        struct \
        { \
            uint8_t structure ## $b_name_len; \
            char structure ## $t_name_str[length-1]; \
        }; \
    }

#define COUNTED_STRING_ASSIGN(structure,string_ptr,length) \
    structure->structure ## $b_name_len = (uint8_t)strnlen(string_ptr, length-1); \
    strncpy(structure->structure ## $t_name_str, string_ptr, length-1)

#endif /* _COUNTED_STRING_H */
