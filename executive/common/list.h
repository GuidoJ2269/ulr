/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _LIST_H
#define _LIST_H

#define LIST_INSERT_HEAD(name, head_ptr, tail_ptr, item_ptr) \
    do \
    { \
        if (head_ptr == 0) \
        { \
            (item_ptr)->name ## $ps_flink = 0; \
            (item_ptr)->name ## $ps_blink = 0; \
            tail_ptr = item_ptr; \
        } \
        else \
        { \
            (item_ptr)->name ## $ps_flink = head_ptr; \
            (item_ptr)->name ## $ps_blink = 0; \
            (head_ptr)->name ## $ps_blink = item_ptr; \
        } \
        head_ptr = item_ptr; \
    } \
    while (0);

#define LIST_INSERT_TAIL(name, head_ptr, tail_ptr, item_ptr) \
    do \
    { \
        if (tail_ptr == 0) \
        { \
            (item_ptr)->name ## $ps_flink = 0; \
            (item_ptr)->name ## $ps_blink = 0; \
            head_ptr = item_ptr; \
        } \
        else \
        { \
            (item_ptr)->name ## $ps_flink = 0; \
            (item_ptr)->name ## $ps_blink = tail_ptr; \
            (tail_ptr)->name ## $ps_flink = item_ptr; \
        } \
        tail_ptr = item_ptr; \
    } \
    while (0);

#define LIST_REMOVE(name, head_ptr, tail_ptr, item_ptr) \
    do \
    { \
        struct _ ## name *next = (item_ptr)->name ## $ps_flink; \
        struct _ ## name *prev = (item_ptr)->name ## $ps_blink; \
        if (next != 0) \
        { \
            next->name ## $ps_blink = prev; \
        } \
        if (prev != 0) \
        { \
            prev->name ## $ps_flink = next; \
        } \
        if (head_ptr == item_ptr) \
        { \
            head_ptr = (head_ptr)->name ## $ps_flink; \
        } \
        if (tail_ptr == item_ptr) \
        { \
            tail_ptr = (tail_ptr)->name ## $ps_blink; \
        } \
        (item_ptr)->name ## $ps_flink = 0; \
        (item_ptr)->name ## $ps_blink = 0; \
    } \
    while (0);

#endif /* _LIST_H */
