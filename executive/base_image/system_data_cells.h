/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSTEM_DATA_CELLS_H
#define _SYSTEM_DATA_CELLS_H

#include <dptdef.h>
#include <ldrimgdef.h>
#include <mtxdef.h>
#include <pfndef.h>
#include <sections.h>
#include <spldef.h>

#define PFN_FREE_LIST    0
#define PFN_MODIFED_LIST 1
#define PFN_BAD_LIST     2
#define NUM_PFN_LISTS    3

#define VERSION_LENGTH 16

#define sch$gl_freecnt  pfn$al_count[PFN_FREE_LIST]
#define sch$gl_mfycnt   pfn$al_count[PFN_MODIFED_LIST]

extern struct _pfn *    pfn$al_head[NUM_PFN_LISTS]      S0_NON_PAGED_DATA;
extern struct _pfn *    pfn$al_tail[NUM_PFN_LISTS]      S0_NON_PAGED_DATA;
extern page_count_t     pfn$al_count[NUM_PFN_LISTS]     S0_NON_PAGED_DATA;
extern page_count_t     pfn$gl_phypgcnt                 S0_NON_PAGED_DATA;

extern struct _ldrimg * ldr$gq_image_list_head          S0_NON_PAGED_DATA;
extern struct _ldrimg * ldr$gq_image_list_tail          S0_NON_PAGED_DATA;

extern char             sys$gq_version[VERSION_LENGTH]  S0_NON_PAGED_DATA;

extern struct _mtx      ioc$gl_mutex                    S0_NON_PAGED_DATA;

extern struct _vlb *    exe$gl_nonpaged                 S0_NON_PAGED_DATA;
extern struct _vlb *    exe$gl_paged                    S0_NON_PAGED_DATA;

extern struct _spl *    smp$ar_spnlkvec                 S0_NON_PAGED_DATA;
extern unsigned short   smp$gw_spnlkcnt                 S0_NON_PAGED_DATA;

extern unsigned short   smp$gw_cpu_cnt                  S0_NON_PAGED_DATA;
extern struct _cpu      smp$gl_cpu_data[MAX_NUM_CPUS]   S0_NON_PAGED_DATA;
extern unsigned short   smp$gw_bus_cnt                  S0_NON_PAGED_DATA;
extern unsigned short   smp$gw_ioapic_cnt               S0_NON_PAGED_DATA;

extern struct _ddb *    opa$ar_ddb                      S0_NON_PAGED_DATA;
extern struct _ucb *    opa$ar_ucb0                     S0_NON_PAGED_DATA;
extern struct _dpt *    opa$ar_dpt                      S0_NON_PAGED_DATA;

#endif /* _SYSTEM_DATA_CELLS_H */
