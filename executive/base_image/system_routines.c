/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <cpu.h>
#include <ssdef.h>
#include <trace.h>
#include <util.h>

#include "system_routines.h"

struct base_vtable base_image = {
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,

    (int_int_func)exe$load_error,
    (int_int_func)exe$load_error,

    (int_int_func)exe$load_error,
    (long_long_func)exe$load_error,

    (pvoid_cpvoid_int_size_func)exe$load_error,
    (int_cpvoid_cpvoid_size_func)exe$load_error,
    (pvoid_pvoid_cpvoid_size_func)exe$load_error,
    (pvoid_pvoid_cpvoid_size_func)exe$load_error,
    (pvoid_pvoid_int_size_func)exe$load_error,

    (pchar_pchar_cpchar_func)exe$load_error,
    (pchar_cpchar_int_func)exe$load_error,
    (int_cpchar_cpchar_func)exe$load_error,
    (pchar_pchar_cpchar_func)exe$load_error,
    (size_cpchar_cpchar_func)exe$load_error,
    (size_cpchar_func)exe$load_error,
    (pchar_pchar_cpchar_size_func)exe$load_error,
    (int_cpchar_cpchar_size_func)exe$load_error,
    (pchar_pchar_cpchar_size_func)exe$load_error,
    (size_cpchar_size_func)exe$load_error,
    (pchar_cpchar_int_func)exe$load_error,
    (size_cpchar_cpchar_func)exe$load_error,
    (pchar_cpchar_cpchar_func)exe$load_error,

    (void_pl1pte_addr_addr_ptf_size_func)exe$load_error,
    (void_pl1pte_addr_size_func)exe$load_error,

    (void_void_func)exe$load_error,
    (void_cchar_func)exe$load_error,

    (void_void_func)exe$load_error,
    (void_cuchar_func)exe$load_error,
    (void_cchar_func)exe$load_error,
    (void_cpchar_func)exe$load_error,
    (void_void_func)exe$load_error,
    (void_cuint_func)exe$load_error,
    (void_cpchar_func)exe$load_error,

    (int_pchan_ppccb_func)exe$load_error,
    (int_pchan_ppccb_func)exe$load_error,

    (void_cpchar__func)exe$load_error,
    (void_cpchar_valist_func)exe$load_error,

    (int_ppvlb_pvlb_func)exe$load_error,
    (int_ppvlb_ppvlb_func)exe$load_error,
    (void_ppvlb_func)exe$load_error,
    (void_ppvlb_func)exe$load_error,
    (void_ppvlb_int_func)exe$load_error,

    (int_ppvlb_psize_ppvoid_func)exe$load_error,
    (int_ppvlb_pvoid_size_func)exe$load_error,
    (int_psize_ppvoid_func)exe$load_error,
    (int_pvoid_size_func)exe$load_error,
    (int_pvoid_size_func)exe$load_error,

    (pfn_pppfn_func)exe$load_error,
    (pfn_pppfn_func)exe$load_error,
    (void_pfn_ppfn_func)exe$load_error,
    (void_pfn_ppfn_func)exe$load_error,
    (void_pfn_uint_ppfn_func)exe$load_error,
    (void_pfn_uint_ppfn_func)exe$load_error,
    (void_pfn_uint_ppfn_func)exe$load_error,
    (pfn_uint_pppfn_func)exe$load_error,

    (void_pmtx_func)exe$load_error,
    (void_pmtx_func)exe$load_error,
    (int_pmtx_func)exe$load_error,
    (void_pmtx_func)exe$load_error,

    (void_splindex_func)exe$load_error,
    (void_pspl_func)exe$load_error,
    (void_splindex_func)exe$load_error,
    (void_pspl_func)exe$load_error,
    (void_splindex_func)exe$load_error,
    (void_pspl_func)exe$load_error
};

int decc$isalnum(int c)
{
    return base_image.decc_isalnum(c);
}

int decc$isalpha(int c)
{
    return base_image.decc_isalpha(c);
}

int decc$iscntrl(int c)
{
    return base_image.decc_iscntrl(c);
}

int decc$isdigit(int c)
{
    return base_image.decc_isdigit(c);
}

int decc$isgraph(int c)
{
    return base_image.decc_isgraph(c);
}

int decc$islower(int c)
{
    return base_image.decc_islower(c);
}

int decc$isprint(int c)
{
    return base_image.decc_isprint(c);
}

int decc$ispunct(int c)
{
    return base_image.decc_ispunct(c);
}

int decc$isspace(int c)
{
    return base_image.decc_isspace(c);
}

int decc$isupper(int c)
{
    return base_image.decc_isupper(c);
}

int decc$isxdigit(int c)
{
    return base_image.decc_isxdigit(c);
}

int decc$isascii(int c)
{
    return base_image.decc_isascii(c);
}

int decc$toascii(int c)
{
    return base_image.decc_toascii(c);
}

int decc$tolower(int c)
{
    return base_image.decc_tolower(c);
}

int decc$toupper(int c)
{
    return base_image.decc_toupper(c);
}

int decc$abs (int i)
{
    return base_image.decc_abs(i);
}

long int decc$labs (long int i)
{
    return base_image.decc_labs(i);
}

void *decc$memchr(const void *s, int c, size_t n)
{
    return base_image.decc_memchr(s, c, n);
}

int decc$memcmp(void* s1, void* s2, size_t n)
{
    return base_image.decc_memcmp(s1, s2, n);
}

void *decc$memcpy(void *dest, void *src, size_t n)
{
    return base_image.decc_memcpy(dest, src, n);
}

void *decc$memmove(void *dest, void *src, size_t n)
{
    return base_image.decc_memmove(dest, src, n);
}

void *decc$memset(void *s, int c, size_t n)
{
    return base_image.decc_memset(s, c, n);
}

char *decc$strcat(char *dest, char *src)
{
    return base_image.decc_strcat(dest, src);
}

char *decc$strchr(const char *s, int c)
{
    return base_image.decc_strchr(s, c);
}

int decc$strcmp(char *s1, char *s2)
{
    return base_image.decc_strcmp(s1, s2);
}

char *decc$strcpy(char *dest, char *src)
{
    return base_image.decc_strcpy(dest, src);
}

size_t decc$strcspn(const char *s1, const char *s2)
{
    return base_image.decc_strcspn(s1, s2);
}

size_t decc$strlen(char *s)
{
    return base_image.decc_strlen(s);
}

char *decc$strncat(char *dest, char *src, size_t n)
{
    return base_image.decc_strncat(dest, src, n);
}

int decc$strncmp( char *s1, char *s2, size_t n)
{
    return base_image.decc_strncmp(s1, s2, n);
}

char *decc$strncpy(char *dest, char *src, size_t n)
{
    return base_image.decc_strncpy(dest, src, n);
}

size_t decc$strnlen(char *s, size_t n)
{
    return base_image.decc_strnlen(s, n);
}

char *decc$strrchr(const char *s, int c)
{
    return base_image.decc_strrchr(s, c);
}

size_t decc$strspn(const char *s1, const char *s2)
{
    return base_image.decc_strspn(s1, s2);
}

char *decc$strstr(const char *s1, const char *s2)
{
    return base_image.decc_strstr(s1, s2);
}

void boo$map(l1pte *p, addr_t paddr, addr_t vaddr, union pt_flags flags, size_t n)
{
    base_image.boo_map(p, paddr, vaddr, flags, n);
}

void boo$unmap(l1pte *p, addr_t vaddr, size_t n)
{
    base_image.boo_unmap(p, vaddr, n);
}

void con$init_cty(void)
{
    base_image.con_init_cty();
}

void con$putchar(char c)
{
    base_image.con_putchar(c);
}

void exe$outblank(void)
{
    base_image.exe_outblank();
}

void exe$outbyte(const unsigned char c)
{
    base_image.exe_outbyte(c);
}

void exe$outchar(const char c)
{
    base_image.exe_outchar(c);
}

void exe$outcrlf(void)
{
    base_image.exe_outcrlf();
}

void exe$outcstring(const char *s)
{
    base_image.exe_outcstring(s);
}

void exe$outhex(const unsigned int x)
{
    base_image.exe_outhex(x);
}

void exe$outzstring(const char *s)
{
    base_image.exe_outztring(s);
}

int ioc$ffchan(channel_t *chan, struct _ccb **ccb)
{
    return base_image.ioc_ffchan(chan, ccb);
}

int ioc$verifychan(channel_t *chan, struct _ccb **ccb)
{
    return base_image.ioc_verifychan(chan, ccb);
}

void exe$kprintf(const char *format, ...)
{
    va_list arg;
    va_start(arg, format);
    base_image.exe_kvprintf(format, arg);
    va_end(arg);
}

void exe$kvprintf(const char *format, va_list arg)
{
    base_image.exe_kvprintf(format, arg);
}

void exe$lal_insert_first(struct _vlb ** head, struct _vlb *block)
{
    base_image.exe_lal_insert_first(head, block);
}

int exe$lal_remove_first(struct _vlb ** head, struct _vlb **block)
{
    return base_image.exe_lal_remove_first(head, block);
}

void exe$extendpool(struct _vlb **head)
{
    base_image.exe_extendpool(head);
}

void exe$flushlists(struct _vlb **head)
{
    base_image.exe_flushlists(head);
}

void exe$reclaimlists(struct _vlb **head, int aggressive)
{
    base_image.exe_reclaimlists(head, aggressive);
}

int exe$allocate(struct _vlb **head, size_t *size, void **ptr)
{
    return base_image.exe_allocate(head, size, ptr);
}

int exe$deallocate(struct _vlb **head, void *ptr, size_t size)
{
    return base_image.exe_deallocate(head, ptr, size);
}

int exe$alononpaged(size_t *size, void **ptr)
{
    return base_image.exe_alononpaged(size, ptr);
}

int exe$deanonpaged(void *ptr, size_t size)
{
    return base_image.exe_deanonpaged(ptr, size);
}

int exe$deanonpgdsiz(void *ptr, size_t size)
{
    /* exe_deanonpgsiz is identical to exe_deanonpaged */
    return base_image.exe_deanonpaged(ptr, size);
}

pfn_t mmg$alloc_pfn(struct _pfn **pfndbe)
{
    return base_image.mmg_alloc_pfn(pfndbe);
}

pfn_t mmg$alloc_zero_pfn(struct _pfn **pfndbe)
{
    return base_image.mmg_alloc_zero_pfn(pfndbe);
}

void mmg$dalloc_pfn(pfn_t n, struct _pfn *pfndbe)
{
    base_image.mmg_dalloc_pfn(n, pfndbe);
}

void mmg$dalloc_zero_pfn(pfn_t n, struct _pfn *pfndbe)
{
    base_image.mmg_dalloc_zero_pfn(n, pfndbe);
}

void mmg$ins_pfnh(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    base_image.mmg_ins_pfnh(n, lst, pfndbe);
}

void mmg$ins_pfnt(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    base_image.mmg_ins_pfnt(n, lst, pfndbe);
}

void mmg$rem_pfn(pfn_t n, unsigned int lst, struct _pfn *pfndbe)
{
    base_image.mmg_rem_pfn(n, lst, pfndbe);
}

pfn_t mmg$rem_pfnh(unsigned int lst, struct _pfn **pfndbe)
{
    return base_image.mmg_rem_pfnh(lst, pfndbe);
}

void sch$lockr(struct _mtx *mutex)
{
    base_image.sch_lockr(mutex);
}

void sch$lockw(struct _mtx *mutex)
{
    base_image.sch_lockw(mutex);
}

int sch$lockwnowait(struct _mtx *mutex)
{
    return base_image.sch_locknowait(mutex);
}

void sch$unlock(struct _mtx *mutex)
{
    base_image.sch_unlock(mutex);
}

void smp$acquire(enum _spl_index index)
{
    base_image.smp_acquire(index);
}

void smp$acquirel(struct _spl *spinlock)
{
    base_image.smp_acquirel(spinlock);
}

void smp$restore(enum _spl_index index)
{
    base_image.smp_restore(index);
}

void smp$restorel(struct _spl *spinlock)
{
    base_image.smp_restorel(spinlock);
}

void smp$release(enum _spl_index index)
{
    base_image.smp_release(index);
}

void smp$releasel(struct _spl *spinlock)
{
    base_image.smp_releasel(spinlock);
}

/* Returns to its caller */
void ioc$return(void)
{
    /* nothing */
}

void exe$load_error(void)
{
    cpu_halt();
}
