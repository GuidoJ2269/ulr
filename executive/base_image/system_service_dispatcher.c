/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <stdlib.h>

#include <ssdef.h>

#include "sections.h"
#include "system_routines.h"
#include "system_service_dispatcher.h"

typedef int (*service_routine_0)(void);
typedef int (*service_routine_1)(reg_t p1);
typedef int (*service_routine_2)(reg_t p1, reg_t p2);
typedef int (*service_routine_3)(reg_t p1, reg_t p2, reg_t p3);
typedef int (*service_routine_4)(reg_t p1, reg_t p2, reg_t p3, reg_t p4);
typedef int (*service_routine_5)(reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5);
typedef int (*service_routine_6)(reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6);
typedef void (*synch_routine)(void);

/* system service exit function table */
synch_routine cmod$al_exit_type[] S0_NON_PAGED_DATA =
{
    0,
    0, /* SYNCH$RMS_STALL */
    0, /* SYNCH$RMS_WAIT */
    0  /* SYNCH$ASSIGN_EXIT */
};

struct chmdte cmod$ar_kernel_dispatch_vector[DISPATCH_VECTOR_SIZE] S0_NON_PAGED_DATA =
{
    { SSET_NULL, 0, exe$load_error }
};

struct chmdte cmod$ar_exec_dispatch_vector[DISPATCH_VECTOR_SIZE] S0_NON_PAGED_DATA =
{
    { SSET_NULL, 0, exe$load_error }
};

int exe$cmoddispatch(struct chmdte* dispatch_entry, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6) S0_NON_PAGED_CODE;

int exe$cmodkrnl(int service_id, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6)
{
    /* TODO: change to kernel mode */
    struct chmdte *dispatch_entry = &cmod$ar_kernel_dispatch_vector[service_id];
    if (dispatch_entry == 0)
    {
        return SS$_ACCVIO;
    }
    return exe$cmoddispatch(dispatch_entry, p1, p2, p3, p4, p5, p6);
}

int exe$cmodexec(int service_id, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6)
{
    /* TODO: change to exec mode */
    struct chmdte *dispatch_entry = &cmod$ar_exec_dispatch_vector[service_id];
    if (dispatch_entry == 0)
    {
        return SS$_ACCVIO;
    }
    return exe$cmoddispatch(dispatch_entry, p1, p2, p3, p4, p5, p6);
}

int exe$cmoddispatch(struct chmdte *dispatch_entry, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6)
{
    int retval = SS$_ACCVIO;
    switch (dispatch_entry->arg_count)
    {
        case 0:
            {
                service_routine_0 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine();
                }
            }
            break;
        case 1:
            {
                service_routine_1 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1);
                }
            }
            break;
        case 2:
            {
                service_routine_2 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1, p2);
                }
            }
            break;
        case 3:
            {
                service_routine_3 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1, p2, p3);
                }
            }
            break;
        case 4:
            {
                service_routine_4 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1, p2, p3, p4);
                }
            }
            break;
        case 5:
            {
                service_routine_5 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1, p2, p3, p4, p5);
                }
            }
            break;
        case 6:
            {
                service_routine_6 service_routine = dispatch_entry->service_routine_addr;
                if (service_routine != 0)
                {
                    retval = service_routine(p1, p2, p3, p4, p5, p6);
                }
            }
            break;
        default:
            exe$load_error();
            break;
    }
    if (dispatch_entry->exit_type_code != SSET_NULL)
    {
        cmod$al_exit_type[dispatch_entry->exit_type_code]();
    }
    return retval;
}

#define SYSCALL(SERVICE) \
int sys$ ## SERVICE () \
{ \
    return exe$ ## SERVICE (); \
}

/*
int sys$qio()
{
    return exe$qio();
}
*/
