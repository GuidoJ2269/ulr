/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <sysparam.h>

union pgd_entry* mmg$gl_syspgd = 0;

void * mmg$gl_npagedyn = 0;
void * mmg$gl_npagnext = 0;
void * mmg$gl_pagedyn = 0;

pfn_t  mmg$gl_maxpfn = 0;
pfn_t  mmg$gl_minpfn = 0;
size_t mmg$gl_maxmem = 0;

struct _pmap * mmg$gl_free_no_pfn_db_list = 0;
struct _pfn *  pfn$a_base = 0;

union boostate exe$gl_state = { 0 };

char     exe$gb_cpuvendor[CPUVENDOR_LEN] = "";
uint16_t exe$gw_cpufamily = 0;
uint8_t  exe$gb_cpumodel = 0;
uint8_t  exe$gb_cpustepping = 0;

uint16_t sgn$gw_pchancnt = CHANNELCNT;
size_t sgn$gl_npagedyn = NPAGEDYN;
size_t sgn$gl_npagevir = NPAGEVIR;
size_t sgn$gl_pagedyn = PAGEDYN;
