/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SECTIONS_H
#define _SECTIONS_H

#define P1_POINTER_PAGE __attribute__ ((section (".p1_pointer_page")))

#ifdef BOOTLOADER
#define S0_NON_PAGED_CODE
#define S0_NON_PAGED_DATA
#else
#define S0_NON_PAGED_CODE __attribute__ ((section (".non_paged_code")))
#define S0_NON_PAGED_DATA __attribute__ ((section (".non_paged_data")))
#endif

#define S0_BOOPARAM __attribute__ ((section (".booparam_data")))
#define S0_SYSPARAM __attribute__ ((section (".sysparam_data")))

#define S0_SYSTEM_SERVICE_VECTORS __attribute__ ((section (".system_service_vectors")))
#define S0_EXECUTVE_TRANSFER_VECTORS __attribute__ ((section (".executive_transfer_vectors")))

#endif /* _SECTIONS_H */
