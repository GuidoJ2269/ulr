/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ssdef.h>

#include "system_service_loader.h"

int exe$connect_services(enum system_service_mode mode, int service_id, enum system_service_exit_type exit_type_code,
                         unsigned int arg_count, void *func_addr)
{
    if ((service_id < 0) || (service_id >= DISPATCH_VECTOR_SIZE))
    {
        return SS$_BADPARAM;
    }
    struct chmdte *entry = 0;
    switch (mode)
    {
        case SSM_KERNEL:
            entry = &cmod$ar_kernel_dispatch_vector[service_id];
            break;
        case SSM_EXECUTIVE:
            entry = &cmod$ar_exec_dispatch_vector[service_id];
            break;
        default:
            return SS$_BADPARAM;
    };
    entry->exit_type_code = exit_type_code;
    entry->arg_count = arg_count;
    entry->service_routine_addr = func_addr;
    return SS$_NORMAL;
}
