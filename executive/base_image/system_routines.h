/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSTEM_ROUTINES_H
#define _SYSTEM_ROUTINES_H

#include <stdarg.h>

#include <ccbdef.h>
#include <memoryalc.h>
#include <mmu.h>
#include <mtxdef.h>
#include <pfndef.h>
#include <sections.h>
#include <spldef.h>

typedef int (*int_cpchar_cpchar_func)(const char *, const char *);
typedef int (*int_cpchar_cpchar_size_func)(const char *, const char *, size_t);
typedef int (*int_cpvoid_cpvoid_size_func)(const void *, const void *, size_t);
typedef int (*int_int_func)(int);
typedef int (*int_pchan_ppccb_func)(channel_t *, struct _ccb **);
typedef int (*int_pmtx_func)(struct _mtx *);
typedef int (*int_ppvlb_pvlb_func)(struct _vlb **, struct _vlb *);
typedef int (*int_ppvlb_ppvlb_func)(struct _vlb **, struct _vlb **);
typedef int (*int_ppvlb_psize_ppvoid_func)(struct _vlb **, size_t *, void **);
typedef int (*int_ppvlb_pvoid_size_func)(struct _vlb **, void *, size_t);
typedef int (*int_psize_ppvoid_func)(size_t *, void **);
typedef int (*int_pvoid_size_func)(void *, size_t);
typedef long int (*long_long_func)(long int);
typedef char * (*pchar_cpchar_cpchar_func)(const char *, const char *);
typedef char * (*pchar_cpchar_int_func)(const char *, int);
typedef char * (*pchar_pchar_cpchar_func)(char *, const char *);
typedef char * (*pchar_pchar_cpchar_size_func)(char *, const char *, size_t);
typedef pfn_t (*pfn_pppfn_func)(struct _pfn **);
typedef pfn_t (*pfn_uint_pppfn_func)(unsigned int, struct _pfn **);
typedef void * (*pvoid_cpvoid_int_size_func)(const void *, int, size_t);
typedef void * (*pvoid_pvoid_cpvoid_size_func)(void *, const void *, size_t);
typedef void * (*pvoid_pvoid_int_size_func)(void *, int, size_t);
typedef size_t (*size_cpchar_func)(const char *);
typedef size_t (*size_cpchar_cpchar_func)(const char *, const char *);
typedef size_t (*size_cpchar_size_func)(const char *, size_t);
typedef void (*void_cchar_func)(const char);
typedef void (*void_cpchar__func)(const char *, ...);
typedef void (*void_cpchar_func)(const char *);
typedef void (*void_cpchar_valist_func)(const char *, va_list);
typedef void (*void_cuchar_func)(const unsigned char);
typedef void (*void_cuint_func)(const unsigned int);
typedef void (*void_pfn_ppfn_func)(pfn_t, struct _pfn *);
typedef void (*void_pfn_uint_ppfn_func)(pfn_t, unsigned int, struct _pfn *);
typedef void (*void_pl1pte_addr_addr_ptf_size_func)(l1pte *, addr_t, addr_t, union pt_flags, size_t);
typedef void (*void_pl1pte_addr_size_func)(l1pte *, addr_t, size_t);
typedef void (*void_pmtx_func)(struct _mtx *);
typedef void (*void_ppvlb_func)(struct _vlb **);
typedef void (*void_ppvlb_int_func)(struct _vlb **, int);
typedef void (*void_pspl_func)(struct _spl *);
typedef void (*void_splindex_func)(enum _spl_index);
typedef void (*void_void_func)(void);

struct base_vtable {
    int_int_func decc_isalnum;
    int_int_func decc_isalpha;
    int_int_func decc_iscntrl;
    int_int_func decc_isdigit;
    int_int_func decc_isgraph;
    int_int_func decc_islower;
    int_int_func decc_isprint;
    int_int_func decc_ispunct;
    int_int_func decc_isspace;
    int_int_func decc_isupper;
    int_int_func decc_isxdigit;
    int_int_func decc_isascii;
    int_int_func decc_toascii;

    int_int_func decc_tolower;
    int_int_func decc_toupper;

    int_int_func decc_abs;
    long_long_func decc_labs;

    pvoid_cpvoid_int_size_func decc_memchr;
    int_cpvoid_cpvoid_size_func decc_memcmp;
    pvoid_pvoid_cpvoid_size_func decc_memcpy;
    pvoid_pvoid_cpvoid_size_func decc_memmove;
    pvoid_pvoid_int_size_func decc_memset;

    pchar_pchar_cpchar_func decc_strcat;
    pchar_cpchar_int_func decc_strchr;
    int_cpchar_cpchar_func decc_strcmp;
    pchar_pchar_cpchar_func decc_strcpy;
    size_cpchar_cpchar_func decc_strcspn;
    size_cpchar_func decc_strlen;
    pchar_pchar_cpchar_size_func decc_strncat;
    int_cpchar_cpchar_size_func decc_strncmp;
    pchar_pchar_cpchar_size_func decc_strncpy;
    size_cpchar_size_func decc_strnlen;
    pchar_cpchar_int_func decc_strrchr;
    size_cpchar_cpchar_func decc_strspn;
    pchar_cpchar_cpchar_func decc_strstr;

    void_pl1pte_addr_addr_ptf_size_func boo_map;
    void_pl1pte_addr_size_func boo_unmap;

    void_void_func con_init_cty;
    void_cchar_func con_putchar;

    void_void_func exe_outblank;
    void_cuchar_func exe_outbyte;
    void_cchar_func exe_outchar;
    void_cpchar_func exe_outcstring;
    void_void_func exe_outcrlf;
    void_cuint_func exe_outhex;
    void_cpchar_func exe_outztring;

    int_pchan_ppccb_func ioc_ffchan;
    int_pchan_ppccb_func ioc_verifychan;

    void_cpchar__func exe_kprintf;
    void_cpchar_valist_func exe_kvprintf;

    int_ppvlb_pvlb_func exe_lal_insert_first;
    int_ppvlb_ppvlb_func exe_lal_remove_first;
    void_ppvlb_func exe_extendpool;
    void_ppvlb_func exe_flushlists;
    void_ppvlb_int_func exe_reclaimlists;

    int_ppvlb_psize_ppvoid_func exe_allocate;
    int_ppvlb_pvoid_size_func exe_deallocate;
    int_psize_ppvoid_func exe_alononpaged;
    int_pvoid_size_func exe_deanonpaged;
    int_pvoid_size_func exe_deanonpgsiz;

    pfn_pppfn_func mmg_alloc_pfn;
    pfn_pppfn_func mmg_alloc_zero_pfn;
    void_pfn_ppfn_func mmg_dalloc_pfn;
    void_pfn_ppfn_func mmg_dalloc_zero_pfn;
    void_pfn_uint_ppfn_func mmg_ins_pfnh;
    void_pfn_uint_ppfn_func mmg_ins_pfnt;
    void_pfn_uint_ppfn_func mmg_rem_pfn;
    pfn_uint_pppfn_func mmg_rem_pfnh;

    void_pmtx_func sch_lockr;
    void_pmtx_func sch_lockw;
    int_pmtx_func sch_locknowait;
    void_pmtx_func sch_unlock;

    void_splindex_func smp_acquire;
    void_pspl_func smp_acquirel;
    void_splindex_func smp_restore;
    void_pspl_func smp_restorel;
    void_splindex_func smp_release;
    void_pspl_func smp_releasel;
};

extern struct base_vtable base_image S0_NON_PAGED_DATA;

extern int decc$isalnum(int c) S0_NON_PAGED_CODE;
extern int decc$isalpha(int c) S0_NON_PAGED_CODE;
extern int decc$iscntrl(int c) S0_NON_PAGED_CODE;
extern int decc$isdigit(int c) S0_NON_PAGED_CODE;
extern int decc$isgraph(int c) S0_NON_PAGED_CODE;
extern int decc$islower(int c) S0_NON_PAGED_CODE;
extern int decc$isprint(int c) S0_NON_PAGED_CODE;
extern int decc$ispunct(int c) S0_NON_PAGED_CODE;
extern int decc$isspace(int c) S0_NON_PAGED_CODE;
extern int decc$isupper(int c) S0_NON_PAGED_CODE;
extern int decc$isxdigit(int c) S0_NON_PAGED_CODE;
extern int decc$isascii(int c) S0_NON_PAGED_CODE;
extern int decc$toascii(int c) S0_NON_PAGED_CODE;

extern int decc$tolower(int c) S0_NON_PAGED_CODE;
extern int decc$toupper(int c) S0_NON_PAGED_CODE;

extern int decc$abs (int i) S0_NON_PAGED_CODE;
extern long int decc$labs (long int i) S0_NON_PAGED_CODE;

extern void *decc$memchr(const void *s, int c, size_t n) S0_NON_PAGED_CODE;
extern int decc$memcmp(void* s1, void* s2, size_t n) S0_NON_PAGED_CODE;
extern void *decc$memcpy(void *dest, void *src, size_t n) S0_NON_PAGED_CODE;
extern void *decc$memmove(void *dest, void *src, size_t n) S0_NON_PAGED_CODE;
extern void *decc$memset(void *s, int c, size_t n) S0_NON_PAGED_CODE;

extern char *decc$strcat(char *dest, char *src) S0_NON_PAGED_CODE;
extern char *decc$strchr(const char *s, int c) S0_NON_PAGED_CODE;
extern int decc$strcmp(char *s1, char *s2) S0_NON_PAGED_CODE;
extern char *decc$strcpy(char *dest, char *src) S0_NON_PAGED_CODE;
extern size_t decc$strcspn(const char *s1, const char *s2) S0_NON_PAGED_CODE;
extern size_t decc$strlen(char *s) S0_NON_PAGED_CODE;
extern char *decc$strncat(char *dest, char *src, size_t n) S0_NON_PAGED_CODE;
extern int decc$strncmp( char *s1, char *s2, size_t n) S0_NON_PAGED_CODE;
extern char *decc$strncpy(char *dest, char *src, size_t n) S0_NON_PAGED_CODE;
extern size_t decc$strnlen(char *s, size_t n) S0_NON_PAGED_CODE;
extern char *decc$strrchr(const char *s, int c) S0_NON_PAGED_CODE;
extern size_t decc$strspn(const char *s1, const char *s2) S0_NON_PAGED_CODE;
extern char *decc$strstr(const char *s1, const char *s2) S0_NON_PAGED_CODE;

extern void boo$map(l1pte *p, addr_t paddr, addr_t vaddr, union pt_flags flags, size_t n) S0_NON_PAGED_CODE;
extern void boo$unmap(l1pte *p, addr_t vaddr, size_t n) S0_NON_PAGED_CODE;

extern void con$init_cty(void) S0_NON_PAGED_CODE;
extern void con$putchar(const char c) S0_NON_PAGED_CODE;

extern void exe$outblank(void) S0_NON_PAGED_CODE;
extern void exe$outbyte(const unsigned char c) S0_NON_PAGED_CODE;
extern void exe$outchar(const char c) S0_NON_PAGED_CODE;
extern void exe$outcrlf(void) S0_NON_PAGED_CODE;
extern void exe$outcstring(const char *s) S0_NON_PAGED_CODE;
extern void exe$outhex(const unsigned int x) S0_NON_PAGED_CODE;
extern void exe$outzstring(const char *s) S0_NON_PAGED_CODE;

extern int ioc$ffchan(channel_t *chan, struct _ccb **ccb) S0_NON_PAGED_CODE;
extern int ioc$verifychan(channel_t *chan, struct _ccb **ccb) S0_NON_PAGED_CODE;

extern void exe$kprintf(const char *format, ...) S0_NON_PAGED_CODE;
extern void exe$kvprintf(const char *format, va_list arg) S0_NON_PAGED_CODE;

extern void exe$lal_insert_first(struct _vlb ** head, struct _vlb *block) S0_NON_PAGED_CODE;
extern int exe$lal_remove_first(struct _vlb ** head, struct _vlb **block) S0_NON_PAGED_CODE;
extern void exe$extendpool(struct _vlb **head) S0_NON_PAGED_CODE;
extern void exe$flushlists(struct _vlb **head) S0_NON_PAGED_CODE;
extern void exe$reclaimlists(struct _vlb **head, int aggressive) S0_NON_PAGED_CODE;

extern int exe$allocate(struct _vlb **head, size_t *size, void **ptr) S0_NON_PAGED_CODE;
extern int exe$deallocate(struct _vlb **head, void *ptr, size_t size) S0_NON_PAGED_CODE;
extern int exe$alononpaged(size_t *size, void **ptr) S0_NON_PAGED_CODE;
extern int exe$deanonpaged(void *ptr, size_t size) S0_NON_PAGED_CODE;
extern int exe$deanonpgdsiz(void *ptr, size_t size) S0_NON_PAGED_CODE;

extern pfn_t mmg$alloc_pfn(struct _pfn **pfndbe) S0_NON_PAGED_CODE;
extern pfn_t mmg$alloc_zero_pfn(struct _pfn **pfndbe) S0_NON_PAGED_CODE;
extern void mmg$dalloc_pfn(pfn_t n, struct _pfn *pfndbe) S0_NON_PAGED_CODE;
extern void mmg$dalloc_zero_pfn(pfn_t n, struct _pfn *pfndbe) S0_NON_PAGED_CODE;
extern void mmg$ins_pfnh(pfn_t n, unsigned int lst, struct _pfn *pfndbe) S0_NON_PAGED_CODE;
extern void mmg$ins_pfnt(pfn_t n, unsigned int lst, struct _pfn *pfndbe) S0_NON_PAGED_CODE;
extern void mmg$rem_pfn(pfn_t n, unsigned int lst, struct _pfn *pfndbe) S0_NON_PAGED_CODE;
extern pfn_t mmg$rem_pfnh(unsigned int lst, struct _pfn **pfndbe) S0_NON_PAGED_CODE;

extern void sch$lockr(struct _mtx *mutex) S0_NON_PAGED_CODE;
extern void sch$lockw(struct _mtx *mutex) S0_NON_PAGED_CODE;
extern int sch$lockwnowait(struct _mtx *mutex) S0_NON_PAGED_CODE;
extern void sch$unlock(struct _mtx *mutex) S0_NON_PAGED_CODE;

extern void smp$acquire(enum _spl_index index) S0_NON_PAGED_CODE;
extern void smp$acquirel(struct _spl *spinlock) S0_NON_PAGED_CODE;
extern void smp$restore(enum _spl_index index) S0_NON_PAGED_CODE;
extern void smp$restorel(struct _spl *spinlock) S0_NON_PAGED_CODE;
extern void smp$release(enum _spl_index index) S0_NON_PAGED_CODE;
extern void smp$releasel(struct _spl *spinlock) S0_NON_PAGED_CODE;

extern void ioc$return(void) S0_NON_PAGED_CODE;

extern void exe$load_error(void) S0_NON_PAGED_CODE;

#endif /* _SYSTEM_ROUTINES_H */
