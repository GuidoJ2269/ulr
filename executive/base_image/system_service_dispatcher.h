/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSTEM_SERVICE_DISPATCHER_H
#define _SYSTEM_SERVICE_DISPATCHER_H

#include <cpu.h>

#include "sections.h"

/* System Service Exit Type */
enum system_service_exit_type
{
    SSET_NULL,
    SSET_SYNCH_RMS_STALL,
    SSET_SYNCH_RMS_WAIT,
    SSET_SYNCH_ASSIGN_EXIT
};

/* System Service Mode */
enum system_service_mode
{
    SSM_KERNEL,
    SSM_EXECUTIVE
};

/* Change Mode Dispatch Table Entry */
struct chmdte
{
    enum system_service_exit_type   exit_type_code;
    unsigned int                    arg_count;
    void                           *service_routine_addr;
};

#define SSI_EXE_ASSIGN  0x00
#define SSI_EXE_DASSGN  0x01
#define SSI_EXE_QIO     0x02
#define SSI_CON_WRITE   0x7f
#define SSI_MAX         0xff

#define DISPATCH_VECTOR_SIZE (SSI_MAX + 1)

extern struct chmdte cmod$ar_kernel_dispatch_vector[DISPATCH_VECTOR_SIZE];
extern struct chmdte cmod$ar_exec_dispatch_vector[DISPATCH_VECTOR_SIZE];

extern int exe$cmodkrnl(int service_id, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6) S0_NON_PAGED_CODE;
extern int exe$cmodexec(int service_id, reg_t p1, reg_t p2, reg_t p3, reg_t p4, reg_t p5, reg_t p6) S0_NON_PAGED_CODE;

#endif /* _SYSTEM_SERVICE_DISPATCHER_H */
