/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <system_data_cells.h>

struct _pfn * pfn$al_head[NUM_PFN_LISTS] = {0};
struct _pfn * pfn$al_tail[NUM_PFN_LISTS] = {0};
page_count_t pfn$al_count[NUM_PFN_LISTS] = {0};
page_count_t pfn$gl_phypgcnt = 0;

struct _ldrimg * ldr$gq_image_list_head = 0;
struct _ldrimg * ldr$gq_image_list_tail = 0;

char sys$gq_version[VERSION_LENGTH] = "V0.0.1DEV";

struct _mtx ioc$gl_mutex = {0};

struct _vlb * exe$gl_nonpaged = 0;
struct _vlb * exe$gl_paged = 0;

struct _spl *  smp$ar_spnlkvec = 0;
unsigned short smp$gw_spnlkcnt = 0;

unsigned short smp$gw_cpu_cnt = 0;
struct _cpu    smp$gl_cpu_data[MAX_NUM_CPUS] = {{CPU$C_INIT, IPL$_POWER, 1, {0}, 0, 0, 0, 0, 0, {0}, 0}};
unsigned short smp$gw_bus_cnt = 0;
unsigned short smp$gw_ioapic_cnt = 0;

struct _ddb * opa$ar_ddb = 0;
struct _ucb * opa$ar_ucb0 = 0;
struct _dpt * opa$ar_dpt = 0;
