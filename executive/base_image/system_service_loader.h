/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSTEM_SERVICE_LOADER_H
#define _SYSTEM_SERVICE_LOADER_H

#include <system_service_dispatcher.h>

extern int exe$connect_services(enum system_service_mode mode, int service_id, enum system_service_exit_type exit_type_code,
                                unsigned int arg_count, void *func_addr) S0_NON_PAGED_CODE;

#endif /* _SYSTEM_SERVICE_LOADER_H */
