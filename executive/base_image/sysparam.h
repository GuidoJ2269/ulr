/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _SYSPARAM_H
#define _SYSPARAM_H

#include <stddef.h>

#include <ccbdef.h>
#include <pfndef.h>

#include "boostate.h"
#include "sections.h"

#define CHANNELCNT 256
#define PAGEDYN 32 * 4096
#define NPAGEDYN 16 * 4096
#define NPAGEVIR 32 * 4096

#define CPUVENDOR_LEN 16

extern union pgd_entry *    mmg$gl_syspgd                       S0_SYSPARAM;

extern void *               mmg$gl_npagedyn                     S0_SYSPARAM;
extern void *               mmg$gl_npagnext                     S0_SYSPARAM;
extern void *               mmg$gl_pagedyn                      S0_SYSPARAM;

extern pfn_t                mmg$gl_maxpfn                       S0_SYSPARAM;
extern pfn_t                mmg$gl_minpfn                       S0_SYSPARAM;
extern size_t               mmg$gl_maxmem                       S0_SYSPARAM;
extern struct _pmap *       mmg$gl_free_no_pfn_db_list          S0_SYSPARAM;

extern union boostate       exe$gl_state                        S0_SYSPARAM;

extern char                 exe$gb_cpuvendor[CPUVENDOR_LEN]     S0_SYSPARAM;
extern uint16_t             exe$gw_cpufamily                    S0_SYSPARAM;
extern uint8_t              exe$gb_cpumodel                     S0_SYSPARAM;
extern uint8_t              exe$gb_cpustepping                  S0_SYSPARAM;

extern size_t               sgn$gl_npagedyn                     S0_SYSPARAM;
extern size_t               sgn$gl_npagevir                     S0_SYSPARAM;
extern size_t               sgn$gl_pagedyn                      S0_SYSPARAM;
extern channel_t            sgn$gw_pchancnt                     S0_SYSPARAM;

extern struct _pfn*         pfn$a_base                          S0_SYSPARAM;

#endif /* _SYSPARAM_H */
