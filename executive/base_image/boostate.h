/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _BOOSTATE_H
#define _BOOSTATE_H

#include <stdint.h>

/* States of system initialization */

#define BOOSTATE$M_SYSBOOT        0x0001;
#define BOOSTATE$M_INIT           0x0002;
#define BOOSTATE$M_SWAPPER        0x0004;
#define BOOSTATE$M_SYSINIT        0x0008;
#define BOOSTATE$M_STARTUP        0x0010;
#define BOOSTATE$M_MAPPED         0x0020;
#define BOOSTATE$M_PFN_INIT       0x0040;
#define BOOSTATE$M_POOL_INIT      0x0080;
#define BOOSTATE$M_XQP            0x0100;
#define BOOSTATE$M_RMS            0x0200;
#define BOOSTATE$M_CONSOLE        0x0400;
#define BOOSTATE$M_OBJREG         0x0800;
#define BOOSTATE$M_AUDITING       0x1000;
#define BOOSTATE$M_OBJECT_SERVICE 0x2000;

#define BOOSTATE$S_BOOSTATEDEF 4;

union boostate
{
    uint32_t gl_state;
    struct
    {
        uint32_t v_sysboot: 1;
        uint32_t v_init: 1;
        uint32_t v_swapper: 1;
        uint32_t v_sysinit: 1;
        uint32_t v_startup: 1;
        uint32_t v_mapped: 1;
        uint32_t v_pfn_init: 1;
        uint32_t v_pool_init: 1;
        uint32_t v_xqp: 1;
        uint32_t v_rms: 1;
        uint32_t v_console: 1;
        uint32_t v_objreg: 1;
        uint32_t v_auditing: 1;
        uint32_t v_object_service: 1;
    };
};

#endif /* _BOOSTATE_H */
