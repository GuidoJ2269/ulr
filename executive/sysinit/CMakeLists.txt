# the kernel target
set(sysinit_SRCS
    ${CRTL_DIR}/src/crti.S
    main.c
    ${CRTL_DIR}/src/crtn.S)

set(sysinit_LAYOUT ${CONFIG_DIR}/${ARCH}/sysinit.ld)

add_executable(sysinit ${sysinit_SRCS})

set_target_properties(sysinit PROPERTIES LINK_FLAGS "-T ${sysinit_LAYOUT} -z common-page-size=16 -z max-page-size=4096")

target_link_libraries(sysinit crtl gcc -Wl,-R base_image)

# install
install(TARGETS sysinit DESTINATION ${PROJECT_SOURCE_DIR}/iso/vms\$common/sysexe)
