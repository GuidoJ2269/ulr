/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <mmg_routines.h>
#include <mutex.h>
#include <sysldr.h>
#include <sysparam.h>
#include <system_routines.h>
#include <vms_macros.h>

/* internal function prototypes */
void exe$init(void);
void dump_vlbs(struct _vlb *head);

void dump_vlbs(struct _vlb *ptr)
{
    while (ptr != 0)
    {
        exe$kprintf("%x %x\t", ptr, ptr->vlb$l_size);
        ptr = ptr->vlb$ps_next;
    }
    exe$kprintf("\n");
}

void exe$init(void)
{
    ldr$init_all();
    con$init_cty();

    exe$gl_nonpaged = (struct _vlb *)mmg$gl_npagedyn;
    exe$gl_nonpaged->vlb$ps_next = 0;
    exe$gl_nonpaged->vlb$l_size = (uint64_t)(mmg$gl_npagnext - mmg$gl_npagedyn);
    exe$gl_paged = (struct _vlb *)mmg$gl_pagedyn;

    exe$kprintf("Initializing ULR %s ...\n", sys$gq_version);
    exe$kprintf("CPU: %s %d %d %d\n", exe$gb_cpuvendor, exe$gw_cpufamily, exe$gb_cpumodel, exe$gb_cpustepping);
    exe$kprintf("RAM: %dMiB (%dMiB free)\n", mmg$gl_maxmem >> 20, (pfn$al_count[PFN_FREE_LIST] * PAGE_SIZE) >> 20);
    exe$kprintf("  PFN database\t\t0x%x\n  Non-paged data\t0x%x\n  Paged data\t\t0x%x\n", pfn$a_base, exe$gl_nonpaged, exe$gl_paged);

    exe$kprintf("\n");
    dump_vlbs(exe$gl_nonpaged);

    size_t sz1 = 0x1000;
    void *ptr1 = 0;
    exe$alononpaged(&sz1, &ptr1);
    size_t sz2 = 0x1000;
    void *ptr2 = 0;
    exe$alononpaged(&sz2, &ptr2);
    size_t sz3 = 0x1000;
    void *ptr3 = 0;
    exe$alononpaged(&sz3, &ptr3);
    dump_vlbs(exe$gl_nonpaged);

    exe$deanonpaged(ptr1, sz1);
    exe$deanonpaged(ptr2, sz2);
    exe$deanonpaged(ptr3, sz3);
    dump_vlbs(exe$gl_nonpaged);

    size_t sz4 = 0xd800;
    void *ptr4 = 0;
    exe$alononpaged(&sz4, &ptr4);
    dump_vlbs(exe$gl_nonpaged);

    sz1 = 0x10;
    ptr1 = 0;
    exe$alononpaged(&sz1, &ptr1);
    sz2 = 0x100;
    ptr2 = 0;
    exe$alononpaged(&sz2, &ptr2);
    sz3 = 0x200;
    ptr3 = 0;
    exe$alononpaged(&sz3, &ptr3);
    dump_vlbs(exe$gl_nonpaged);

    exe$deanonpaged(ptr1, sz1);
    exe$deanonpaged(ptr2, sz2);
    exe$deanonpaged(ptr3, sz3);
    exe$deanonpaged(ptr4, sz4);
    dump_vlbs(exe$gl_nonpaged);

    exe$flushlists(&exe$gl_nonpaged);
    dump_vlbs(exe$gl_nonpaged);

    reset_ipl();
}

void main(void)
{
    exe$init();
    struct _mtx mutex;
    sch$lockr(&mutex);
    sch$unlock(&mutex);
    reset_ipl();

    /* throw a general protection fault as ultimate test */
    //exe$kprintf("\nException test:");
    //exe$kprintf("x=%d\n", *((char *)0x0123456789abcdef));
    for (;;)
    {
        /* nothing ... */
    }
}
