#!/bin/bash
base=0xffffffff80000000
offset=0

# loadable executive images and drivers
# TODO: this array/list definition works only in bash
modules=(
base_image
cpu_routines
system_primitives
system_synchronization
io_routines
sys_vm
opdriver
)

for mod in ${modules[@]}
do
    size=0
    binary=`find ./executive -name $mod.exe`
    for m in `objdump -p $binary | awk '$3=="memsz" {print $4}'`
    do
        m=$(($m + 4095))
        m=$(($m & 0xfffffffffffff000))
        size=$(($size + $m))
    done
    addr=`printf "%x" $(($base + $offset))`
    ldscript=`find ./executive -name $mod.ld`
    perl -i -pe "undef $/; s/(\/\*\ base\ address\ \*\/\s+?\.\ =\ 0x).*?;/\1$addr;/" $ldscript
    printf "%-32s%s\n" $mod $addr
    offset=$(($offset + size))
done
