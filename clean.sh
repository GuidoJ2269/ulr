#!/bin/sh
rm -f CMakeCache.txt CopyOfCMakeCache.txt
rm -f `find . -name Makefile`
rm -f `find . -name cmake_install.cmake`
rm -f install_manifest.txt
rm -rf `find . -name CMakeFiles`
rm -f `find . -name *.exe`
rm -f `find . -name *.lib`
rm -f `find . -name *.obj`
rm -f `find . -name *.o`
rm -rf iso *.iso
