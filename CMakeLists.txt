project(ULR)

cmake_minimum_required(VERSION 2.8)

enable_language(ASM)

set(CMAKE_EXECUTABLE_SUFFIX ".exe")

set(CMAKE_SHARED_LIBRARY_PREFIX "")
set(CMAKE_SHARED_LIBRARY_SUFFIX ".exe")

set(CMAKE_STATIC_LIBRARY_PREFIX "")
set(CMAKE_STATIC_LIBRARY_SUFFIX ".lib")

set(ARCH_DIR "${PROJECT_SOURCE_DIR}/executive/arch")
set(BASE_IMAGE_DIR "${PROJECT_SOURCE_DIR}/executive/base_image")
set(COMMON_DIR "${PROJECT_SOURCE_DIR}/executive/common")
set(CONFIG_DIR "${PROJECT_SOURCE_DIR}/executive/config")
set(CPU_ROUTINES_DIR "${PROJECT_SOURCE_DIR}/executive/cpu_routines")
set(CRTL_DIR "${PROJECT_SOURCE_DIR}/libs/crtl")
set(IO_ROUTINES_DIR "${PROJECT_SOURCE_DIR}/executive/io_routines")
set(LIB_DIR "${PROJECT_SOURCE_DIR}/executive/lib")
set(LIBRTL_DIR "${PROJECT_SOURCE_DIR}/libs/librtl")
set(OPDRIVER_DIR "${PROJECT_SOURCE_DIR}/executive/opdriver")
set(PRIVATE_DIR "${PROJECT_SOURCE_DIR}/executive/private")
set(SHELL_DIR "${PROJECT_SOURCE_DIR}/executive/shell")
set(STARLET_DIR "${PROJECT_SOURCE_DIR}/libs/starlet")
set(SYSBOOT_DIR "${PROJECT_SOURCE_DIR}/executive/sysboot")
set(SYSLDR_DIR "${PROJECT_SOURCE_DIR}/executive/sysldr")
set(SYSTEM_PRIMITIVES_DIR "${PROJECT_SOURCE_DIR}/executive/system_primitives")
set(SYSTEM_SYNCHRONIZATION_DIR "${PROJECT_SOURCE_DIR}/executive/system_synchronization")
set(SYS_VM_DIR "${PROJECT_SOURCE_DIR}/executive/sys_vm")

add_subdirectory(apps)
add_subdirectory(executive)
add_subdirectory(libs)

add_subdirectory(final)
