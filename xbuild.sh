#!/bin/sh

# build versions
XBUILD_BINUTILS_VERSION=2.30
XBUILD_GCC_VERSION=7.3.0

# setup environment
export WORKING_DIR="../crosscompiler"
export TARGET=x86_64-elf
export PREFIX="$HOME/opt/cross"
export PATH="$PREFIX/bin:$PATH"

# get source packages
mkdir -p $WORKING_DIR
cd $WORKING_DIR
rm -rf *
wget http://ftp.gnu.org/gnu/binutils/binutils-${XBUILD_BINUTILS_VERSION}.tar.xz
tar -xJf binutils-${XBUILD_BINUTILS_VERSION}.tar.xz
wget http://ftp.gnu.org/gnu/gcc/gcc-${XBUILD_GCC_VERSION}/gcc-${XBUILD_GCC_VERSION}.tar.xz
tar -xJf gcc-${XBUILD_GCC_VERSION}.tar.xz

# build binutils
cd $HOME/workspace/crosscompiler
mkdir build-binutils
cd build-binutils
../binutils-${XBUILD_BINUTILS_VERSION}/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install
which -- $TARGET-as || echo $TARGET-as is not in the PATH
which -- $TARGET-ld || echo $TARGET-ld is not in the PATH

# build gcc
cd $HOME/workspace/crosscompiler
mkdir build-gcc
cd build-gcc
../gcc-${XBUILD_GCC_VERSION}/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
which -- $TARGET-gcc || echo $TARGET-gcc is not in the PATH

# show cross compiler version
$PREFIX/bin/$TARGET-as --version
$PREFIX/bin/$TARGET-gcc --version
$PREFIX/bin/$TARGET-ld --version
