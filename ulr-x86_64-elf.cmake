set(CMAKE_SYSTEM_NAME ULR)
set(CMAKE_SYSTEM_PROCESSOR x86_64)
set(CMAKE_SYSTEM_VERSION 0.0.1)

set(CMAKE_C_COMPILER x86_64-elf-gcc)
set(CMAKE_CXX_COMPILER x86_64-elf-g++)
#set(CMAKE_ASM_COMPILER x86_64-elf-as)
#set(CMAKE_LINKER x86_64-elf-ld)
#set(CMAKE_AR x86_64-elf-ar)
#set(CMAKE_NM x86_64-elf-nm)
#set(CMAKE_OBJCOPY x86_64-elf-objcopy)
#set(CMAKE_OBJDUMP x86_64-elf-objdump)
#set(CMAKE_RANLIB x86_64-elf-ranlib)
#set(CMAKE_STRIP x86_64-elf-strip)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_FIND_ROOT_PATH /home/guido/opt/cross/bin)
