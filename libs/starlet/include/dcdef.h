/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _DCDEF_H
#define _DCDEF_H

/* Device Class */
enum _devclass
{
    DC$_DISK           = 0x01,   /* Disk device */
    DC$_TAPE           = 0x02,   /* Tape device */
    DC$_SCOM           = 0x20,   /* Synchronous communication, network device */
    DC$_CARD           = 0x41,   /* Card reader device */
    DC$_TERM           = 0x42,   /* Terminal device */
    DC$_LP             = 0x43,   /* Line Printer */
    DC$_WORKSTATION    = 0x46,   /* Workstations */
    DC$_REALTIME       = 0x60,   /* Real time device */
    DC$_DECVOICE       = 0x61,   /* DECvoice Products */
    DC$_AUDIO          = 0x62,   /* Audio */
    DC$_VIDEO          = 0x63,   /* Video */
    DC$_BUS            = 0x80,   /* Buses, Adapters */
    DC$_MAILBOX        = 0xa0,   /* Mailbox */
    DC$_REMCSL_STORAGE = 0xaa,   /* Remote Console Storage */
    DC$_MISC           = 0xc8    /* Miscellaneous */
};

#endif /* _DCDEF_H */
