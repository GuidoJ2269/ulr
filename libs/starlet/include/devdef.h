/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#ifndef _DEVDEF_H
#define _DEVDEF_H

/* Device Characteristics */
union _devchar
{
    uint64_t dev$q_devchar;
    struct
    {
        uint64_t dev$v_rec: 1;      /* record-oriented device (bit 0) */
        uint64_t dev$v_ccl: 1;      /* carriage control device (bit 1) */
        uint64_t dev$v_trm: 1;      /* terminal device (bit 2) */
        uint64_t dev$v_dir: 1;      /* directory-structured device (bit 3) */
        uint64_t dev$v_sdi: 1;      /* single directory-structured device (bit 4) */
        uint64_t dev$v_sod: 1;      /* sequential block-oriented device (for example, tape) (bit 5) */
        uint64_t dev$v_spl: 1;      /* device is spooled (bit 6) */
        uint64_t dev$v_opr: 1;      /* operator device (bit 7) */
        uint64_t dev$v_rct: 1;      /* device contains rct (bit 8) */
        uint64_t dev$v_res1: 4;     /* reserved (bits 9-12 (9-c hex)) */
        uint64_t dev$v_net: 1;      /* network device (bit 13 (d hex)) */
        uint64_t dev$v_fod: 1;      /* file-oriented device (bit 14 (e hex)) */
        uint64_t dev$v_dua: 1;      /* dual-ported device (bit 15 (f hex)) */
        uint64_t dev$v_shr: 1;      /* shareable device (used by more than one program simultaneously) (bit 16 (10 hex)) */
        uint64_t dev$v_gen: 1;      /* generic device (bit 17 (11 hex)) */
        uint64_t dev$v_avl: 1;      /* device available for use (bit 18 (12 hex)) */
        uint64_t dev$v_mnt: 1;      /* device mounted (bit 19 (13 hex)) */
        uint64_t dev$v_mbx: 1;      /* mailbox device (bit 20 (14 hex)) */
        uint64_t dev$v_dmt: 1;      /* device marked for dismounting (bit 21 (15 hex)) */
        uint64_t dev$v_elg: 1;      /* error logging enabled (bit 22 (16 hex)) */
        uint64_t dev$v_all: 1;      /* device allocated (bit 23 (17 hex)) */
        uint64_t dev$v_por: 1;      /* device mounted as foreign (bit 24 (18 hex)) */
        uint64_t dev$v_swl: 1;      /* device software write locked (bit 25 (19 hex)) */
        uint64_t dev$v_idv: 1;      /* device capable of providing input (bit 26 (1a hex)) */
        uint64_t dev$v_odv: 1;      /* device capable of providing output (bit 27 (1b hex)) */
        uint64_t dev$v_rnd: 1;      /* device allowing random access (bit 28 (1c hex)) */
        uint64_t dev$v_rtm: 1;      /* real-time device (bit 29 (1d hex)) */
        uint64_t dev$v_rck: 1;      /* read-checking enabled (bit 30 (1e hex)) */
        uint64_t dev$v_wck: 1;      /* write-checking enabled (bit 31 (1f hex)) */
        uint64_t dev$v_clu: 1;      /* device available cluster-wide (bit 32 (20 hex)) */
        uint64_t dev$v_det: 1;      /* detached terminal (bit 33 (21 hex)) */
        uint64_t dev$v_rtt: 1;      /* remote-terminal ucb extension (bit 34 (22 hex)) */
        uint64_t dev$v_cdp: 1;      /* dual-path device with two ucbs (bit 35 (23 hex)) */
        uint64_t dev$v_2p: 1;       /* two paths known to device (bit 36 (24 hex)) */
        uint64_t dev$v_mscp: 1;     /* device accessed using mscp (bit 37 (25 hex)) */
        uint64_t dev$v_ssm: 1;      /* shadow set member (bit 38 (26 hex)) */
        uint64_t dev$v_srv: 1;      /* served by mscp server (bit 39 (27 hex)) */
        uint64_t dev$v_red: 1;      /* redirected terminal (bit 40 (28 hex)) */
        uint64_t dev$v_nnm: 1;      /* extended device naming (bit 41 (29 hex)) */
        uint64_t dev$v_wbc: 1;      /* device supports write-back caching (bit 42 (2a hex)) */
        uint64_t dev$v_wtc: 1;      /* device supports write-through caching (bit 43 (2b hex)) */
        uint64_t dev$v_hoc: 1;      /* device supports host caching (bit 44 (2c hex)) */
        uint64_t dev$v_loc: 1;      /* device accessible via local (non-emulated) controller (bit 45 (2d hex)) */
        uint64_t dev$v_dfs: 1;      /* device is dfs-served (bit 46 (2e hex)) */
        uint64_t dev$v_dap: 1;      /* device is dap accessed (bit 47 (2f hex)) */
        uint64_t dev$v_nlt: 1;      /* device is not-last-track (i.e. it has no bad block information on its last track) (bit 48 (30 hex)) */
        uint64_t dev$v_sex: 1;      /* device (tape) supports serious exception handling (bit 49 (31 hex)) */
        uint64_t dev$v_shd: 1;      /* device is a member of a host based shadow set (bit 50 (32 hex)) */
        uint64_t dev$v_vrt: 1;      /* device is a shadow set virtual unit (bit 51 (33 hex)) */
        uint64_t dev$v_ldr: 1;      /* loader present (tapes) (bit 52 (34 hex)) */
        uint64_t dev$v_nolb: 1;     /* device ignores server load balancing requests (bit 53 (35 hex)) */
        uint64_t dev$v_noclu: 1;    /* device will never be available cluster-wide (bit 54 (36 hex)) */
        uint64_t dev$v_vmem: 1;     /* virtual member of a constituent set (bit 55 (37 hex)) */
        uint64_t dev$v_scsi: 1;     /* scsi device (bit 56 (38 hex)) */
        uint64_t dev$v_wlg: 1;      /* ... (bit 57 (39 hex)) */
        uint64_t dev$v_nofe: 1;     /* ... (bit 58 (3a hex)) */
        uint64_t dev$v_res2: 1;     /* reserved (bit 59 (3b hex)) */
        uint64_t dev$v_cramio: 1;   /* device supports CRAM IO (bit 60 (3c hex)) */
        uint64_t dev$v_dtn: 1;      /* ... (bit 61 (3d hex)) */
        uint64_t dev$v_res3: 1;     /* reserved (bit 62 (3e hex)) */
        uint64_t dev$v_pool_mbr: 1; /* ... (bit 63 (3f hex)) */
    };
};

#define     DEV$M_REC               0x0000000000000001
#define     DEV$M_CCL               0x0000000000000002
#define     DEV$M_TRM               0x0000000000000004
#define     DEV$M_DIR               0x0000000000000008
#define     DEV$M_SDI               0x0000000000000010
#define     DEV$M_SQD               0x0000000000000020
#define     DEV$M_SPL               0x0000000000000040
#define     DEV$M_OPR               0x0000000000000080
#define     DEV$M_RCT               0x0000000000000100
#define     DEV$M_QSVD              0x0000000000000200
#define     DEV$M_QSVBL             0x0000000000000400
#define     DEV$M_MPDEV_SECONDARY   0x0000000000000800
#define     DEV$M_MPDEV_MEMBER      0x0000000000001000
#define     DEV$M_NET               0x0000000000002000
#define     DEV$M_FOD               0x0000000000004000
#define     DEV$M_DUA               0x0000000000008000
#define     DEV$M_SHR               0x0000000000010000
#define     DEV$M_GEN               0x0000000000020000
#define     DEV$M_AVL               0x0000000000040000
#define     DEV$M_MNT               0x0000000000080000
#define     DEV$M_MBX               0x0000000000100000
#define     DEV$M_DMT               0x0000000000200000
#define     DEV$M_ELG               0x0000000000400000
#define     DEV$M_ALL               0x0000000000800000
#define     DEV$M_FOR               0x0000000001000000
#define     DEV$M_SWL               0x0000000002000000
#define     DEV$M_IDV               0x0000000004000000
#define     DEV$M_ODV               0x0000000008000000
#define     DEV$M_RND               0x0000000010000000
#define     DEV$M_RTM               0x0000000020000000
#define     DEV$M_RCK               0x0000000040000000
#define     DEV$M_WCK               0x0000000080000000
#define     DEV$M_CLU               0x0000000100000000
#define     DEV$M_DET               0x0000000200000000
#define     DEV$M_RTT               0x0000000400000000
#define     DEV$M_CDP               0x0000000800000000
#define     DEV$M_2P                0x0000001000000000
#define     DEV$M_MSCP              0x0000002000000000
#define     DEV$M_SSM               0x0000004000000000
#define     DEV$M_SRV               0x0000008000000000
#define     DEV$M_RED               0x0000010000000000
#define     DEV$M_NNM               0x0000020000000000
#define     DEV$M_WBC               0x0000040000000000
#define     DEV$M_WTC               0x0000080000000000
#define     DEV$M_HOC               0x0000100000000000
#define     DEV$M_LOC               0x0000200000000000
#define     DEV$M_DFS               0x0000400000000000
#define     DEV$M_DAP               0x0000800000000000
#define     DEV$M_NLT               0x0001000000000000
#define     DEV$M_SEX               0x0002000000000000
#define     DEV$M_SHD               0x0004000000000000
#define     DEV$M_VRT               0x0008000000000000
#define     DEV$M_LDR               0x0010000000000000
#define     DEV$M_NOLB              0x0020000000000000
#define     DEV$M_NOCLU             0x0040000000000000
#define     DEV$M_VMEM              0x0080000000000000
#define     DEV$M_SCSI              0x0100000000000000
#define     DEV$M_WLG               0x0200000000000000
#define     DEV$M_NOFE              0x0400000000000000
#define     DEV$M_RESERVED2         0x0800000000000000
#define     DEV$M_CRAMIO            0x1000000000000000
#define     DEV$M_DTN               0x2000000000000000
#define     DEV$M_RESERVED3         0x4000000000000000
#define     DEV$M_POOL_MBR          0x8000000000000000

#endif /* _DEVDEF_H */
