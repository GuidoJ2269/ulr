/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Copyright (C) 2016-2018 Guido de Jong <guidoj2269@gmail.com>
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int skip_atoi(const char **s);
unsigned int do_div(long *n, unsigned int base);
char *number(char *str, long num, unsigned base, int size, int precision, int type);

int skip_atoi(const char **s)
{
    int i = 0;
    while (isdigit(**s))
    {
        i = i*10 + *((*s)++) - '0';
    }
    return i;
}

#define ZEROPAD 1       /* pad with zero */
#define SIGN    2       /* unsigned/signed long */
#define PLUS    4       /* show plus */
#define SPACE   8       /* space if plus */
#define LEFT    16      /* left justified */
#define SPECIAL 32      /* 0x */
#define LARGE   64      /* use 'ABCDEF' instead of 'abcdef' */

unsigned int do_div(long *n, unsigned int base)
{
    unsigned int res;
    res = (unsigned int)((*n) % base);
    *n = (*n) / base;
    return res;
}

char *number(char *str, long num, unsigned int base, int size, int precision, int type)
{
    char c, sign, tmp[66];
    const char *digits = "0123456789abcdefghijklmnopqrstuvwxyz";
    int i;

    if ((base < 2) || (base > 36))
    {
        return 0;
    }
    if (type & LARGE)
    {
        digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    if (type & LEFT)
    {
        type &= ~ZEROPAD;
    }
    c = (type & ZEROPAD) ? '0' : ' ';
    sign = 0;
    if (type & SIGN)
    {
        if (num < 0)
        {
            sign = '-';
            num = -num;
            size--;
        }
        else if (type & PLUS)
        {
            sign = '+';
            size--;
        }
        else if (type & SPACE)
        {
            sign = ' ';
            size--;
        }
    }
    if (type & SPECIAL)
    {
        if (base == 16)
        {
            size -= 2;
        }
        else if (base == 8)
        {
            size--;
        }
    }
    i = 0;
    if (num == 0)
    {
        tmp[i++] = '0';
    }
    else
    {
        while (num != 0)
        {
            tmp[i++] = digits[do_div(&num, base)];
        }
    }

    if (i > precision)
    {
        precision = i;
    }
    size -= precision;
    if (!(type & (ZEROPAD + LEFT)))
    {
        while (size-- > 0)
        {
            *str++ = ' ';
        }
    }
    if (sign)
    {
        *str++ = sign;
    }
    if (type & SPECIAL) {
        if (base == 8)
        {
            *str++ = '0';
        }
        else if (base == 16)
        {
            *str++ = '0';
            *str++ = digits[33];
        }
    }
    if (!(type & LEFT))
    {
        while (size-- > 0)
        {
            *str++ = c;
        }
    }
    while (i < precision--)
    {
        *str++ = '0';
    }
    while (i-- > 0)
    {
        *str++ = tmp[i];
    }
    while (size-- > 0)
    {
        *str++ = ' ';
    }
    return str;
}

int vsprintf(char *buf, const char *fmt, va_list args)
{
    int len;
    long num;
    int i;
    unsigned int base;
    char *str;
    const char *s;

    int flags;          /* flags to number() */

    int field_width;    /* width of output field */
    int precision;      /* min. # of digits for integers; max number of chars for from string */
    int qualifier;      /* 'h', 'l', or 'L' for integer fields */

    for (str = buf ; *fmt ; ++fmt)
    {
        if (*fmt != '%')
        {
            *str++ = *fmt;
            continue;
        }

        /* process flags */
        flags = 0;
        repeat:
            ++fmt;      /* this also skips first '%' */
            switch (*fmt)
            {
                case '-': flags |= LEFT; goto repeat;
                case '+': flags |= PLUS; goto repeat;
                case ' ': flags |= SPACE; goto repeat;
                case '#': flags |= SPECIAL; goto repeat;
                case '0': flags |= ZEROPAD; goto repeat;
            }

        /* get field width */
        field_width = -1;
        if (isdigit(*fmt))
        {
            field_width = skip_atoi(&fmt);
        }
        else if (*fmt == '*')
        {
            ++fmt;
            /* it's the next argument */
            field_width = va_arg(args, int);
            if (field_width < 0)
            {
                field_width = -field_width;
                flags |= LEFT;
            }
        }

        /* get the precision */
        precision = -1;
        if (*fmt == '.')
        {
            ++fmt;
            if (isdigit(*fmt))
            {
                precision = skip_atoi(&fmt);
            }
            else if (*fmt == '*')
            {
                ++fmt;
                /* it's the next argument */
                precision = va_arg(args, int);
            }
            if (precision < 0)
            {
                precision = 0;
            }
        }

        /* get the conversion qualifier */
        qualifier = -1;
        if ((*fmt == 'h') || (*fmt == 'l') || (*fmt == 'L'))
        {
            qualifier = *fmt;
            ++fmt;
        }

        /* default base */
        base = 10;

        switch (*fmt)
        {
            case 'c':
                if (!(flags & LEFT))
                {
                    while (--field_width > 0)
                    {
                        *str++ = ' ';
                    }
                }
                *str++ = (char)va_arg(args, int);
                while (--field_width > 0)
                {
                    *str++ = ' ';
                }
                continue;

            case 's':
                s = va_arg(args, char *);
                if (!s)
                {
                    s = "<NULL>";
                }

                len = (int)strnlen(s, (unsigned int)precision);

                if (!(flags & LEFT))
                {
                    while (len < field_width--)
                    {
                        *str++ = ' ';
                    }
                }
                for (i = 0; i < len; ++i)
                {
                    *str++ = *s++;
                }
                while (len < field_width--)
                {
                    *str++ = ' ';
                }
                continue;

            case 'p':
                if (field_width == -1)
                {
                    field_width = 2 * sizeof(void *);
                    flags |= ZEROPAD;
                }
                str[0] = '0';
                str[1] = 'x';
                str = number(str + 2, (long)va_arg(args, void *), 16, field_width, precision, flags);
                continue;

            case 'n':
                if (qualifier == 'l')
                {
                    long *ip = va_arg(args, long *);
                    *ip = (str - buf);
                }
                else
                {
                    int * ip = va_arg(args, int *);
                    *ip = (int)(str - buf);
                }
                continue;

            /* integer number formats - set up the flags and "break" */
            case 'o':
                base = 8;
                break;

            case 'X':
                flags |= LARGE;
                /* fall through */
            case 'x':
                base = 16;
                break;

            case 'd':
            case 'i':
                flags |= SIGN;
                /* fall through */
            case 'u':
                break;

            default:
                if (*fmt != '%')
                {
                    *str++ = '%';
                }
                if (*fmt)
                {
                    *str++ = *fmt;
                }
                else
                {
                    --fmt;
                }
                continue;
        }
        if (qualifier == 'L')
        {
            num = (long)va_arg(args, unsigned long);
        }
        else if (qualifier == 'l')
        {
            num = (long)va_arg(args, unsigned long);
            if (flags & SIGN)
            {
                num = labs(num);
            }
        }
        else if (qualifier == 'h')
        {
            num = (long)va_arg(args, int);
            if (flags & SIGN)
            {
                num = labs(num);
            }
        }
        else if (flags & SIGN)
        {
            num = (long)va_arg(args, int);
        }
        else
        {
            num = (long)va_arg(args, unsigned int);
        }
        str = number(str, num, base, field_width, precision, flags);
    }
    *str = '\0';
    return (int)(str - buf);
}
